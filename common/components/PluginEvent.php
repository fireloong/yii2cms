<?php

namespace common\components;

use yii\base\Event;
/**
 * description of PluginEvent
 *
 * @author FireLoong
 */
class PluginEvent extends Event
{
    public $params = null;
}
