<?php


namespace common\components;

use Yii;
use yii\base\Behavior;
/**
 * description of HelloBehavior
 *
 * @author FireLoong
 */
class HelloBehavior extends Behavior
{
    public function events()
    {
        return [
            //\yii\base\Controller::EVENT_AFTER_ACTION => 'hello',
            //\yii\db\ActiveRecord::EVENT_INIT => 'inits',
            //\yii\base\Module::EVENT_BEFORE_ACTION => 'beforeAction',
            \yii\base\Model::EVENT_AFTER_VALIDATE => 'afterValidate'
        ];
    }
    public function hello($event)
    {
        $vars = [];
        foreach (get_object_vars($event->sender) as $var => $object) {
            $vars[] = $var;
        }
        print_r($vars);
        print_r($event->sender->loadedModules);
        exit;
    }

    public function beforeAction($event)
    {
        echo 'beforeAction'.PHP_EOL;
    }
    public function inits($event)
    {
        //print_r($event->sender->user);
//        print_r($event->sender->errors);
//        exit;
//        $content = \yii\helpers\VarDumper::export($event);
//        file_put_contents(Yii::getAlias('@common/tmp/a.txt'),$content);
        echo 'hehe';
    }

    public function afterValidate($event)
    {
        print_r($event);
        exit;
    }
}
