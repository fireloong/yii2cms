<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;

/**
 * description of Color
 *
 * @author FireLoong
 */
class Color extends Field
{
    public function setup($options = [])
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
        }

        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        $input = Html::activeTextInput($this->model, $this->attribute, $options);
        $addon = Html::tag('span', '<i></i>', ['class' => 'input-group-addon']);

        $this->parts['{input}'] = Html::tag('div', $input . $addon, [
            'id' => 'cp_' . $this->attribute,
            'class' => 'input-group colorpicker-component'
        ]);
        $this->form->assets['color'] = [
            'css' => 'https://cdn.jsdelivr.net/npm/bootstrap-colorpicker@2.5.3/dist/css/bootstrap-colorpicker.min.css',
            'js' => 'https://cdn.jsdelivr.net/npm/bootstrap-colorpicker@2.5.3/dist/js/bootstrap-colorpicker.min.js',
            'bundles' => ['yii\bootstrap\BootstrapThemeAsset', 'yii\bootstrap\BootstrapPluginAsset']
        ];
        $id = 'cp_' . $this->attribute;
        $this->form->view->registerJs('$(\'#' . $id . '\').colorpicker();');
        $this->form->view->registerCss('#' . $id . '{width:225px;}');
        return $this;
    }
}
