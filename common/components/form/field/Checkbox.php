<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;

/**
 * description of Checkbox
 *
 * @author FireLoong
 */
class Checkbox extends Field
{
    public function setup($options = [], $enclosedByLabel = true)
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
            $enclosedByLabel = $this->setupParams[1] ?? $enclosedByLabel;
        }
        if ($enclosedByLabel) {
            $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
            $this->parts['{label}'] = '';
        } else {
            if (isset($options['label']) && !isset($this->parts['{label}'])) {
                $this->parts['{label}'] = $options['label'];
                if (!empty($options['labelOptions'])) {
                    $this->labelOptions = $options['labelOptions'];
                }
            }
            unset($options['labelOptions']);
            $options['label'] = null;
            $this->parts['{input}'] = Html::activeCheckbox($this->model, $this->attribute, $options);
        }

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        return $this;
    }
}
