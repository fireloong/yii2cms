<?php


namespace common\components\form\field;

use common\components\form\Form;
use Yii;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use common\components\form\Field;
use backend\modules\categories\models\Categories;
use yii\helpers\BaseHtml;

/**
 * description of Category
 *
 * @author FireLoong
 */
class Category extends Field
{
    public function setup($optioins = [])
    {
        print_r($this->setupParams);
        return $this;
    }

    public function xmlSetup(\SimpleXMLElement $field, $options = [])
    {
        $data = ArrayHelper::remove($options, 'data');
        $extension = (string)$field->attributes()->extension;
        $categories = Categories::find()
            ->select(['id', 'parent_id', 'title', 'lft', 'level', 'published', 'language'])
            ->where(['extension' => $extension, 'published' => 1])
            ->all();

        $items = [];
        if ((string)$field->attributes()->show_root === 'true') {
            $items[] = Yii::t('common', 'ROOT');
        }
        foreach ($categories as $category) {
            $language = $category->language === '*' ? '' : ' (' . $category->language . ')';
            $prefix = '';
            if (intval($category->level) > 0) {
                $prefix = ' ' . str_repeat('- ', $category->level);
            }
            $items[$category->id] = $prefix . $category->title . $language;
        }

        $dropDownList = new DropDownList(array_merge([
            'model' => $this->model,
            'attribute' => $this->attribute . '[' . (string)$field->attributes()->name . ']',
            'form' => $this->form,
            'type' => 'dropDownList'
        ], $options));

        $label = Yii::t($data['langCat'], (string)$field->attributes()->label);
        $description = (string)$field->attributes()->description;
        $labelOptions = [];
        if (!empty($description)) {
            $labelOptions['desc'] = Yii::t($data['langCat'], $description);
        }
        return $dropDownList->setup($items, $this->setupParams)
            ->label($label, $labelOptions);
    }

    public function xSetup(\SimpleXMLElement $field, $options = [])
    {
        $data = ArrayHelper::remove($options, 'data');

        $extension = (string)$field->attributes()->extension;

        $categories = Categories::find()
            ->select(['id', 'parent_id', 'title', 'lft', 'level', 'published', 'language'])
            ->where(['extension' => $extension, 'published' => 1])
            ->orderBy('lft')
            ->all();

        $items = [];
        if ((string)$field->attributes()->show_root === 'true') {
            $items[] = Yii::t('common', 'ROOT');
        }
        foreach ($categories as $category) {
            $language = $category->language === '*' ? '' : ' (' . $category->language . ')';
            $prefix = '';
            if (intval($category->level) > 0) {
                $prefix = ' ' . str_repeat('- ', $category->level);
            }
            $items[$category->id] = $prefix . $category->title . $language;
        }

        $options = array_merge($this->inputOptions, $options);
        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }
        $this->parts['{input}'] = Html::activeDropDownList($this->model,$this->attribute,$items,$options);

        $label = Yii::t($data['langCat'], (string)$field->attributes()->label);

        $description = (string)$field->attributes()->description;
        $labelOptions = [];
        if (!empty($description)) {
            $labelOptions['desc'] = Yii::t($data['langCat'], $description);
        }

        return $this->label($label, $labelOptions);
    }
}
