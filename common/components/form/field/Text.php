<?php


namespace common\components\form\field;

use Yii;
use yii\helpers\ArrayHelper;
use common\components\form\Field;

/**
 * description of Text
 *
 * @author FireLoong
 */
class Text extends Field
{
    public function setup($options = ['class' => 'form-control'])
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
        }
        return $this->textInput($options);
    }

    public function xmlSetup(\SimpleXMLElement $field, $options = [])
    {
        $data = ArrayHelper::remove($options, 'data');
        return $this->textInput($this->setupParams)
            ->label(Yii::t($data['langCat'], (string)$field->attributes()->label));
    }
}
