<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;

/**
 * description of checkboxList
 *
 * @author FireLoong
 */
class CheckboxList extends Field
{
    public function setup($items = [], $options = [])
    {
        if ($this->setupParams) {
            $items = $this->setupParams[0] ?? $items;
            $options = $this->setupParams[1] ?? $options;
        }

        $options['itemOptions']['labelOptions']['class'] = $options['itemOptions']['labelOptions']['class'] ?? 'checkbox-inline';

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->_skipLabelFor = true;
        $this->parts['{input}'] = Html::activeCheckboxList($this->model, $this->attribute, $items, $options);

        return $this;
    }
}
