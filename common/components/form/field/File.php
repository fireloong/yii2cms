<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;

/**
 * description of File
 *
 * @author FireLoong
 */
class File extends Field
{
    public function setup($options = [])
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
        }

        if ($this->inputOptions !== ['class' => 'form-control']) {
            $options = array_merge($this->inputOptions, $options);
        }

        if (!isset($this->form->options['enctype'])) {
            $this->form->options['enctype'] = 'multipart/form-data';
        }

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeFileInput($this->model, $this->attribute, $options);

        return $this;
    }
}
