<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;

/**
 * description of DropDownList
 *
 * @author FireLoong
 */
class DropDownList extends Field
{
    public function setup($items = [], $options = [])
    {
        if ($this->setupParams) {
            $items = $this->setupParams[0] ?? $items;
            $options = $this->setupParams[1] ?? $options;
        }

        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeDropDownList($this->model, $this->attribute, $items, $options);

        return $this;
    }
}
