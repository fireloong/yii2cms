<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Field;

/**
 * description of Hidden
 *
 * @author FireLoong
 */
class Hidden extends Field
{
    public $template = '{input}';

    public function setup($options = [])
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
        }

        $options = array_merge($this->inputOptions, $options);

        $this->parts['{input}'] = Html::activeHiddenInput($this->model, $this->attribute, $options);
        return $this;
    }

    public function label($label = null, $options = [])
    {
        $this->parts['{label}'] = '';
        return $this;
    }

    public function begin()
    {
        if ($this->form->enableClientScript) {
            $clientOptions = $this->getClientOptions();
            if (!empty($clientOptions)) {
                $this->form->attributes[] = $clientOptions;
            }
        }
        return '';
    }

    public function end()
    {
        return '';
    }
}
