<?php


namespace common\components\form\field;

use yii\helpers\Html;
use common\components\form\Form;
use common\components\form\Field;
/**
 * description of Textarea
 *
 * @author FireLoong
 */
class Textarea extends Field
{
    public function setup($options = [])
    {
        if ($this->setupParams) {
            $options = $this->setupParams[0] ?? $options;
        }

        $options = array_merge($this->inputOptions, $options);

        if ($this->form->validationStateOn === Form::VALIDATION_STATE_ON_INPUT) {
            $this->addErrorClassIfNeeded($options);
        }

        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);
        $this->parts['{input}'] = Html::activeTextarea($this->model, $this->attribute, $options);

        return $this;
    }
}
