<?php


namespace common\components\form;

/**
 * description of FormHelper
 *
 * @author FireLoong
 */
class FormHelper
{
    protected static $paths;
    protected static $entities = [
        'field' => [],
        'form' => [],
        'rule' => []
    ];

    public static function addFormPath($new = null)
    {
        return self::addPath('form', $new);
    }

    protected static function addPath($entity, $new = null)
    {
        $paths = &self::$paths[$entity];
        if (empty($paths)) {
            $entity_plural = $entity . 's';
            $paths[] = __DIR__ . '/' . $entity_plural;
        }
        settype($new, 'array');
        foreach ($new as $path) {
            if (!in_array($path, $paths)) {
                array_unshift($paths, trim($path));
            }
            if (!is_dir($path)) {
                array_unshift($paths, trim($path));
            }
        }
        return $paths;
    }
}
