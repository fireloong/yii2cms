<?php


namespace common\components;

use Yii;
use yii\validators\Validator;

/**
 * description of JsonValidator
 *
 * @author FireLoong
 */
class JsonValidator extends Validator
{
    /**
     * {@inheritdoc}
     */
    protected function validateValue($value)
    {
        json_decode($value);

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $message = null;
                break;
            case JSON_ERROR_DEPTH:
                $message = Yii::t('yii', 'JSON_ERROR_DEPTH');
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $message = Yii::t('yii', 'JSON_ERROR_STATE_MISMATCH');
                break;
            case JSON_ERROR_CTRL_CHAR:
                $message = Yii::t('yii', 'JSON_ERROR_CTRL_CHAR');
                break;
            case JSON_ERROR_SYNTAX:
                $message = Yii::t('yii', 'JSON_ERROR_SYNTAX');
                break;
            case JSON_ERROR_UTF8:
                $message = Yii::t('yii', 'JSON_ERROR_UTF8');
                break;
            default:
                $message = Yii::t('yii', 'UNKNOWN_ERROR');
                break;
        }
        return is_null($message) ? null : [$message, []];
    }
}
