<?php


namespace common\components;

use Yii;
use yii\i18n\MissingTranslationEvent;

/**
 * description of YiiTranslationEventHandler
 *
 * @author FireLoong
 */
class YiiTranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        $messageFile = Yii::getAlias('@common/messages/') . $event->language . '/' . $event->category . '.php';
        $message = '';
        if (is_file($messageFile)) {
            $messages = include $messageFile;
            if (!is_array($messages)) {
                $messages = [];
            }
            if (isset($messages[$event->message])) {
                $message = $messages[$event->message];
            }
        }
        $event->translatedMessage = $message ?: $event->message;
    }
}
