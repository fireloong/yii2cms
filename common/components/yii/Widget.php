<?php


namespace common\components\yii;

/**
 * description of Widget
 *
 * @author FireLoong
 */
class Widget extends \yii\base\Widget
{
    public function render($view, $params = [])
    {
        $classArray = explode('\\', static::class);
        array_shift($classArray);
        array_pop($classArray);
        $themeView = '//' . implode('/', $classArray) . '/' . $view;

        try {
            return parent::render($themeView, $params);
        } catch (\yii\base\ViewNotFoundException $e) {
            return parent::render($view, $params);
        }
    }
}
