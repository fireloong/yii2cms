<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%member}}".
 *
 * @property int $uid
 * @property string $name
 * @property int $lastvisitDate
 *
 * @property User $u
 */
class Member extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%member}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid'], 'required'],
            [['uid', 'lastvisitDate'], 'integer'],
            [['name'], 'string', 'max' => 200],
            [['uid'], 'unique'],
            [['uid'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['uid' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'uid' => Yii::t('site', 'USER_ID'),
            'name' => Yii::t('site', 'ADMIN_NAME'),
            'lastvisitDate' => Yii::t('site', 'LAST_VISIT_DATE')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getU()
    {
        return $this->hasOne(User::class, ['id' => 'uid']);
    }
}
