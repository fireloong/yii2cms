<?php

return [
    'language' => 'zh-CN',
    'sourceLanguage' => null,
    'timeZone' => 'Asia/Shanghai',
    'version' => '1.0.0',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/basic',
                'baseUrl' => '@web/themes/basic',
                'pathMap' => [
                    '@app/views' => '@app/themes/basic'
                ]
            ]
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd HH:mm:ss'
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,// 是否开启美化URL
            'showScriptName' => false,// 是否显示脚本名称，如“index.php”
            'suffix' => '.html'// 显示后缀
        ]
    ],
    'on beforeRequest' => function ($event) {
        $cookies = $event->sender->request->cookies;
        if ($cookies->has('language')) {
            $event->sender->language = $cookies->getValue('language');
        } else {
            $language = $event->sender->cache->getOrSet('app.language', function () {
                $rows = (new \yii\db\Query())
                    ->select(['params'])
                    ->from('{{%extensions}}')
                    ->where(['element' => 'mod_languages', 'type' => 'module'])
                    ->one();
                if ($rows && isset($rows['params'])) {
                    $client = \common\components\Helper::getClient();
                    $params = json_decode($rows['params'], true);
                    return $params[$client] ?: false;
                }
            });

            if ($language) {
                $event->sender->language = $language;
            }
        }

        // 创建媒体（media）目录资源
        $assetManager = $event->sender->assetManager;
        $assetManager->hashCallback = function ($path) {
            return basename($path);
        };
        $url = $assetManager->getPublishedUrl('@common/media');
        if (!is_dir(Yii::getAlias('@webroot' . $url))) {
            $assetManager->publish('@common/media');
        }
        $assetManager->hashCallback = null;
    }
];
