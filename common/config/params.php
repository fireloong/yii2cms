<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'global' => [
        'editor' => 'tinymce',
        'theme' => 'basic',
        'hostInfo' => [
            'backend' => 'http://backend.tld/',
            'frontend' => 'http://frontend.tld/'
        ]
    ]
];
