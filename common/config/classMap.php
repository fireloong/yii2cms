<?php

return [
    'yii\base\Component' => '@common/components/yii/Component.php',
    'yii\i18n\PhpMessageSource' => '@common/components/yii/PhpMessageSource.php',
    'yii\base\View' => '@common/components/yii/View.php',
    'yii\helpers\Html' => '@common/components/yii/helpers/Html.php'
];
