<?php

use common\plugins\actionlog\main\PluginBehavior;

return [
    'backend\modules\admin\models\form\Login' => [
        'actionlog' => [
            'class' => PluginBehavior::class
        ]
    ],
    'yii\web\User' => [
        'actionlog' => [
            'class' => PluginBehavior::class
        ]
    ],
//    'yii\db\BaseActiveRecord' => [
//        'actionlog' => [
//            'class' => PluginBehavior::class
//        ]
//    ]
];
