<?php


namespace common\plugins\actionlog\main;

use Yii;
use yii\helpers\Url;
use yii\web\User;
use yii\base\Event;
use yii\base\Behavior;
use yii\db\BaseActiveRecord;
use common\components\Helper;
use common\models\Member;
use backend\models\Admin;
use backend\modules\actionlogs\models\ActionLogs;
use backend\modules\actionlogs\controllers\DefaultController;

/**
 * description of ActionlogBehavior
 *
 * @author FireLoong
 */
class PluginBehavior extends Behavior
{
    /**
     * {@inheritdoc}
     */
    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_VALIDATE => 'afterValidate',
//            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            User::EVENT_AFTER_LOGIN => 'afterLogin',
            User::EVENT_AFTER_LOGOUT => 'afterLogout',
            DefaultController::EVENT_AFTER_LOG_PURGE => 'afterLogPurge',
            DefaultController::EVENT_AFTER_LOG_EXPORT => 'afterLogExport'
        ];
    }

    /**
     * 登录验证失败记录日志
     * @param Event $event
     */
    public function afterValidate(Event $event)
    {
        if (isset($event->sender->user) && $event->sender->user && !empty($event->sender->errors)) {
            $message = [
                'action' => 'login',
                'userid' => $event->sender->user->id,
                'username' => $event->sender->user->username,
                'accountlink' => Url::to(['/admin/user/view', 'id' => $event->sender->user->id]),
                'app' => Helper::getClient()
            ];

            $actionLogs = new ActionLogs();
            $actionLogs->addLog('USER_TRIED_LOGIN_APP', $message, $event->sender->user->id);
        }
    }

    /**
     * 登录成功记录日志
     * @param Event $event
     */
    public function afterLogin(Event $event)
    {
        $message = [
            'action' => 'login',
            'userid' => $event->identity->id,
            'username' => $event->identity->username,
            'accountlink' => Url::to(['/admin/user/view', 'id' => $event->identity->id]),
            'app' => Helper::getClient()
        ];

        $actionLogs = new ActionLogs();
        $actionLogs->addLog('USER_LOGGED_APP', $message, $message['userid']);
        if (is_null($event->identity->admin)){
            $member = Member::findOne($message['userid']);
            $member->lastvisitDate = time();
            $member->save();
        }else{
            $admin = Admin::findOne($message['userid']);
            $admin->lastvisitDate = time();
            $admin->save();
        }
    }

    /**
     * 退出成功记录日志
     * @param Event $event
     */
    public function afterLogout(Event $event)
    {
        $message = [
            'action' => 'logout',
            'userid' => $event->identity->id,
            'username' => $event->identity->username,
            'accountlink' => Url::to(['/admin/user/view', 'id' => $event->identity->id]),
            'app' => Helper::getClient()
        ];

        $actionLogs = new ActionLogs();
        $actionLogs->addLog('USER_LOGGED_OUT_APP', $message, $message['userid']);
    }

    /**
     * 删除日志记录日志
     */
    public function afterLogPurge()
    {
        $message = [
            'action' => 'actionlogs',
            'userid' => Yii::$app->user->identity->id,
            'username' => Yii::$app->user->identity->username,
            'accountlink' => Url::to(['/admin/user/view', 'id' => Yii::$app->user->identity->id])
        ];

        $actionLogs = new ActionLogs();
        $actionLogs->addLog('USER_PURGED_ONE_OR_MORE_ROWS', $message, $message['userid']);
    }

    /**
     * 导出日志记录日志
     */
    public function afterLogExport()
    {
        $message = [
            'action' => 'actionlogs',
            'userid' => Yii::$app->user->identity->id,
            'username' => Yii::$app->user->identity->username,
            'accountlink' => Url::to(['/admin/user/view', 'id' => Yii::$app->user->identity->id])
        ];

        $actionLogs = new ActionLogs();
        $actionLogs->addLog('USER_EXPORTED_ONE_OR_MORE_ROWS', $message, $message['userid']);
    }

//    public function afterDelete(Event $event)
//    {
//        if(get_class($event->sender) === 'backend\modules\actionlogs\models\ActionLogs'){
//            print_r($event->sender);
//        }
//        exit;
//    }

    public function afterHello($event)
    {
        print_r($event);
    }
}
