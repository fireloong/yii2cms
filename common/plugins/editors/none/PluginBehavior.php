<?php


namespace common\plugins\editors\none;

use yii\base\Behavior;

/**
 * description of ActionlogBehavior
 *
 * @author FireLoong
 */
class PluginBehavior extends Behavior
{
    public $options = [];
    public $form;
    public $inputId;

    public function editor()
    {
        $width = $this->wh('width', '100%');
        $height = $this->wh('height', '300px');
        $css = '#' . $this->inputId . '{width:' . $width . ';height:' . $height . ';}';
        $this->form->view->registerCss($css);
    }

    private function wh($px, $default)
    {
        $result = '';
        if (isset($this->options[$px]) && !empty($this->options[$px])) {
            if (is_numeric($this->options[$px])) {
                $result = $this->options[$px] . 'px';
            } elseif (is_string($this->options[$px])) {
                $result = $this->options[$px];
            }
        }

        return $result ?: $default;
    }
}
