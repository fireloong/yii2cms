-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- 主机： mysql
-- 生成日期： 2020-03-18 10:34:45
-- 服务器版本： 5.7.29
-- PHP 版本： 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `yii`
--

-- --------------------------------------------------------

--
-- 表的结构 `y_action_logs`
--

CREATE TABLE `y_action_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `info` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `log_date` int(11) NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `item_id` int(11) NOT NULL DEFAULT '0',
  `ip_address` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_action_logs`
--

INSERT INTO `y_action_logs` (`id`, `info`, `message`, `log_date`, `uid`, `item_id`, `ip_address`) VALUES
(1, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571737951, 1, 0, '127.0.0.1'),
(2, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571737957, 1, 0, '127.0.0.1'),
(3, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571737962, 1, 0, '127.0.0.1'),
(4, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571752360, 1, 0, '127.0.0.1'),
(5, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571752370, 1, 0, '127.0.0.1'),
(6, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571799233, 1, 0, '127.0.0.1'),
(7, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1571969229, 1, 0, '127.0.0.1'),
(8, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574668950, 1, 0, '172.17.0.1'),
(9, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574686523, 1, 0, '172.17.0.1'),
(10, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574730996, 1, 0, '172.17.0.1'),
(11, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574770178, 1, 0, '172.17.0.1'),
(12, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1574777174, 1, 0, '172.17.0.1'),
(13, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1574777221, 1, 0, '172.17.0.1'),
(14, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574906783, 1, 0, '172.17.0.1'),
(15, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1574995386, 1, 0, '172.17.0.1'),
(16, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575006849, 1, 0, '192.168.1.3'),
(17, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575030984, 1, 0, '172.17.0.1'),
(18, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575030996, 1, 0, '172.17.0.1'),
(19, 'USER_LOGGED_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575031226, 1, 0, '172.17.0.1'),
(20, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575031233, 1, 0, '172.17.0.1'),
(21, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575033968, 1, 0, '172.17.0.1'),
(22, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575034004, 1, 0, '192.168.1.3'),
(23, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575117958, 1, 0, '172.17.0.1'),
(24, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575117975, 1, 0, '172.17.0.1'),
(25, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575117981, 1, 0, '172.17.0.1'),
(26, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575119233, 1, 0, '172.17.0.1'),
(27, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575119370, 1, 0, '172.17.0.1'),
(28, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575119406, 1, 0, '172.17.0.1'),
(29, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575119412, 1, 0, '172.17.0.1'),
(30, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575119482, 1, 0, '172.17.0.1'),
(31, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575120703, 1, 0, '172.17.0.1'),
(32, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121119, 1, 0, '172.17.0.1'),
(33, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121155, 1, 0, '172.17.0.1'),
(34, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121168, 1, 0, '172.17.0.1'),
(35, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121508, 1, 0, '172.17.0.1'),
(36, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121650, 1, 0, '172.17.0.1'),
(37, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121926, 1, 0, '192.168.1.3'),
(38, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121939, 1, 0, '192.168.1.3'),
(39, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575121987, 1, 0, '192.168.1.3'),
(40, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575166009, 1, 0, '172.17.0.1'),
(41, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575166146, 1, 0, '172.17.0.1'),
(42, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575167896, 1, 0, '172.17.0.1'),
(43, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575168692, 1, 0, '172.17.0.1'),
(44, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575169360, 1, 0, '172.17.0.1'),
(45, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575169406, 1, 0, '192.168.1.3'),
(46, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575181428, 1, 0, '172.17.0.1'),
(47, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575181437, 1, 0, '172.17.0.1'),
(48, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197392, 1, 0, '192.168.1.3'),
(49, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197548, 1, 0, '192.168.1.3'),
(50, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197569, 1, 0, '192.168.1.3'),
(51, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197581, 1, 0, '192.168.1.3'),
(52, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197592, 1, 0, '192.168.1.3'),
(53, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575197751, 1, 0, '172.17.0.1'),
(54, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575253834, 1, 0, '192.168.1.3'),
(55, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575254039, 1, 0, '172.17.0.1'),
(56, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575254846, 1, 0, '172.17.0.1'),
(57, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575276529, 1, 0, '192.168.1.5'),
(58, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575281347, 1, 0, '192.168.1.3'),
(59, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575283424, 1, 0, '192.168.1.5'),
(60, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575283560, 1, 0, '172.17.0.1'),
(61, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575289567, 1, 0, '172.17.0.1'),
(62, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575338533, 1, 0, '172.17.0.1'),
(63, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575339786, 1, 0, '172.17.0.1'),
(64, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575339798, 1, 0, '172.17.0.1'),
(65, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575340464, 1, 0, '192.168.1.5'),
(66, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575340532, 1, 0, '192.168.1.3'),
(67, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575354304, 1, 0, '172.17.0.1'),
(68, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575375506, 1, 0, '172.17.0.1'),
(69, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575511264, 1, 0, '172.17.0.1'),
(70, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575525993, 1, 0, '192.168.1.3'),
(71, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575542649, 1, 0, '172.17.0.1'),
(72, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575542726, 1, 0, '172.17.0.1'),
(73, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575543674, 1, 0, '172.17.0.1'),
(74, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575545654, 1, 0, '192.168.1.3'),
(75, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575596156, 1, 0, '172.17.0.1'),
(76, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575597504, 1, 0, '192.168.1.3'),
(79, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575619778, 1, 0, '172.17.0.1'),
(80, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575620067, 1, 0, '172.17.0.1'),
(81, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575621740, 1, 0, '172.17.0.1'),
(82, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575621746, 1, 0, '172.17.0.1'),
(83, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575622351, 1, 0, '172.17.0.1'),
(84, 'USER_PURGED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1575622823, 1, 0, '172.17.0.1'),
(85, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575632191, 1, 0, '172.17.0.1'),
(86, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575684383, 1, 0, '172.17.0.1'),
(87, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575719702, 1, 0, '192.168.1.3'),
(88, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575721864, 1, 0, '172.17.0.1'),
(89, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575772539, 1, 0, '192.168.1.2'),
(90, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575774134, 1, 0, '172.17.0.1'),
(91, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575857251, 1, 0, '192.168.1.4'),
(92, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575857412, 1, 0, '172.17.0.1'),
(93, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575890094, 1, 0, '172.17.0.1'),
(94, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1575946103, 1, 0, '172.17.0.1'),
(95, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576029647, 1, 0, '192.168.1.4'),
(96, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576030460, 1, 0, '172.17.0.1'),
(97, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576116774, 1, 0, '172.17.0.1'),
(98, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576130316, 1, 0, '192.168.1.4'),
(99, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1576131743, 1, 0, '192.168.1.4'),
(100, 'USER_EXPORTED_ONE_OR_MORE_ROWS', '{\"action\":\"actionlogs\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\"}', 1576131763, 1, 0, '192.168.1.4'),
(101, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576221161, 1, 0, '172.17.0.1'),
(102, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576226423, 1, 0, '192.168.1.4'),
(103, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576227951, 1, 0, '192.168.1.4'),
(104, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576227991, 1, 0, '192.168.1.4'),
(105, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576320796, 1, 0, '172.17.0.1'),
(106, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576374728, 1, 0, '192.168.1.4'),
(107, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576375107, 1, 0, '172.17.0.1'),
(108, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576407532, 1, 0, '172.17.0.1'),
(109, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576408790, 1, 0, '172.17.0.1'),
(110, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409198, 3, 0, '172.17.0.1'),
(111, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409362, 3, 0, '172.17.0.1'),
(112, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409370, 1, 0, '172.17.0.1'),
(113, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409463, 1, 0, '172.17.0.1'),
(114, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409471, 3, 0, '172.17.0.1'),
(115, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409476, 3, 0, '172.17.0.1'),
(116, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576409483, 1, 0, '172.17.0.1'),
(117, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576411922, 1, 0, '192.168.1.4'),
(118, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576417751, 1, 0, '172.17.0.1'),
(119, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576417758, 1, 0, '172.17.0.1'),
(120, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576417800, 1, 0, '172.17.0.1'),
(121, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"hehe.html\",\"app\":\"backend\"}', 1576417869, 1, 0, '172.17.0.1'),
(122, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576417875, 1, 0, '172.17.0.1'),
(123, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576419250, 1, 0, '172.17.0.1'),
(124, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576463590, 1, 0, '172.17.0.1'),
(125, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576464150, 1, 0, '192.168.1.4'),
(126, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576500473, 1, 0, '172.17.0.1'),
(127, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576500489, 1, 0, '172.17.0.1'),
(128, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576503596, 1, 0, '172.17.0.1'),
(129, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576503603, 1, 0, '172.17.0.1'),
(130, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576503609, 1, 0, '172.17.0.1'),
(131, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576549548, 1, 0, '172.17.0.1'),
(132, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576549787, 3, 0, '172.17.0.1'),
(133, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576563805, 1, 0, '192.168.1.4'),
(134, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576563822, 1, 0, '192.168.1.4'),
(135, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576563835, 3, 0, '192.168.1.4'),
(136, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576567127, 3, 0, '192.168.1.4'),
(137, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576567200, 1, 0, '172.17.0.1'),
(138, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576567205, 1, 0, '172.17.0.1'),
(139, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576567213, 3, 0, '172.17.0.1'),
(140, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576572206, 3, 0, '172.17.0.1'),
(141, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576572213, 1, 0, '172.17.0.1'),
(142, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576575736, 3, 0, '192.168.1.4'),
(143, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576575900, 3, 0, '192.168.1.4'),
(144, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576575999, 1, 0, '192.168.1.4'),
(145, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576635014, 1, 0, '172.17.0.1'),
(146, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576665796, 1, 0, '172.17.0.1'),
(147, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576676566, 1, 0, '192.168.1.4'),
(148, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576719577, 1, 0, '192.168.1.4'),
(149, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576720229, 1, 0, '172.17.0.1'),
(150, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576753937, 1, 0, '172.17.0.1'),
(151, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576804884, 1, 0, '172.17.0.1'),
(152, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576809052, 1, 0, '192.168.1.4'),
(153, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576809114, 1, 0, '192.168.1.2'),
(154, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"frontend\"}', 1576813249, 1, 0, '172.17.0.1'),
(155, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576813254, 1, 0, '172.17.0.1'),
(156, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576840356, 1, 0, '172.17.0.1'),
(157, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576893383, 1, 0, '172.17.0.1'),
(158, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576925290, 1, 0, '172.17.0.1'),
(159, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576926702, 1, 0, '192.168.1.2'),
(160, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576926712, 1, 0, '192.168.1.2'),
(161, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1576926720, 3, 0, '192.168.1.2'),
(162, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1576985120, 1, 0, '172.17.0.1'),
(163, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577067436, 1, 0, '172.17.0.1'),
(164, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577170608, 1, 0, '172.17.0.1'),
(165, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577238545, 1, 0, '172.17.0.1'),
(166, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577324765, 1, 0, '172.17.0.1'),
(167, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577340434, 1, 0, '172.17.0.1'),
(168, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577340494, 1, 0, '172.17.0.1'),
(169, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577340686, 1, 0, '172.17.0.1'),
(170, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577428230, 1, 0, '172.17.0.1'),
(171, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577449097, 1, 0, '172.17.0.1'),
(172, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577497838, 1, 0, '172.17.0.1'),
(173, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577585432, 1, 0, '172.17.0.1'),
(174, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577670202, 1, 0, '172.17.0.1'),
(175, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577697998, 1, 0, '192.168.1.4'),
(176, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577760669, 1, 0, '172.17.0.1'),
(177, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":3,\"username\":\"loong\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=3\",\"app\":\"backend\"}', 1577799348, 3, 0, '172.17.0.1'),
(178, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577843552, 1, 0, '172.17.0.1'),
(179, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577869022, 1, 0, '172.17.0.1'),
(180, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577870162, 1, 0, '172.17.0.1'),
(181, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577870203, 1, 0, '172.17.0.1'),
(182, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1577931023, 1, 0, '172.17.0.1'),
(183, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578015109, 1, 0, '172.17.0.1'),
(184, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578049518, 1, 0, '172.17.0.1'),
(185, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578103648, 1, 0, '172.17.0.1'),
(186, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578118609, 1, 0, '172.17.0.1'),
(187, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578188499, 1, 0, '172.17.0.1'),
(188, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578308037, 1, 0, '172.17.0.1'),
(189, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578363435, 1, 0, '172.17.0.1'),
(190, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578395269, 1, 0, '172.17.0.1'),
(191, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578482210, 1, 0, '172.17.0.1'),
(192, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578535178, 1, 0, '172.17.0.1'),
(193, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578621998, 1, 0, '172.17.0.1'),
(194, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578709068, 1, 0, '172.17.0.1'),
(195, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578715179, 1, 0, '172.17.0.1'),
(196, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578715179, 1, 0, '172.17.0.1'),
(197, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578715187, 1, 0, '172.17.0.1'),
(198, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715323, 2, 0, '172.17.0.1'),
(199, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715695, 2, 0, '172.17.0.1'),
(200, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715734, 2, 0, '172.17.0.1'),
(201, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715856, 2, 0, '172.17.0.1'),
(202, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715950, 2, 0, '172.17.0.1'),
(203, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578715960, 2, 0, '172.17.0.1'),
(204, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578716366, 2, 0, '172.17.0.1'),
(205, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578716377, 2, 0, '172.17.0.1'),
(206, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":2,\"username\":\"demo\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=2\",\"app\":\"frontend\"}', 1578716380, 2, 0, '172.17.0.1'),
(207, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578743044, 1, 0, '172.17.0.1'),
(208, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578748903, 1, 0, '172.17.0.1'),
(209, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578797224, 1, 0, '172.17.0.1'),
(210, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"frontend\"}', 1578835826, 1, 0, '172.17.0.1'),
(211, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578835954, 1, 0, '172.17.0.1'),
(212, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578879690, 1, 0, '172.17.0.1'),
(213, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578914733, 1, 0, '172.17.0.1'),
(214, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578969291, 1, 0, '172.17.0.1'),
(215, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1578992355, 1, 0, '172.17.0.1'),
(216, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579055540, 1, 0, '172.17.0.1'),
(217, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579140553, 1, 0, '172.17.0.1'),
(218, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579141262, 1, 0, '172.17.0.1'),
(219, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579179955, 1, 0, '172.17.0.1'),
(220, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579228850, 1, 0, '172.17.0.1'),
(221, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579258421, 1, 0, '172.17.0.1'),
(222, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579310884, 1, 0, '172.17.0.1'),
(223, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579351178, 1, 0, '172.17.0.1'),
(224, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579400325, 1, 0, '172.17.0.1'),
(225, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579434491, 1, 0, '172.17.0.1'),
(226, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579519212, 1, 0, '172.17.0.1'),
(227, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579571311, 1, 0, '172.17.0.1'),
(228, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579572337, 1, 0, '172.17.0.1'),
(229, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1579660542, 1, 0, '172.17.0.1'),
(230, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582006681, 1, 0, '172.17.0.1'),
(231, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582077932, 1, 0, '172.17.0.1'),
(232, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582099485, 1, 0, '172.17.0.1'),
(233, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582163560, 1, 0, '172.17.0.1'),
(234, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582167282, 1, 0, '172.17.0.1'),
(235, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582173249, 1, 0, '172.17.0.1'),
(236, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582254166, 1, 0, '172.17.0.1'),
(237, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582254171, 1, 0, '172.17.0.1'),
(238, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582278070, 1, 0, '172.17.0.1'),
(239, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582462612, 1, 0, '172.17.0.1'),
(240, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582462882, 1, 0, '172.17.0.1'),
(241, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582943471, 1, 0, '172.17.0.1'),
(242, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1582943471, 1, 0, '172.17.0.1'),
(243, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583115092, 1, 0, '172.17.0.1'),
(244, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583129265, 1, 0, '172.17.0.1'),
(245, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583204497, 1, 0, '172.17.0.1'),
(246, 'USER_LOGGED_OUT_APP', '{\"action\":\"logout\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583224038, 1, 0, '172.17.0.1'),
(247, 'USER_TRIED_LOGIN_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583228450, 1, 0, '172.17.0.1'),
(248, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583228459, 1, 0, '172.17.0.1'),
(249, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583288611, 1, 0, '172.17.0.1'),
(250, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583325760, 1, 0, '172.17.0.1'),
(251, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583330991, 1, 0, '172.17.0.1'),
(252, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583374306, 1, 0, '172.17.0.1'),
(253, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583475198, 1, 0, '172.17.0.1'),
(254, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583585834, 1, 0, '172.17.0.1'),
(255, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583632798, 1, 0, '172.17.0.1'),
(256, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583754076, 1, 0, '172.17.0.1'),
(257, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583809737, 1, 0, '172.17.0.1'),
(258, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1583976672, 1, 0, '172.17.0.1'),
(259, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1584099097, 1, 0, '172.17.0.1'),
(260, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1584099180, 1, 0, '172.17.0.1'),
(261, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1584186175, 1, 0, '172.17.0.1'),
(262, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1584250774, 1, 0, '172.17.0.1'),
(263, 'USER_LOGGED_APP', '{\"action\":\"login\",\"userid\":1,\"username\":\"admin\",\"accountlink\":\"\\/admin\\/user\\/view.html?id=1\",\"app\":\"backend\"}', 1584494813, 1, 0, '172.17.0.1');

-- --------------------------------------------------------

--
-- 表的结构 `y_admin`
--

CREATE TABLE `y_admin` (
  `uid` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastvisitDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_admin`
--

INSERT INTO `y_admin` (`uid`, `name`, `lastvisitDate`) VALUES
(1, 'Super User', 1584494813),
(3, '', 1577799348);

-- --------------------------------------------------------

--
-- 表的结构 `y_auth_assignment`
--

CREATE TABLE `y_auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_auth_assignment`
--

INSERT INTO `y_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('ROLE_REGISTERED', '2', 1576329270),
('ROLE_SUPER_ADMINISTRATOR', '1', 1565848960),
('ROLE_SUPER_ADMINISTRATOR', '3', 1577799376);

-- --------------------------------------------------------

--
-- 表的结构 `y_auth_item`
--

CREATE TABLE `y_auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_auth_item`
--

INSERT INTO `y_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('@app-backend/*', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/actionlogs/*', 2, NULL, NULL, NULL, 1571735776, 1571735776),
('@app-backend/actionlogs/default/*', 2, NULL, NULL, NULL, 1571735776, 1571735776),
('@app-backend/actionlogs/default/index', 2, NULL, NULL, NULL, 1571735776, 1571735776),
('@app-backend/admin/*', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/assignment/*', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/assignment/assign', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/assignment/index', 2, NULL, NULL, NULL, 1565848732, 1565848732),
('@app-backend/admin/assignment/revoke', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/assignment/view', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/default/*', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/default/index', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/*', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/create', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/delete', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/index', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/update', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu-types/view', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/*', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/create', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/delete', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/index', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/update', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/menu/view', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/*', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/permission/assign', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/create', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/delete', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/index', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/remove', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/permission/update', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/permission/view', 2, NULL, NULL, NULL, 1565848733, 1565848733),
('@app-backend/admin/role/*', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/assign', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/create', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/delete', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/index', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/remove', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/update', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/role/view', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/*', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/assign', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/create', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/index', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/refresh', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/route/remove', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/*', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/create', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/delete', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/index', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/update', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/rule/view', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/user/*', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/activate', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/change-password', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/delete', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/user/index', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/user/login', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/user/logout', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/admin/user/request-password-reset', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/reset-password', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/signup', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/admin/user/view', 2, NULL, NULL, NULL, 1565848734, 1565848734),
('@app-backend/cache/*', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/cache/index', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/*', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/create', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/delete', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/index', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/update', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/clients/view', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/content/*', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/content/articles/*', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/content/articles/create', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/content/articles/delete', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/content/articles/index', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/content/articles/update', 2, NULL, NULL, NULL, 1578832026, 1578832026),
('@app-backend/extensions/*', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/extensions/create', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/extensions/delete', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/extensions/installer', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/extensions/manage', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/extensions/update', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/extensions/view', 2, NULL, NULL, NULL, 1565848735, 1565848735),
('@app-backend/languages/*', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/installed/*', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/installed/index', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/installed/switch', 2, NULL, NULL, NULL, 1569852538, 1569852538),
('@app-backend/languages/languages/*', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/languages/create', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/languages/delete', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/languages/index', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/languages/update', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/languages/view', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/overrides/*', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/overrides/create', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/overrides/delete', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/overrides/index', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/languages/overrides/update', 2, NULL, NULL, NULL, 1569760303, 1569760303),
('@app-backend/media/*', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/default/*', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/default/create', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/default/delete', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/default/index', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/default/upload', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/images/*', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/images/index', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/media/images/upload', 2, NULL, NULL, NULL, 1574857671, 1574857671),
('@app-backend/plugins/*', 2, NULL, NULL, NULL, 1574684794, 1574684794),
('@app-backend/plugins/default/*', 2, NULL, NULL, NULL, 1574684794, 1574684794),
('@app-backend/plugins/default/edit', 2, NULL, NULL, NULL, 1574684794, 1574684794),
('@app-backend/plugins/default/index', 2, NULL, NULL, NULL, 1574684794, 1574684794),
('@app-backend/site/*', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/site/error', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/site/icon', 2, NULL, NULL, NULL, 1578713143, 1578713143),
('@app-backend/site/index', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/site/login', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/site/logout', 2, NULL, NULL, NULL, 1565848736, 1565848736),
('@app-backend/site/sysinfo', 2, NULL, NULL, NULL, 1575858346, 1575858346),
('@app-backend/themes/*', 2, NULL, NULL, NULL, 1575343826, 1575343826),
('@app-backend/themes/styles/*', 2, NULL, NULL, NULL, 1575343825, 1575343825),
('@app-backend/themes/styles/index', 2, NULL, NULL, NULL, 1575343825, 1575343825),
('@app-backend/widgets/*', 2, NULL, NULL, NULL, 1575862824, 1575862824),
('@app-backend/widgets/default/*', 2, NULL, NULL, NULL, 1575862824, 1575862824),
('@app-backend/widgets/default/index', 2, NULL, NULL, NULL, 1575862824, 1575862824),
('@app-frontend/*', 2, NULL, NULL, NULL, 1576323440, 1576323440),
('@app-frontend/content/*', 2, NULL, NULL, NULL, 1578835780, 1578835780),
('@app-frontend/content/default/*', 2, NULL, NULL, NULL, 1578835780, 1578835780),
('@app-frontend/content/default/index', 2, NULL, NULL, NULL, 1578835780, 1578835780),
('@app-frontend/content/default/view', 2, NULL, NULL, NULL, 1578835780, 1578835780),
('@app-frontend/site/*', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/about', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/captcha', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/contact', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/error', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/index', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/language', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/login', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/logout', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/redirect', 2, NULL, NULL, NULL, 1583400862, 1583400862),
('@app-frontend/site/request-password-reset', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/resend-verification-email', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/reset-password', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/signup', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('@app-frontend/site/verify-email', 2, NULL, NULL, NULL, 1578713399, 1578713399),
('ROLE_PUBLIC', 1, 'ROLE_PUBLIC_DESCRIPTION', NULL, NULL, 1578711818, 1578743406),
('ROLE_REGISTERED', 1, 'ROLE_REGISTERED_DESCRIPTION', NULL, NULL, 1576325765, 1578743395),
('ROLE_SUPER_ADMINISTRATOR', 1, 'ROLE_SUPER_ADMINISTRATOR_DESCRIPTION', NULL, NULL, 1565848943, 1578743381);

-- --------------------------------------------------------

--
-- 表的结构 `y_auth_item_child`
--

CREATE TABLE `y_auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_auth_item_child`
--

INSERT INTO `y_auth_item_child` (`parent`, `child`) VALUES
('ROLE_SUPER_ADMINISTRATOR', '@app-backend/*'),
('ROLE_PUBLIC', '@app-backend/admin/user/login'),
('ROLE_PUBLIC', '@app-backend/admin/user/logout'),
('ROLE_PUBLIC', '@app-backend/admin/user/request-password-reset'),
('ROLE_PUBLIC', '@app-backend/admin/user/signup'),
('ROLE_PUBLIC', '@app-backend/site/error'),
('ROLE_PUBLIC', '@app-backend/site/login'),
('ROLE_PUBLIC', '@app-backend/site/logout'),
('ROLE_REGISTERED', '@app-frontend/*'),
('ROLE_PUBLIC', '@app-frontend/content/*'),
('ROLE_PUBLIC', '@app-frontend/site/about'),
('ROLE_PUBLIC', '@app-frontend/site/captcha'),
('ROLE_PUBLIC', '@app-frontend/site/contact'),
('ROLE_PUBLIC', '@app-frontend/site/error'),
('ROLE_PUBLIC', '@app-frontend/site/index'),
('ROLE_PUBLIC', '@app-frontend/site/language'),
('ROLE_PUBLIC', '@app-frontend/site/login'),
('ROLE_PUBLIC', '@app-frontend/site/logout'),
('ROLE_PUBLIC', '@app-frontend/site/redirect'),
('ROLE_PUBLIC', '@app-frontend/site/request-password-reset'),
('ROLE_PUBLIC', '@app-frontend/site/resend-verification-email'),
('ROLE_PUBLIC', '@app-frontend/site/signup'),
('ROLE_PUBLIC', '@app-frontend/site/verify-email'),
('ROLE_SUPER_ADMINISTRATOR', 'ROLE_REGISTERED');

-- --------------------------------------------------------

--
-- 表的结构 `y_auth_rule`
--

CREATE TABLE `y_auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `y_categories`
--

CREATE TABLE `y_categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `metakey` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `metadata` varchar(2048) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_at` int(11) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_categories`
--

INSERT INTO `y_categories` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `params`, `metadesc`, `metakey`, `metadata`, `created_by`, `created_at`, `modified_by`, `modified_at`, `hits`, `language`) VALUES
(1, 0, 0, 1, 0, 'uncategorised', 'mod_content', 'Uncategorised', 'uncategorised', '', '', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 1, 1576828229, 1, 1583477774, 0, '*'),
(2, 0, 2, 3, 0, 'news', 'mod_content', 'News', 'news', '', '', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 1, 1576848918, 1, 1583477774, 0, 'en-US'),
(3, 0, 4, 7, 0, 'news', 'mod_content', '新闻', 'news', '', '', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 1, 1576849332, 1, 1583477774, 0, 'zh-CN'),
(4, 3, 5, 6, 1, 'news/blog', 'mod_content', '博客', 'blog', '', '', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 1, 1583477773, 1, 1583477774, 0, 'zh-CN');

-- --------------------------------------------------------

--
-- 表的结构 `y_clients`
--

CREATE TABLE `y_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_clients`
--

INSERT INTO `y_clients` (`id`, `name`, `url`, `path`, `description`, `ordering`, `status`) VALUES
(1, 'backend', 'http://backend.tld/', '/backend', 'Background management application', 1, 1),
(2, 'frontend', 'http://frontend.tld/', '/frontend', 'Front desk application', 2, 1);

-- --------------------------------------------------------

--
-- 表的结构 `y_content`
--

CREATE TABLE `y_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `modified_at` int(11) NOT NULL,
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `publish_up` int(11) DEFAULT NULL,
  `publish_down` int(11) DEFAULT NULL,
  `images` text COLLATE utf8_unicode_ci NOT NULL,
  `urls` text COLLATE utf8_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `xreference` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_content`
--

INSERT INTO `y_content` (`id`, `title`, `alias`, `introtext`, `fulltext`, `status`, `catid`, `created_at`, `created_by`, `created_by_alias`, `modified_at`, `modified_by`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `ordering`, `metakey`, `metadesc`, `hits`, `metadata`, `featured`, `language`, `xreference`, `note`) VALUES
(1, '最新!4名中国船员遭海盗绑架，中国驻加蓬大使馆\"回应\"', 'zui-xin4ming-zhong-guo-chuan-yuan-zao-hai-dao-bang-jia-zhong-guo-zhu-jia-peng-da-shi-guan-hui-ying', '', '<p>【环球网报道&nbsp;记者 李东尧】外媒称，加蓬政府22日表示，加蓬首都利伯维尔港口4艘船只夜间遭到海盗袭击，导致1名加蓬籍船长被杀，另有4名中国籍船员被绑架。刚刚，环球网编辑部收到了中国驻加蓬大使馆工作人员发来的情况说明。其中提到，使馆昨天已经第一时间启动应急预案，指导企业向当地渔政部门和警方报案，同时紧急同加方联系交涉并召开联席会议，要求全力展开追查搜救行动，保障船员安全。</p>\r\n<p>情况说明全文如下：</p>\r\n<p>使馆昨天已经第一时间启动应急预案，指导企业向当地渔政部门和警方报案，并即派员赶赴现场了解情况，慰问相关企业;同时紧急同加方联系交涉并召开联席会议，要求全力展开追查搜救行动，保障船员安全。加方目前已加强海上安全措施，并开展相关工作。使馆并提醒我在加企业和公民加强安全防范，遇紧急情况及时报警并与使馆联系寻求协助。</p>\r\n<p><strong>此前报道：</strong><!-- pagebreak --></p>\r\n<p><strong>4名中国船员被绑架!加蓬首都港口遭海盗袭击，1名加蓬籍船长被杀</strong></p>\r\n<p>【环球网报道 记者 乌元春】据法新社报道，加蓬政府22日表示，加蓬首都利伯维尔港口4艘船只夜间遭到海盗袭击，导致1名加蓬籍船长被杀，另有4名中国籍船员被绑架。</p>\r\n<p><img src=\"https://himg2.huanqiucdn.cn/attachment2010/2019/1223/09/46/20191223094612748.png\" alt=\"法新社报道截图\" data-upload-link=\"%7B%22cover%22%3A%22%22%2C%22desc%22%3A%22%E6%B3%95%E6%96%B0%E7%A4%BE%E6%8A%A5%E9%81%93%E6%88%AA%E5%9B%BE%22%2C%22id%22%3A%22649f7adf92d9fd02869f1df885b941cd%22%2C%22url%22%3A%22http%3A%5C%2F%5C%2Fhimg2.huanqiu.com%5C%2Fattachment2010%5C%2F2019%5C%2F1223%5C%2F09%5C%2F46%5C%2F20191223094612748.png%22%2C%22tags%22%3A%5B%5D%2C%22time%22%3A%22%22%2C%22width%22%3A1061%2C%22height%22%3A118%2C%22mime%22%3A%22image%5C%2Fpng%22%2C%22size%22%3A12%7D\" /></p>\r\n<p>法新社报道截图</p>\r\n<p>加蓬政府发言人表示，海盗袭击了4艘船只。该发言人表示，海盗袭击在利伯维尔港口并不常见，但在几内亚湾附近却极为频繁。该发言人还表示，该国已部署国防和安全部队，&ldquo;在国际刑警组织和次区域机构的合作下，确保该地区的安全，并追查行凶者&rdquo;。</p>\r\n<p><img src=\"https://himg2.huanqiucdn.cn/attachment2010/2019/1223/09/45/20191223094533432.png\" alt=\"资料图：加蓬首都利伯维尔\" data-upload-link=\"%7B%22cover%22%3A%22%22%2C%22desc%22%3A%22%E8%B5%84%E6%96%99%E5%9B%BE%EF%BC%9A%E5%8A%A0%E8%93%AC%E9%A6%96%E9%83%BD%E5%88%A9%E4%BC%AF%E7%BB%B4%E5%B0%94%22%2C%22id%22%3A%22c22791836176cab9e0b99c9d01bb4786%22%2C%22url%22%3A%22http%3A%5C%2F%5C%2Fhimg2.huanqiu.com%5C%2Fattachment2010%5C%2F2019%5C%2F1223%5C%2F09%5C%2F45%5C%2F20191223094533432.png%22%2C%22tags%22%3A%5B%5D%2C%22time%22%3A%22%22%2C%22width%22%3A768%2C%22height%22%3A511%2C%22mime%22%3A%22image%5C%2Fpng%22%2C%22size%22%3A449%7D\" /></p>\r\n<p>资料图：加蓬首都利伯维尔</p>\r\n<p>法新社称，遭海盗袭击的4艘船只中，其中2艘是中加公司Sigapeche的渔船，4名被绑架的中国船员就来自该公司。第3艘船属于总部设在加蓬根蒂尔港的海运公司Satram，第4艘船则是悬挂巴拿马国旗的货船。<!-- pagebreak -->报道最后表示，几内亚湾已成为海盗袭击、抢劫和绑架事件频发的中心地带。根据国际海事局的数据，从今年1月到9月，全球82%的海上绑架事件都发生在几内亚湾。</p>', 1, 1, 1577104219, 1, '', 1582256331, 1, 1577104219, NULL, '{\"image_intro\":\"\",\"align_intro\":\"\",\"alt_intro\":\"\",\"caption_intro\":\"\",\"image_full\":\"\",\"align_full\":\"\",\"alt_full\":\"\",\"caption_full\":\"\"}', '', '', 0, '', '', 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(2, '错过的终究放下', 'cuo-guo-de-zhong-jiu-fang-xia', '', '<p>　　不得不说时间真的是这个世间最好的跨度，一些承诺，一段誓言，终究败给了时间，回忆再美好也经不住流年。有些人错过了终究得放下&hellip;&hellip;。</p>\r\n<p>　　在过往中，总有一个人，曾经是你的满心欢喜，现在却成了你的闭口不提。他闯进我们的生活，带给我们感动和美好，却又在某一时刻，猝不及防的从我们身边抽离，留下了一地的回忆。</p>\r\n<p>　　 当初说着各种承诺、各种誓言，我也相信了你就是我的小确幸。也是你让我相信了这世间还有美好，可最终你还是离我而去，终究你我成了彼此生命中的过客。</p>\r\n<p>　　我们错过了，我曾幻想过天长地久；我也曾幻想过和你手牵手走在黄昏的路上。可终究你走了，留给我的只有回忆。我以为守着回忆你就可以回来，可是我错了，我，终究没等来你的转身，我终究知道错过了就是错过了，回不来了。我也终于决定放下了&hellip;&hellip;</p>\r\n<p>　　终于下定决心把你归还于人海了！其实很早就在逼自己慢慢的去放手了，每次听着你那冠冕堂皇的话我尽然差点相信了我和你会有以后&hellip;&hellip;</p>\r\n<p>　　我没有你善于伪装，我学不会做最坏的人，我也不想浪费太多的时间和精力去等一个不可能的结果！虽然先动心、动情的人是你，无数次主动和挽留的人也是你，可我还是学不会去做一个你渴望中的人。</p>\r\n<p>　　这一路有快乐、有坎坷、有心酸。记得你曾对我说过：&ldquo;这一路来太多的心酸和坎坷自己必须好好珍惜才是&hellip;&hellip;&rdquo;你也说过：&ldquo;我不必有顾虑，你会珍惜你会好好保护着我&hellip;&hellip;&rdquo;这些话在耳边响起犹如昨天，那么悦耳那么清晰。可我不想这样原地不动的去等待和期望了，我准备回头了，回到我的原点，回到不是和你开始的原地了&hellip;&hellip;。</p>\r\n<p>　　你的承诺和誓言总归太遥远，你总归太缥缈。当我不在是你生命中的独一无二，我宁愿离去，也不愿在一份残缺的爱里苦苦挣扎。</p>\r\n<p>　　你总归是我命中未了的缘和劫，我们也终究错过了！如果上天能够重新来过，我会绕过那个和你认识的地方，遇见你也许就是没有结果，可我也能释怀了。</p>\r\n<p>　　我不能抱着那些回忆来折磨自己，我也不想就这样颓废的麻木的去过每一天了。你给的一切在回忆的沼泽里只会让我放不下，你走后在每一个似曾相识的场景里我总是会不由自主的想起你，我会盯着你送的东西久久的发呆，也会因为看到某个熟悉的背影，而伤心落泪。</p>\r\n<p><img style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/assets/media/深度截图_选择区域_20200116101658.png\" alt=\"深度截图_选择区域_20200116101658.png\" width=\"500\" height=\"288\" /></p>\r\n<p>　　我曾试过收起那些东西甚至屏蔽一切与你有关的东西。直到最后，我不得不承认，用心爱过的人，就连忘记也需要格外用力。</p>\r\n<p>　　我也曾试着挽回，试着去弥补，可终究太苍白太无力了。这就是世间的无奈，我也不得不承认你我终究错过了，我也该放下了&hellip;&hellip;</p>\r\n<p>　　现在的我终于学会了该怎么去割舍，我终究学会了真正的放下就是面对你的一切波澜不惊、坦然面对。即使听见你的名字多少次也不再泛起涟漪，终于接受了，你只能陪我一程，终究无法参与我的余生。</p>\r\n<p>　　谢谢你教会我的一切，包括那些没实现的承诺&hellip;&hellip;或许你会说你爱过我，可我还是决定把你归还于人海了，不是赌气，也不是不爱，只是觉得该清醒了&hellip;&hellip;</p>\r\n<p>　　我终于能很轻松地说我们错过了，你终究是那个错的人，我也决定放下了！余生很长，放下错的人，才能拥抱属于我的幸福。</p>', 1, 1, 1577104879, 1, '', 1579263666, 1, 1577104879, NULL, '{\"image_intro\":\"\",\"align_intro\":\"\",\"alt_intro\":\"\",\"caption_intro\":\"\",\"image_full\":\"\",\"align_full\":\"\",\"alt_full\":\"\",\"caption_full\":\"\"}', '', '', 0, '', '', 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 1, '*', '', ''),
(3, '从3个角度，看互联网+课程的如何结合', 'cong3ge-jiao-du-kan-hu-lian-wangke-cheng-de-ru-he-jie-he', '', '<div class=\"img-container\">随着教育信息化进程的加快，互联网将深度与教育融合，传统&ldquo;满堂灌&rdquo;形式的课程体系，不仅无法满足培养具备新时代信息化素养人才的需求，且无法满足新时代学生个性化发展的需求。因此，教育必将与互联网相结合，改变原有课程设计体系，课程实施方式及课程结构，为教育现代化开辟新的篇章。</div>\r\n<div class=\"img-container\"><!-- pagebreak --></div>\r\n<div class=\"img-container\"><img class=\"large\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"/assets/media/728da9773912b31b1aeaf1998f037b7cdbb4e199.jpeg\" alt=\"728da9773912b31b1aeaf1998f037b7cdbb4e199.jpeg\" width=\"640\" height=\"360\" data-loadfunc=\"0\" data-loaded=\"0\" /></div>\r\n<p>课程基础</p>\r\n<p><span class=\"bjh-p\">现代教育中决定课程的关键因素分别是：知识、社会要求、社会条件、学生特点。</span></p>\r\n<p><span class=\"bjh-p\">知识是符合人类文明发展方向，代表人类对物质世界和精神世界探索的总和。课程作为知识的载体，代表了知识的有效传承和发展，在课程演变过程中，第一个需要解决的核心问题就是选择何种知识作为课程的基础。因此知识是影响课程的最基本前提，知识的多寡决定了课程的丰富程度。</span></p>\r\n<p><span class=\"bjh-p\">社会要求代表了时代对教育及人才能力的诉求，在一定程度上 ，教育的本质是培养面向现代化，面向世界，面向未来，可为人类及社会发展贡献力量的新时代人才。因此，社会对人才的要求，决定了人们从知识库中选择何种知识形成课程，是影响课程的决定性因素。</span></p>\r\n<p><span class=\"bjh-p\">社会条件反映了当前社会能提供的社会科学技术及物质生产水平，是决定课程物质形态的基础。社会物质及技术的多寡决定现代化课程理念的更新程度，进而决定课程组织方式及展示形态。因纸张技术的发展，使课本教学成为可能；因多媒体设备及技术的发展，使基于多媒体的教学模式逐步普及。</span></p>\r\n<p><span class=\"bjh-p\">在当前阶段，物联网，云计算，大数据及移动互联网得到长足的应用和普及。就课程教学而言，大部分依旧采用传统模式，和互联网的结合程度普遍较低，在社会条件和实际教学诉求的驱动下，急需进行变革。</span></p>\r\n<p><span class=\"bjh-p\">不同个体的学生对知识的接受程度和侧重程度存在一定的差异，教育需实现因材施教，以人为本，注重个体的发展，传统的课程组织方式无法满足不断发现的个性化学生诉求。需依据学生认知特点，认识水平，学习动机等个性化需要提供动态的课程组织方式。</span></p>\r\n<p><span class=\"bjh-p\">在知识、社会要求、社会条件、学生特点的共同作用下，互联网将逐步改变现有课程知识传承的目标。传统课程核心目标为促进知识的传递，从现有社会知识总量中，依据社会发展需求汇集核心知识，以集中课堂教学的方式，通过不同学科传递至学生。</span></p>\r\n<p><span class=\"bjh-p\">统一化的知识传递体系符合工业社会的需求，工业化强调统一化，效率，规模。现有课程组织方式可快速实现知识的灌输，批量生产具备一定基础的教育型人才。</span></p>\r\n<p><span class=\"bjh-p\">但21世纪是信息化的时代，依托互联网科学技术的飞速发展，新兴技术大量涌现，知识量急剧膨胀，知识更新速度空前加快，出现了&ldquo;知识爆炸&rdquo;的现象。</span></p>\r\n<p><span class=\"bjh-p\">在新时代中，课程研究者不得不面对飞速增长知识和技术与单位时间内可传递有限知识间的挑战。依据当前以传承知识为基础的课程目标，在实际组织及实施过程中，为满足当前社会发展对人才知识结构的现实要求，只能通过不断添加新的知识到现有课程中，随着课程知识的不断增加，将逐步面临&ldquo;知识过载&rdquo;的尴尬境地，总不能无限延长学生学习时间。</span></p>\r\n<p><span class=\"bjh-p\">因此，在信息社会中需改变课程基础目标，以传授知识为主转变为培养学生自主学习能力和主动知识获取方式，提高学生终生学习动机和终生学习能力。</span></p>\r\n<p><span class=\"bjh-p\">革新课程设计理念和课程组织方式，推崇&ldquo;以人为本，因材施教&rdquo;的设计理念，培养学生自主学习能力和知识获取方式，培育学生终生学习动机和终生学习能力。21世纪是信息化社会，在信息社会中核心生成要素就是信息，信息驱动时代发展。在当前社会环境下，信息经济发展越来越依赖与以知识为基础的信息产业，在一定程度上，信息知识获取的及时和利用决定了整体信息产业的发展，新时代孕育着中华崛起的新机遇，新机遇需要一代代创新型人才不断的发掘和坚持不懈的前行。</span></p>\r\n<p><span class=\"bjh-p\">时代响起了新的号召，需要新时代的人才具备极强的信息化素养，具备信息获取、信息分析和信息加工的能力。而作为人才培养的摇篮，教育课程急需响应社会发展的现实需求。</span></p>\r\n<p><span class=\"bjh-p\">下面我们将分别从课程设计、课程实施和课程结构三个方面来做简单的结介绍，看课程如何实现与互联网的结合。</span></p>\r\n<p>1. 线上线下融合的课程设计体系</p>\r\n<p><span class=\"bjh-p\">信息时代下的数字原住民追求个性化，趣味性的阅读，学习体验。在新时代，为满足数字原住民的诉求，课程的设计越来越强调个性化，可交互性。急需摆脱传统单向，枯燥的课程组织模式。因此课程需借助互联网，推出融合线上线下，基于互联网教学的电子版立体化课程。立体化课程需包含教材、教学课件、网络课程、教学资源库（含试题库）、教材应用和服务支撑平台等重要组成部分。</span></p>\r\n<p><span class=\"bjh-ul\"><span class=\"bjh-li\">其中教材作为课程教学的主体可通过灵活的组织方式实现个性化的知识传递，教材的设计在确保交互性，趣味性的同时强调传递基础性、通用性的知识；</span><span class=\"bjh-li\">教学课件可基于教育资源公共服务平台集成优质教育资源，在帮助教师讲授重点、难点知识体系的同时，有效实现教育资源的开发共享，逐步消除资源发展不均衡的现实问题；</span><span class=\"bjh-li\">网络课程辅助学生自主学习，完成课程内容的课前预习，课后复习等线下自主学习活动，在一定程度上，还可实现翻转课堂的效果，使课堂组织及开展方式更加灵活；</span><span class=\"bjh-li\">教学资源库（含试题库）主要满足学生个性化知识拓展需要，集合各类型优质教育资源，通过互联网统一对外提供，学生按需获取，为学生拓展视野提供最坚定的支持；</span><span class=\"bjh-li\">教材应用和服务支持平台为教师和学生提供全面学习的支撑服务。</span></span><span class=\"bjh-p\">基于互联网技术设置线上与线上的混合式课程，解决当前课程知识传递困境的同时，满足学生个性化，差异化教学需求 ，实现真正的&ldquo;学生主体，教师主导&rdquo;的新理念。</span></p>\r\n<p><span class=\"bjh-p\">在课程设计中，进一步前强调教材的可交互性，有效提升教学趣味性和学生参与程度。互联网+课程将进一步打破时空限制，知识的传递和课程的教学将不受教室的限制。</span></p>\r\n<p><span class=\"bjh-p\">教师基于互联网面向学生提供丰富的教学资源，建立科学的评测机制，有效设置线上线下的比例。学生将可依据个人需求，按需获取所需知识，依据个人学习步调，知识接收程度自我调节学习进度，实现真正的线上线下相融合的课程设计。</span></p>\r\n<p>2. 回归生活的课程实施方案</p>\r\n<p><span class=\"bjh-p\">基于互联网的课程设计方式将革新现代课程设计理念，强调&ldquo;学生主体，教师主导&rdquo;，满足个性化，趣味性的人才发展要求，贴合现代化人才培养内需。在教材，教学课件，网络课程，教学资源库（含试题库），教材应用和服务支持平台为基础的立体化课程设计方向的驱动下，课程设计 ，将迈入新的台阶。</span></p>\r\n<p><span class=\"bjh-p\">但课程作为一个统一的整体，具备合理设计理念的同时，在课程实施过程中同步需借助互联网手段，推动课程组织及实施的进化。下面将通过引入&ldquo;回归生活的课程实施方案&rdquo;为主题，讨论在课程实施过程中，遇到的问题及相对应的解决方案。</span></p>\r\n<p><span class=\"bjh-p\">传统课程实施主要基于教室场景，学生学习活动被限制在单一环境，知识传递的时间和空间严重受限。这种实施方式必将限制学生在真实环境中知识的应用和迁移，容易导致学生大脑只是用于知识存储的硬盘，存储课程灌输的知识，而没有对应现实场景来驱动知识的调用，产生实际价值。</span></p>\r\n<p><span class=\"bjh-p\">因此，在课程实施过程中，需借助互联网手段打破课程教学时间空间限制，将学生从实际教室场景中解放出来，将知识的传递与具体生活场景发生关联，让学生在生活中学习，在学习中生活，提高学生知识迁移及应用水平。</span></p>\r\n<p><span class=\"bjh-p\">互联网新兴理念及技术与课程的深度融合将有效助力构建回归生活的课程实施环境。基于互联网手段搭建移动学习，泛在学习环境，为学生提供沉浸式的学习体验，帮助学生理解学习内容，了解学习情境，进一步激发学生学习主动性。在实际实施构建过程中，可采用虚拟现实技术+群体知识构建的方案。</span></p>\r\n<p><span class=\"bjh-p\">虚拟现实技术具备沉浸性，交互性，想象性的先天技术优势，借助虚拟现实技术实施课程教学可有效打破&ldquo;先理念再实践&rdquo;的固有课程组织方式，避免学用脱节。在虚拟现实技术的支持下，学生可依据所学知识进行仿真化实验构建，例如，理工科学生科直接进行仿真实验，构建仿真汽车，仿真桥梁；生物及医学类学生可进行仿真人体解剖，了解人体结构等。</span></p>\r\n<p><span class=\"bjh-p\">随着虚拟现实技术发展，必将引入更多技术及能力助力实际课程实施，使课程教学实施摆时间空间的限制，创造在现实生活中无法便捷实现的课程环境。</span></p>\r\n<p><span class=\"bjh-p\">课程的实施强调沉浸式，贴合实际生活场景，借助虚拟现实技术可引导课程实施的创新， 增强趣味性和互动性。在确保趣味，互动的同时，还需提升学生主观能动性，前面我们一直强调，新课程的组织最好可遵循：&ldquo;学生主体，教师主导&rdquo;的目标，这一目标不仅作用于课程的组织，也可以作用于课程内容的实施上。</span></p>\r\n<p><span class=\"bjh-p\">在互联网时代，知识将不在在掌握在某个人手里，借助互联网，人人都可成为知识的消费者，同时也可以成为知识的生产者，这是互联网发展的源泉之一。传统教学过程中，教师传授的知识是课程内容的全部 ，学生只能被动的接收，只能作为知识的消费者，而无法加入到知识的构建中。</span></p>\r\n<p><span class=\"bjh-p\">因此，在新时代课程内容构建中，我们强调课程内容是生成的、进化的，这种进化驱动力不仅仅来源于教师，也来源于学生，教师和学生一起参与课程内容的构建。</span></p>\r\n<p><span class=\"bjh-p\">群体智慧是课程内容进化的重要动力，以教师为主导，借助互联网技术，例如在线共享及协同软件实现教师与学生的协同知识构建体系，基于协同知识构建体系让学生参与到课程内容的创造过程中，在提升学生的高阶思维素养的同时，逐步培育学生自我反思意识，发展学生的批判思维。</span></p>\r\n<p><span class=\"bjh-p\">在实际协同过程中，学生通过讨论、争辩、分享、答疑等互动手段引发知识冲突，促进自我反思，优化已有的知识结构，从而创造性地产生新的知识和新的技能。在协同知识构建过程中，对学生提出要求的同时，也要求教师转变教学思路，教师需要重点关注学生知识建构的过程和生成的内容，关注学生知识创造和改进的过程，而不只是关注教学的内容、教学活动的表面形式、学生参与学习的次数等。</span></p>\r\n<p><span class=\"bjh-p\">虽然课程要求具备一定程度的计划性，但在具备计划性的同时应坚信学生是教学的主体。在整体协同过程中，教师起到引导，把控，评测作用。通过协同知识构建体系提高学生协同效能，强调群体智慧的重要，共同创建进化的，具备旺盛生命力的课程知识。</span></p>\r\n<p>3. 整合性的课程结构</p>\r\n<p><span class=\"bjh-p\">上面我们说过以混合式课程的主体，结合教材，网络课件，网络课程，教学资源库（含试题库），教材应用和服务支持平台多种形式的融合线上线下的课程设计体系。提出课程要贴合生活情景，让学生&ldquo;在学习中实践，在实践中学习&rdquo;回归生活的教材实施方案。 下面我们将从&ldquo;融合线上线下课程设计&rdquo;体系，&ldquo;回归生活的教材实施&rdquo;方案出发，介绍整合性课程结构的基础内容。</span></p>\r\n<p><span class=\"bjh-p\">21世纪，时代对人员素养提出新的要求，在信息飞速剧增，知识急剧爆炸的时代背景下，教育的目标及理念面临新的挑战。而其中最核心的挑战就是：如何通过学校教育培养出具备终生学习能力和素养的人才综合性人才，以适应时代，知识的飞速发展的需要。</span></p>\r\n<p><span class=\"bjh-p\">传统教育中以学科知识为主的课程体系，已无法满足社会对人才知识结构的诉求。社会发展越来越趋向多元化，多元化的社会分工驱动人才需具备多元化的知识结构。因此，反应到课程体系上，需摆脱传统以学科为主的知识传递体系，强调跨学科教学的重要性，知识传递途径从分散独立走向整合。</span></p>\r\n<p><span class=\"bjh-p\">在现阶段社会条件的支持下，孕育了一系列以培养学生跨学科知识能力和素养的整合性课程解决方案，例如：STEM教育，创客教育，少儿编程等，下面我们着重介绍STEM教育和创客教育。</span></p>\r\n<p><span class=\"bjh-p\">STEM是科学（Science），技术（Technology），工程（Engineering），数学（Mathematics）四门学科的统称，其中科学在于认识世界、解释自然界的客观规律；技术和工程则是在尊重自然规律的基础上改造世界、实现与自然界的和谐共处、解决社会发展过程中遇到的难题；数学则作为技术与工程学科的基础工具。</span></p>\r\n<p><span class=\"bjh-p\">STEM教育并不是四门学科的简单叠加，而是将四门学科有机的整合在一起，形成一个统一的整体用于培养及提高学生科学素养，技术素养，工程素养，数学素养以适应时代的发展。</span></p>\r\n<p><span class=\"bjh-p\">STEM教育起源于美国，1986年美国国家科学委员会发表《本科的科学、数学和工程教育》报告，强调科学素养的重要性，得到全世界范围内教育学者的广泛认可。2006年1月31人日美国总统布什在其国情咨文中公布一项重要计划&mdash;&mdash;《美国竞争力计划》（American Competitiveness Initiative，ACI），提出知识经济时代教育目标之一是培养具有STEM素养的人才，并称其为全球竞争力的关键。</span></p>\r\n<p><span class=\"bjh-p\">十年后的2016年9月14日，美国研究所与美国教育部综合了研讨会与会学者对 STEM 未来十年的发展愿景与建议，联合发布：《 教育中的创新愿景》(STEM 2026：A Vision for Innovation in STEM Education) 。</span></p>\r\n<p><span class=\"bjh-p\">旨在推进STEM教育创新方面的研究和发展，并为之提供坚实依据，该报告提出了六个愿景，力求在实践社区、活动设计、教育经验、学习空间、学习测量、社会文化环境等方面促进 STEM 教育的发展，以确保各年龄阶段以及各类型的学习者都能享有优质的 STEM 学习体验，解决 STEM 教育公平问题，进而保持美国的竞争力。</span></p>\r\n<p><span class=\"bjh-p\">从此美国政府逐步加大了对从小学到大学各个层次的STEM教育的支持力度，推出教育基金，鼓励各州改善STEM教育，加大对基础教育阶段理工科教师的培养和培训。</span></p>\r\n<p><span class=\"bjh-p\">近年来STEM教育逐步受到国内外教育研究者的重视，旨在全力培养具备知识经济时代强力竞争力的综合性人才。而STEM教育所具备的跨学科、趣味性、情境性、协作性在内的核心特征正好符合时代对人才能力的培养诉求。</span></p>\r\n<p><span class=\"bjh-p\"><span class=\"bjh-strong\">跨学科：</span>传统课程结构设计中，我们依据实际教学需要，将知识划分为不同的学科进行教授，在一定程度上可以提高教学的效率，学生关注点聚集在特定的学科知识上，可实现复杂知识内容的讲解。</span></p>\r\n<p><span class=\"bjh-p\">但依据学科进行知识的划分，和实际生活场景是分裂的，在实际生活中知识的迁移的应用是连续的，整合的，我们需运用包含科学，技术，工程，数学在内的整合性知识去解决实际场景中遇到的问题。</span></p>\r\n<p><span class=\"bjh-p\">因此STEM应运而生，跨学科是它最重要的核心特征，在STEM教育项目中，我们不再将重点放某个特定的学科或过于关注学科的界限，而是将重心放在特定问题上，引导学生使用科学，技术，工程，数学在内的整合性知识去解决实际遇到的问题，在问题中学习。培养学生跨学科能力，提高学生发现问题和解决问题的能力，以适应知识时代的发展需要。</span></p>\r\n<p><span class=\"bjh-p\"><span class=\"bjh-strong\">趣味性：</span>相对于传统教育，STEM教育将更具趣味性。在STEM设计和实施过程中强调将知识融合在有趣，具有挑战，跟学生实际生活相关的问题或活动中。知识蕴含在实际趣味问题中，学生通过自主或协作解决问题实现主动获取知识。强调趣味性，通过问题或活动激发学生内在学习动机，通过问题的解决获得成就感。</span></p>\r\n<p><span class=\"bjh-p\">在教育过程中，强调分享、创造，强调让学生体验和获得分享中的快乐感与创造中的成就感。部分项目还可将STEM知识与游戏结合，通过游戏化学习，激发学生学习兴趣和主动性，摆脱传统枯燥式，填充式教学的弊端。</span></p>\r\n<p><span class=\"bjh-p\"><span class=\"bjh-strong\">情境性：</span>现代社会强调知识的迁移和运用能力，学生不在是因为知识而学习知识，而是为解决实际生活情境问题而学习知识。知识需与具体生活情境结合。STEM教育的核心就是强调知识的情境性，知识还原于丰富的生活，结合生活中有趣、有挑战的问题，让学生通过解决问题来完成教学。</span></p>\r\n<p><span class=\"bjh-p\">在实际STEM项目设计中，项目的问题一方面要基于真实的生活情景，另一方面又要蕴含所要教授的结构化知识，让学生在贴合实际情境问题的项目中学习知识，有效提高学生将知识进行情境化运用的能力。</span></p>\r\n<p><span class=\"bjh-p\"><span class=\"bjh-strong\">协作性：</span>STEM教育要求以真实生活为出发点，设计贴合情境的问题或项目，在实际操作过程中学习知识。而实际生活环境，问题的解决离不开群体的协作。</span></p>\r\n<p><span class=\"bjh-p\">在STEM教育的设计理念里，要求知识的共同构建，因此课程将采用以小组为单位的设计模式，通过小组之间的交流，讨论，分享共同完成学习资料的收集与分析、提出和验证假设、分享和共建知识，进而整体评价学习成果。</span></p>\r\n<p><span class=\"bjh-p\">STEM强调贴合生活环境，构建具备跨学科，趣味性，情境性，协作性的STEM学习环境，让学习在解决问题中学习，培养学生综合问题解决能力，培养综合性人才。在主流的整合性课程结构中，除STEM教育外，还有以项目为主导的创客教育，下面我们将对创客教育做简单的介绍。</span></p>\r\n<p><span class=\"bjh-p\">提到创客教育，很多时候容易和STEM教育混为一谈，所以在介绍创客教育前，我们先简单的说一下两者之间的联系和区别。</span></p>\r\n<p><span class=\"bjh-p\">首先，STEM教育强调将科学、技术、工程、数字，乃至艺术等基础学科知识融合起来教学，采用问题驱动，培养学生知识应用和迁移的能力。在实际课程实施过程中，强调的是整合性知识的构建与传递。</span></p>\r\n<p><span class=\"bjh-p\">创客教育则是提倡开发自己的创意，通过软硬件将创意实现成具体物品，例如使用3D打印技术实现3D产品构建，利用安卓电子编程制作电子器件。</span></p>\r\n<p><span class=\"bjh-p\">从这两个含义可以很明显的区分，STEAM教育更讲究地是跨学科的&ldquo;知识融合&rdquo;，而创客教育重点在于将想法进行&ldquo;实践创造&rdquo;。但不管是STEM教育还是创客教育，强调的都是知识的自主探索，知识与实际场景或产品相结合，不在是孤立且抽象的存在。</span></p>\r\n<p><span class=\"bjh-p\">两种教育模式的出发点都在于使用整合性知识结构，解决实际生活中所遇到的问题。以培养学生动手能力，沟通能力，协作能力，自主学习能力。所以，在人才培养的目标上两者具有高度的一致。</span></p>\r\n<p><span class=\"bjh-p\">并且创客活动需要STEAM教育提供知识理论基础，而创客实践则是STEAM教育的最终目的，就是知识体系只有帮助实物创造才能发挥作用，甚至成为可商业化的产品，联接教育与商业。因此STEM教育和创客教育两者既有区别，又有联系，相辅相成，共同促进。</span></p>\r\n<p><span class=\"bjh-p\">相对于STEM教育问题驱动模式，创客教育更多的是偏向于项目驱动，通过完整的项目提升学生的动手能力及知识应用能力。例如少儿编程，开源硬件， 传感器等项目。在创客项目中，学生在创客教育的过程中，必将经历&ldquo;调研分析、制定方案、动手制造、评估反思&rdquo;等完整的项目迭代步骤。</span></p>\r\n<p><span class=\"bjh-p\">通过以项目为中心，驱动知识的运用是创客教育的核心点。同时意味着，不同于STEM教育，在项目过程中， 将更关注于具体的项目结果，在评价体系上 ，将以项目驱动成果作为核心指标。</span></p>\r\n<p><span class=\"bjh-p\">总之，不管是STEM教育或是创客教育，与传统课程结构都具有明显的区别。所蕴含的区别代表这未来人才的内在诉求。在信息化时代中，我们需要通过创新的整合式课程结构培养综合性人才。知识不应该是分裂的，应该是有机整合在一起。</span></p>\r\n<p><span class=\"bjh-p\">以创新的教育课程组织方式，坚持&ldquo;学生主体，教师主导&rdquo;，强调人的能动性，激发学生学习兴趣，使学生在生活中学习，在学生中生活，掌握知识迁移和应用能力，不断接收创造知识，在实现个人生活成功的同时，促进社会发展。</span></p>\r\n<p>结语</p>\r\n<p><span class=\"bjh-p\">2011年6月，教育部发布《教育信息化十年发展规划（2011-2020 年）》，明确提出各级政府在教育经费中按不低于 8%的比例列支教育信息化经费，保障教育信息化拥有持续、稳定的政府财政投入。</span></p>\r\n<p><span class=\"bjh-p\">2017 年教育信息化经费支出不低于 2731 亿元，预计到2020年，教育信息化经费支出或将达3863亿元，其中K12占比超过50%。</span></p>\r\n<p><span class=\"bjh-p\">教育信息进程在资金和政策的双重驱动下，近几年来得到飞速的发展。已基本完成以&ldquo;三通两平台&rdquo;为主的《教育信息化1.0行动》，实现教育环境的信息化改造，为教育信息化发展奠定基础互联网环境基础。持续推进以&ldquo;三高，两全，一大&rdquo;为核心的《教育信息化2.0行动》。</span></p>\r\n<p><span class=\"bjh-p\">但在实际推行过程中，我们也遇到不少急需解决的现实问题，例如，因缺乏统一性部署而导致的数据孤岛问题；东部沿海经济发达地区和中西部经济落后地区发展不均衡问题；强调建设却无配套应用平台的问题等。回归到本次所讲述的互联网+课程上，现大部分地区已具备一定程度的互联网条件，可有效实施互联网教学。</span></p>\r\n<p><span class=\"bjh-p\">但是整体课程及教学理念还停留在传统工业课程社会的的模式上，还未跟上互联网的步伐，导致出现有条件不会使用或用不好的尴尬境地。</span></p>\r\n<p><span class=\"bjh-p\">因此，为适应信息化社会对人才核心素养及能力的要求，教育应紧密和互联网结合，以互联网思维改变现有课程结构。在实际课程实施过程中，应强调课程设计需融合线上线上，课程实施应回归生活，课程结构应是整合性的。通过全新的课程设计，课程实施，课程结构理念，改变现有课程组织方式，适应时代发展需求，为培养现代化人才贡献力量。</span></p>\r\n<p><span class=\"bjh-p\">本文由 @老鬼 原创发布于人人都是产品经理。未经许可，禁止转载</span></p>\r\n<p><span class=\"bjh-p\">题图来自 Unsplash，基于 CC0 协议</span></p>', 1, 3, 1583326264, 1, '人人都是产品经理', 1583326264, 1, 1583326264, NULL, '{\"image_intro\":\"\",\"align_intro\":\"\",\"alt_intro\":\"\",\"caption_intro\":\"\",\"image_full\":\"\",\"align_full\":\"\",\"alt_full\":\"\",\"caption_full\":\"\"}', '', '', 0, '', '', 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(4, 'You Have Only One Life', 'you-have-only-one-life', '', '<p>　There are moments in life when you miss someone so much that you just want to pick them from your dreams and hug them for real! Dream what you want to dream;go where you want to go;be what you want to be,because you have only one life and one chance to do all the things you want to do.</p>\r\n<p>　　May you have enough happiness to make you sweet,enough trials to make you strong,enough sorrow to keep you human,enough hope to make you happy? Always put yourself in others&rsquo;shoes.If you feel that it hurts you,it probably hurts the other person, too.</p>\r\n<p>　　The happiest of people don&rsquo;t necessarily have the best of everything;they just make the most of everything that comes along their way.Happiness lies for those who cry,those who hurt, those who have searched,and those who have tried,for only they can appreciate the importance of people</p>\r\n<p>　　who have touched their lives.Love begins with a smile,grows with a kiss and ends with a tear.The brightest future will always be based on a forgotten past, you can&rsquo;t go on well in lifeuntil you let go of your past failures and heartaches.</p>\r\n<p>　　When you were born,you were crying and everyone around you was smiling.Live your life so that when you die,you\'re the one who is smiling and everyone around you is crying.</p>\r\n<p>　　Please send this message to those people who mean something to you,to those who have touched your life in one way or another,to those who make you smile when you really need it,to those that make you see the brighter side of things when you are really down,to those who you want to let them know that you appreciate their friendship.And if you don&rsquo;t, don&rsquo;t worry,nothing bad will happen to you,you will just miss out on the opportunity to brighten someone&rsquo;s day with this message.</p>', 1, 2, 1583326386, 1, '', 1583326386, 1, 1583326386, NULL, '{\"image_intro\":\"\",\"align_intro\":\"\",\"alt_intro\":\"\",\"caption_intro\":\"\",\"image_full\":\"\",\"align_full\":\"\",\"alt_full\":\"\",\"caption_full\":\"\"}', '', '', 0, '', '', 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '', ''),
(5, '为全球公共卫生事业作出贡献（患难见真情 共同抗疫情）', 'wei-quan-qiu-gong-gong-wei-sheng-shi-ye-zuo-chu-gong-xian-huan-nan-jian-zhen-qing-gong-tong-kang-yi-qing', '', '<p>目前，中国新冠肺炎疫情防控形势积极向好的态势正在拓展。在部分国家和地区出现疫情扩散的形势下，中国外交部及驻外使领馆推动开展疫情防控国际合作，发挥我国负责任大国作用，为全球公共卫生事业作出贡献，获得各方高度认同和赞赏。多国政府表示，面对这一全球性挑战，愿与中方保持密切沟通与合作，共同战胜疫情。</p>\r\n<p>　　&ldquo;我们满怀信心，愿与中方携手战胜疫情&rdquo;</p>\r\n<p>　　中国一直密切关注全球新冠肺炎疫情形势，始终秉持构建人类命运共同体理念，本着公开、透明和负责任的态度加强国际防疫合作。</p>\r\n<p>　　3月3日下午，中国外交部会同中国国家卫生健康委举行与阿塞拜疆、白俄罗斯、格鲁吉亚、摩尔多瓦、亚美尼亚、土库曼斯坦及上合组织秘书处新冠肺炎疫情专家视频交流会。来自国家卫健委及外交部、海关总署、中国驻相关国家大使馆代表，中国疾控中心、北大医院等技术机构专家，相关国家外交和卫生部门、驻华外交机构、卫生领域专家及有关国际组织代表参加了视频会议。中方专家全面介绍了中国疫情形势和防控、救治等经验，各方就防控措施、诊断筛查、实验室检测等方面内容进行了深入交流，并表示将进一步加强信息交流和协调行动，共同维护地区和全球卫生安全。</p>\r\n<p>　　中国驻乌干达大使馆日前通过乌干达主流媒体NTV电视台，详细介绍中国疫情形势和防控工作取得的成效，就乌公众高度关注的在武汉留学生安全和疫情对乌干达及乌中关系影响等答疑释惑。乌干达总统穆塞韦尼表示，乌方尊重世卫组织的建议，不会因疫情对国际旅行施加限制。在双方有效沟通下，乌政府有关部门与中国驻乌大使馆保持密切联系，积极向公众公开疫情信息，普及防控知识，帮助乌民众提高卫生防护意识。</p>\r\n<p>　　日本国内疫情形势发生变化后，中国驻日本大使馆加强了同日方的沟通和协调，积极推动双方卫生主管部门及传染病防治机构建立专门沟通合作渠道。日本内阁府官员表示，愿与中方在智慧城市、超级都市合作框架下推进病毒防控及远程诊疗体系等合作。日本自民党干事长二阶俊博说：&ldquo;我深信，日中两国团结协作，一定能够战胜疫情。&rdquo;</p>\r\n<p>　　中国驻墨西哥大使馆同墨西哥外交、卫生、海关、移民、边检等各部门保持密切沟通，及时向墨方通报疫情情况及中方防控举措。墨西哥外长埃布拉德对中方采取的积极举措表示赞赏和感谢，并表示，得益于中方全面、严格的防控措施，在华墨西哥公民的生命安全和身体健康得到充分保障。</p>\r\n<p>　　中国驻尼日利亚大使馆通过多种形式，积极介绍中国抗击疫情举措。尼中地方合作论坛秘书长阿布杜拉蒂夫&middot;谢胡表示：&ldquo;感谢中国透明及时地通报疫情有关情况。疫情当前，中国行动果断、迅速、有效，尼方也将继续向中国学习防疫抗疫经验。我们满怀信心，愿与中方携手战胜疫情。&rdquo;</p>\r\n<p>　　&ldquo;共建一带一路沿线国家和地区更应加强团结合作、共克时艰&rdquo;</p>\r\n<p>　　疫情发生后，很多参与共建&ldquo;一带一路&rdquo;的国家和地区向中国提供支持和帮助，以不同方式表明愿同中方继续加强&ldquo;一带一路&rdquo;合作的意愿。在力所能及范围内，中方向相关国家提供了援助。</p>\r\n<p>　　中国驻意大利大使馆连日来持续向意方通报最新疫情信息。意大利卫生部部长斯佩兰扎表示，希望加强与中方在信息沟通、药物与疫苗研发等方面合作，共同应对挑战。意大利外长迪马约表示，意政府正采取有力举措防控疫情，愿同中方加强沟通与合作，密切两国卫生部门间的信息交流，&ldquo;意方重视并支持共建&lsquo;一带一路&rsquo;。在此艰难时刻，共建&lsquo;一带一路&rsquo;沿线国家和地区更应加强团结合作、共克时艰&rdquo;。</p>\r\n<p>　　据中国驻俄罗斯大使馆介绍，疫情发生后，俄方派遣专家团队赴华联合研制抗病毒药物和疫苗。俄联邦消费者权利保护及公益监督署署长兼国家总防疫师波波娃高度评价中方为抗击疫情所作巨大努力和取得的重要成果。她强调，面对汹涌疫情，俄中是伙伴、是战友，俄方从中方抗击疫情的斗争中学到很多宝贵经验，实践证明中方采取的防疫举措切实有效。她预祝中国政府和人民抗击疫情斗争取得&ldquo;完胜&rdquo;。</p>\r\n<p>　　中国驻克罗地亚大使馆通过当地媒体介绍中国防控疫情有关情况，指出疫情对中国经济的影响是阶段性的、暂时性的，不会改变中国经济长期向好的基本面，引发当地舆论关注和积极反响。</p>\r\n<p>　　为支持巴基斯坦疫情防控工作，针对巴方医疗力量薄弱等情况，中方向巴方捐赠了一批新冠病毒核酸检测试剂盒。巴国家粮食安全与研究部部长马赫杜姆&middot;胡斯鲁&middot;巴赫蒂亚尔表示，无论何时，巴中两国总是相互支持，体现了巴中之间的深厚友谊。</p>\r\n<p>　　&ldquo;希望进一步研究借鉴中方的有益做法和经验&rdquo;</p>\r\n<p>　　中国驻纽约总领馆通过多种方式积极介绍中国疫情防控有关举措。美国全国广播公司所属康卡斯特集团高级副总裁、首席财务官迈克尔&middot;卡瓦纳表示，中国政府和人民在抗击疫情中展现出的团结和努力令人称赞，为各国抗击疫情积累了宝贵经验。&ldquo;北京环球影城主题公园项目目前进展顺利，我们对中国经济前景充满信心。美国全国广播公司将继续致力于客观、平衡地报道中国疫情防控努力。&rdquo;</p>\r\n<p>　　中国驻欧盟使团主动向欧方介绍有关情况，并多次组织中欧双方公共卫生专家举行视频会议，共同研究应对疫情，探讨进一步就疫情诊断治疗、科学信息共享、医学临床实验等开展合作。欧方感谢中方及时通报有关情况，表示注意到世卫组织充分肯定中国政府采取的防控措施及成效，&ldquo;欧方愿继续与中方保持密切沟通，希望进一步研究借鉴中方的有益做法和经验&rdquo;。</p>\r\n<p>　　中国驻里约热内卢总领馆与领区内各州、里约热内卢市卫生主管部门及主流媒体建立信息通报机制，定期通报中国疫情防控情况。里约热内卢州政府代表迪亚斯表示，疫情发生后，中方做了大量及时有效工作，遏制了疫情蔓延。&ldquo;中国展现了负责任大国担当。感谢中方提供的有关疫情信息，疫情不会影响两国人民的情谊。&rdquo;</p>\r\n<p>　　中国驻南非大使馆举行多场新闻发布会，邀请南非国内及国际媒体参加，介绍中国抗击疫情进展和中非在相关领域合作情况。南非太阳城度假酒店总经理布莱特&middot;霍普说：&ldquo;中国疫情防控工作取得积极成效，整体形势稳定向好，各地有序复工复产。我们已做好充分准备，在疫情结束之后迎接更多来自中国的游客。&rdquo;</p>\r\n<p>　　（本报北京、东京、墨西哥城、阿布贾、罗马、莫斯科、伊斯兰堡、纽约、布鲁塞尔、约翰内斯堡3月5日电&nbsp;&nbsp;记者颜欢、刘军国、刘旭霞、姜宣、叶琦、孔歌、张晓东、丁雪真、李凉、方莹馨、万宇、吕强）</p>', 1, 4, 1583477876, 1, '', 1583477876, 1, 1583477876, NULL, '{\"image_intro\":\"\",\"align_intro\":\"\",\"alt_intro\":\"\",\"caption_intro\":\"\",\"image_full\":\"\",\"align_full\":\"\",\"alt_full\":\"\",\"caption_full\":\"\"}', '', '', 0, '', '', 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, 'zh-CN', '', '');

-- --------------------------------------------------------

--
-- 表的结构 `y_extensions`
--

CREATE TABLE `y_extensions` (
  `id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` json NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) DEFAULT '0',
  `status` smallint(6) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_extensions`
--

INSERT INTO `y_extensions` (`id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `protected`, `manifest_cache`, `params`, `ordering`, `status`) VALUES
(1, 0, 'English United States Language Pack', 'package', 'pkg_en-US', '', 0, 1, 1, '{\"name\": \"English United States Language Pack\", \"type\": \"package\", \"group\": \"\", \"author\": \"FireLoong www.fireloong.com\", \"version\": \"1.0.0\", \"filename\": \"pkg_zh-US\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"\\n\\t    Yii2CMS v1.0.0 English (United States) Language Package\\n    \", \"creationDate\": \"July 2019\"}', '', 0, 1),
(2, 1, 'English (en-US)', 'language', 'en-US', '', 2, 1, 1, '{\"name\": \"English (en-US)\", \"type\": \"language\", \"group\": \"\", \"author\": \"FireLoong www.fireloong.com\", \"version\": \"1.0.0\", \"filename\": \"install\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"en-US site language\", \"creationDate\": \"July 2019\"}', '', 0, 1),
(3, 1, 'English (en-US)', 'language', 'en-US', '', 1, 1, 1, '{\"name\": \"English (en-US)\", \"type\": \"language\", \"group\": \"\", \"author\": \"FireLoong www.fireloong.com \", \"version\": \"1.0.0\", \"filename\": \"install\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"en-US Administrator language\", \"creationDate\": \"July 2019\"}', '', 0, 1),
(4, 0, 'Chinese Simplified (zh-CN) Language Pack', 'package', 'pkg_zh-CN', '', 0, 1, 1, '{\"name\": \"Chinese Simplified (zh-CN) Language Pack\", \"type\": \"package\", \"group\": \"\", \"author\": \"FireLoong 火龙网 www.fireloong.com\", \"version\": \"1.0.0\", \"filename\": \"pkg_zh-CN\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong火龙网 www.fireloong.com.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"Yii2CMS 简体中文(zh-CN) 语言\", \"creationDate\": \"2019年7月\"}', '', 0, 1),
(5, 4, 'Chinese Simplified (zh-CN)', 'language', 'zh-CN', '', 2, 1, 1, '{\"name\": \"Chinese Simplified (zh-CN)\", \"type\": \"language\", \"group\": \"\", \"author\": \"FireLoong 火龙网 www.fireloong.com\", \"version\": \"1.0.0\", \"filename\": \"install\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong 火龙网 www.fireloong.com\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"zh-CN 网站前台简体中文语言\", \"creationDate\": \"2019年7月\"}', '', 0, 1),
(6, 4, 'Chinese Simplified (zh-CN)', 'language', 'zh-CN', '', 1, 1, 1, '{\"name\": \"Chinese Simplified (zh-CN)\", \"type\": \"language\", \"group\": \"\", \"author\": \"FireLoong 火龙网 www.fireloong.com \", \"version\": \"1.0.0\", \"filename\": \"install\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong 火龙网 www.fireloong.com\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"zh-CN 管理后台简体中文语言\", \"creationDate\": \"2019年7月\"}', '', 0, 1),
(7, 0, 'Admin', 'module', 'mod_admin', '', 1, 1, 1, '{\"name\": \"Admin\", \"type\": \"module\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"rbac-admin\", \"version\": \"1.0.0\", \"filename\": \"admin\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong火龙网 www.fireloong.com.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"RBAC Manager for Yii 2\", \"creationDate\": \"2019 - 09\"}', '', 0, 1),
(8, 0, 'LANGUAGES', 'module', 'mod_languages', '', 1, 1, 1, '{\"name\": \"LANGUAGES\", \"type\": \"module\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"mod_langs\", \"version\": \"1.0.1\", \"filename\": \"languages\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong火龙网 www.fireloong.com.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"LANGUAGES_XML_DESCRIPTION\", \"creationDate\": \"2019 - 08\"}', '{\"backend\":\"zh-CN\",\"frontend\":\"zh-CN\"}', 0, 1),
(9, 0, 'ACTIONS_LOGS', 'module', 'mod_actionlogs', '', 1, 1, 1, '{\"name\": \"ACTIONS_LOGS\", \"type\": \"module\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"mod_actionlogs\", \"version\": \"1.0.0\", \"filename\": \"actionlogs\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong火龙网 www.fireloong.com.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"ACTIONS_LOGS_XML_DESCRIPTION\", \"creationDate\": \"2019 - 08\"}', '', 0, 1),
(10, 0, 'ACTION_LOG_NAME', 'plugin', 'main', 'actionlog', 0, 1, 1, '{\"name\": \"ACTION_LOG_NAME\", \"type\": \"plugin\", \"group\": \"actionlog\", \"author\": \"FireLoong\", \"langCat\": \"plg_actionlog\", \"version\": \"1.0.0\", \"filename\": \"main\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"ACTION_LOG_XML_DESCRIPTION\", \"creationDate\": \"July 2019\"}', '', 0, 1),
(11, 0, 'EDITORS_NONE', 'plugin', 'none', 'editors', 0, 1, 1, '{\"name\": \"EDITORS_NONE\", \"type\": \"plugin\", \"group\": \"editors\", \"author\": \"FireLoong\", \"langCat\": \"plg_none\", \"version\": \"1.0.0\", \"filename\": \"none\", \"authorUrl\": \"www.fireloong.com/\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"XML_DESCRIPTION\", \"creationDate\": \"2019 - 11\"}', '', 0, 1),
(12, 0, 'EDITORS_TINYMCE', 'plugin', 'tinymce', 'editors', 0, 1, 1, '{\"name\": \"EDITORS_TINYMCE\", \"type\": \"plugin\", \"group\": \"editors\", \"author\": \"Tiny Technologies, Inc\", \"langCat\": \"plg_tinymce\", \"version\": \"5.1.1\", \"filename\": \"tinymce\", \"authorUrl\": \"https://www.tiny.cloud\", \"copyright\": \"Tiny Technologies, Inc\", \"authorEmail\": \"N/A\", \"description\": \"XML_DESCRIPTION\", \"creationDate\": \"2005-2019\"}', '{\"sets_amount\":\"3\",\"height\":\"350\",\"width\":\"100%\"}', 0, 1),
(13, 0, 'THEME_BASIC', 'theme', 'basic', '', 1, 1, 1, '{\"name\": \"THEME_BASIC\", \"type\": \"theme\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"theme_basic\", \"version\": \"1.0.0\", \"filename\": \"themeDetails\", \"authorUrl\": \"\", \"copyright\": \"Copyright (C) 2019 - 2019 Open Source Matters, Inc. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"THEME_XML_DESCRIPTION\", \"creationDate\": \"2019/10/10\"}', '{\"showSiteName\":\"1\"}', 0, 1),
(14, 0, 'THEME_ADMINLTE', 'theme', 'adminlte', '', 1, 1, 0, '{\"name\": \"THEME_ADMINLTE\", \"type\": \"theme\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"theme_adminlte\", \"version\": \"1.0.0\", \"filename\": \"themeDetails\", \"authorUrl\": \"https://adminlte.io/\", \"copyright\": \"Copyright (C) 2019 - 2019 Open Source Matters, Inc. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"THEME_XML_DESCRIPTION\", \"creationDate\": \"2019/10/10\"}', '{\"showSiteName\":\"1\"}', 0, 1),
(15, 0, 'WIDGET_ALERT', 'widget', 'alert', '', 1, 1, 1, '{\"name\": \"WIDGET_ALERT\", \"type\": \"widget\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"wid_alert\", \"version\": \"1.0.0\", \"filename\": \"widget\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"WIDGET_ALERT_XML_DESCRIPTION\", \"creationDate\": \"2019/12/07\"}', '', 0, 1),
(16, 0, 'WIDGET_LATEST_ACTIONS', 'widget', 'latest_actions', '', 1, 1, 1, '{\"name\": \"WIDGET_LATEST_ACTIONS\", \"type\": \"widget\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"wid_latest_actions\", \"version\": \"1.0.0\", \"filename\": \"widget\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"Copyright (C) 2005 - 2019 Open Source Matters. All rights reserved.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"WIDGET_LATEST_ACTIONS_XML_DESCRIPTION\", \"creationDate\": \"2019/12/07\"}', '{\"count\":\"5\",\"bootstrap_size\":\"6\"}', 0, 1),
(17, 0, 'CONTENT', 'module', 'mod_content', '', 1, 1, 1, '{\"name\": \"CONTENT\", \"type\": \"module\", \"group\": \"\", \"author\": \"FireLoong\", \"langCat\": \"mod_content\", \"version\": \"1.0.0\", \"filename\": \"content\", \"authorUrl\": \"www.fireloong.com\", \"copyright\": \"版权所有 (C) 2005 - 2019 FireLoong火龙网 www.fireloong.com.\", \"authorEmail\": \"fireloong@foxmail.com\", \"description\": \"CONTENT_XML_DESCRIPTION\", \"creationDate\": \"2020 - 01\"}', '', 0, 1);

-- --------------------------------------------------------

--
-- 表的结构 `y_languages`
--

CREATE TABLE `y_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` char(7) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_languages`
--

INSERT INTO `y_languages` (`id`, `code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `ordering`) VALUES
(1, 'en-US', 'English (en-US)', 'English (United States)', 'en', 'en_us', '', '', '', '', 1, 0),
(2, 'zh-CN', 'Chinese Simplified (zh-CN)', '简体中文(中国)', 'zh', 'zh_cn', '说明', '开源,OSC,开源软件,开源硬件,开源网站,开源社区,java开源,perl开源,python开源,ruby开源,php开源,开源项目,开源代码', 'OSCHINA.NET 是目前领先的中文开源技术社区。我们传播开源的理念，推广开源项目，为 IT 开发者提供了一个发现、使用、并交流开源技术的平台', '中文开源技术交流社区', 1, 0);

-- --------------------------------------------------------

--
-- 表的结构 `y_member`
--

CREATE TABLE `y_member` (
  `uid` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lastvisitDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_member`
--

INSERT INTO `y_member` (`uid`, `name`, `lastvisitDate`) VALUES
(2, '', 1578716377);

-- --------------------------------------------------------

--
-- 表的结构 `y_menu`
--

CREATE TABLE `y_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `menutype` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `parent` int(11) DEFAULT NULL,
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `request` text COLLATE utf8_unicode_ci NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `browserNav` tinyint(1) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `lft` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `rgt` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `home` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_menu`
--

INSERT INTO `y_menu` (`id`, `name`, `menutype`, `alias`, `note`, `path`, `type`, `published`, `parent`, `level`, `route`, `request`, `params`, `browserNav`, `language`, `lft`, `rgt`, `ordering`, `home`, `data`) VALUES
(1, 'SYSTEM', 'main', '', '', '', 'heading', 1, NULL, 0, '', '', '', 0, '*', 0, 17, 0, 0, 0x7b226c616e672d636174223a2273697465227d),
(2, 'CONTROL_PANEL', 'main', '', '', '', 'module', 1, 1, 1, '@app-backend/site/index', '', '', 0, '*', 1, 2, 0, 1, 0x7b226c616e672d636174223a2273697465227d),
(3, '-', 'main', '', '', '', 'separator', 1, 1, 1, '', '', '', 0, '*', 3, 4, 0, 0, NULL),
(4, 'GLOBAL_CONFIGURATION', 'main', '', '', '', 'module', 1, 1, 1, '', '', '', 0, '*', 5, 6, 0, 0, 0x7b226c616e672d636174223a2273697465227d),
(5, '-', 'main', '', '', '', 'separator', 1, 1, 1, '', '', '', 0, '*', 7, 8, 0, 0, NULL),
(6, 'CLEAR_CACHE', 'main', '', '', '', 'module', 1, 1, 1, '@app-backend/cache/index', '', '', 0, '*', 9, 10, 0, 0, 0x7b226c616e672d636174223a226361636865227d),
(7, 'USERS', 'main', '', '', '', 'heading', 1, NULL, 0, '', '', '', 0, '*', 30, 37, 1, 0, 0x7b226c616e672d636174223a2273697465227d),
(8, 'USER_MANAGER', 'main', '', '', '', 'module', 1, 7, 1, '@app-backend/admin/user/index', '', '', 0, '*', 31, 34, 0, 0, 0x7b226c616e672d636174223a2273697465227d),
(9, 'USERS_ADD_USER', 'main', '', '', '', 'module', 1, 8, 2, '@app-backend/admin/user/signup', '', '', 0, '*', 32, 33, 0, 0, 0x7b226c616e672d636174223a2273697465227d),
(10, 'MODULES', 'main', '', '', '', 'heading', 1, NULL, 0, '', '', '', 0, '*', 42, 47, 4, 0, 0x7b226c616e672d636174223a2273697465227d),
(12, 'EXTENSIONS', 'main', '', '', '', 'heading', 1, NULL, 0, '', '', '', 0, '*', 48, 71, 5, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(13, 'EXTENSIONS_MANAGER', 'main', '', '', '', 'module', 1, 12, 1, '@app-backend/extensions/installer', '', '', 0, '*', 49, 60, 0, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(14, 'EXTENSIONS_INSTALL', 'main', '', '', '', 'module', 1, 13, 2, '@app-backend/extensions/installer', '', '', 0, '*', 50, 51, 0, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(15, 'EXTENSIONS_UPDATE', 'main', '', '', '', 'module', 1, 13, 2, '@app-backend/extensions/update', '', '', 0, '*', 52, 53, 0, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(16, 'EXTENSIONS_MANAGE', 'main', '', '', '', 'module', 1, 13, 2, '@app-backend/extensions/manage', '', '', 0, '*', 54, 55, 0, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(17, 'DATABASE', 'main', '', '', '', 'module', 1, 13, 2, '', '', '', 0, '*', 56, 57, 0, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(18, 'LANGUAGES_INSTALL', 'main', '', '', '', 'module', 1, 13, 2, '', '', '', 0, '*', 58, 59, 1, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(19, 'WIDGETS', 'main', '', '', '', 'module', 1, 12, 1, '@app-backend/widgets/default/index', '', '', 0, '*', 63, 64, 1, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(20, 'PLUGINS', 'main', '', '', '', 'module', 1, 12, 1, '@app-backend/plugins/default/index', '', '', 0, '*', 65, 66, 1, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(21, 'THEMES', 'main', '', '', '', 'module', 1, 12, 1, '@app-backend/themes/styles/index', '', '', 0, '*', 67, 68, 1, 0, 0x7b226c616e672d636174223a22657874656e73696f6e73227d),
(22, '-', 'main', '', '', '', 'separator', 1, 12, 1, '', '', '', 0, '*', 61, 62, 0, 0, NULL),
(23, 'Admin', 'main', '', '', '', 'module', 1, 10, 1, '@app-backend/admin/assignment/index', '', '', 0, '*', 43, 44, 0, 0, 0x7b226d6f64756c652d6964223a372c226c616e672d636174223a22726261632d61646d696e227d),
(24, 'LANGUAGES', 'main', '', '', '', 'module', 1, 12, 1, '@app-backend/languages/installed/index', '', '', 0, '*', 69, 70, 1, 0, 0x7b226d6f64756c652d6964223a382c226c616e672d636174223a226d6f645f6c616e6773227d),
(25, 'ACTIONS_LOGS', 'main', '', '', '', 'module', 1, 7, 1, '@app-backend/actionlogs/default/index', '', '', 0, '*', 35, 36, 0, 0, 0x7b226d6f64756c652d6964223a392c226c616e672d636174223a226d6f645f616374696f6e6c6f6773227d),
(26, 'CLIENTS', 'main', '', '', '', 'module', 1, 1, 1, '@app-backend/clients/index', '', '', 0, '*', 11, 12, 0, 0, 0x7b226c616e672d636174223a22636c69656e7473227d),
(27, 'MEDIA', 'main', '', '', '', 'module', 1, 10, 1, '@app-backend/media/default/index', '', '', 0, '*', 45, 46, 0, 0, 0x7b226c616e672d636174223a226d6f645f6d65646961227d),
(28, '-', 'main', '', '', '', 'separator', 1, 1, 1, '', '', '', 0, '*', 13, 14, 0, 0, NULL),
(29, 'SYSTEM_INFORMATION', 'main', '', '', '', 'module', 1, 1, 1, '@app-backend/site/sysinfo', '', '', 0, '*', 15, 16, 0, 0, 0x7b226c616e672d636174223a2273697465227d),
(30, 'CONTENT', 'main', '', '', '', 'heading', 1, NULL, 0, '', '', '', 0, '*', 38, 41, 3, 0, 0x7b226c616e672d636174223a226d6f645f636f6e74656e74227d),
(31, 'ARTICLES', 'main', '', '', '', 'module', 1, 30, 1, '@app-backend/content/articles/index', '', '', 0, '*', 39, 40, 0, 0, 0x7b226c616e672d636174223a226d6f645f636f6e74656e74227d),
(32, '首页', 'mainmenu', 'home', '', 'home', 'module', 1, NULL, 0, '@app-frontend/site/index', '', '{\"template\":\"index\"}', 0, 'zh-CN', 18, 19, 0, 1, NULL),
(33, '关于我们', 'mainmenu', 'about', '', 'about', 'module', 1, NULL, 0, '@app-frontend/content/default/view', '{\"id\":\"2\"}', '', 0, 'zh-CN', 20, 25, 0, 0, NULL),
(34, '百度', 'mainmenu', 'baidu', '', 'baidu', 'url', 1, NULL, 0, 'http://www.baidu.com', '', '', 1, 'zh-CN', 26, 27, 0, 0, NULL),
(35, 'dfhh', 'mainmenu', 'test', '', 'about/test', 'module', 1, 33, 1, '@app-frontend/content/default/view', '{\"id\":\"3\"}', '', 0, '*', 21, 24, 0, 0, NULL),
(36, 'aaa', 'mainmenu', 'aaa', '', 'about/test/aaa', 'module', 1, 35, 2, '@app-frontend/content/default/view', '{\"id\":\"5\"}', '', 0, '*', 22, 23, 0, 0, NULL),
(37, 'Home', 'mainmenu', 'home', '', 'home', 'module', 1, NULL, 0, '@app-frontend/site/index', '', '{\"template\":\"index\"}', 0, 'en-US', 28, 29, 0, 1, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `y_menu_types`
--

CREATE TABLE `y_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `menutype` varchar(24) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_menu_types`
--

INSERT INTO `y_menu_types` (`id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 'main', 'BACKEND_MAIN_MENU_LABEL', 'BACKEND_MAIN_MENU_DESC', 1),
(2, 'mainmenu', 'FRONTEND_MAIN_MENU_LABEL', 'FRONTEND_MAIN_MENU_DESC', 2),
(3, 'sfgf', 'dfdf', '', 1),
(4, 'sfgfsdf', 'dfdf', '', 2);

-- --------------------------------------------------------

--
-- 表的结构 `y_migration`
--

CREATE TABLE `y_migration` (
  `version` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_migration`
--

INSERT INTO `y_migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1565703587),
('m130524_201442_init', 1565704366),
('m140506_102106_rbac_init', 1565704398),
('m140602_111327_create_menu_table', 1565704414),
('m160312_050000_create_user', 1565704414),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1565704399),
('m180523_151638_rbac_updates_indexes_without_prefix', 1565704399),
('m190124_110200_add_verification_token_column_to_user_table', 1565704366),
('m190531_064329_admin', 1565704367),
('m190601_090302_member', 1565704368),
('m190620_012148_extensions', 1565704368),
('m190709_095834_languages', 1565704369),
('m190713_104212_clients', 1565704369),
('m190803_130244_menu_types', 1565704414);

-- --------------------------------------------------------

--
-- 表的结构 `y_overrides`
--

CREATE TABLE `y_overrides` (
  `id` int(11) NOT NULL,
  `override_key` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `override_value` text COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `language` char(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_override` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_overrides`
--

INSERT INTO `y_overrides` (`id`, `override_key`, `override_value`, `category`, `language`, `client_id`, `type`, `is_override`) VALUES
(1, 'SUPER_ADMINISTRATOR', '超级管理员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(2, 'SUPER_ADMINISTRATOR_DESCRIPTION', '有全部权限的管理员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(3, 'REGISTERED', '注册会员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(4, 'REGISTERED_DESCRIPTION', '已经注册的用户', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(5, 'BACKEND_MAIN_MENU_DESC', '后台主菜单', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(6, 'BACKEND_MAIN_MENU_LABEL', '主菜单', 'rbac-admin', 'zh-CN', 1, 'menu-types', 1),
(9, 'FRONTEND_MAIN_MENU_LABEL', '主菜单', 'rbac-admin', 'zh-CN', 1, 'menu-types', 1),
(10, 'FRONTEND_MAIN_MENU_DESC', '前台主菜单', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(11, 'ROLE_PUBLIC', '公共角色', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(12, 'ROLE_PUBLIC_DESCRIPTION', '最低权限的角色', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(13, 'ROLE_SUPER_ADMINISTRATOR', '超级管理员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(14, 'ROLE_SUPER_ADMINISTRATOR_DESCRIPTION', '有全部权限的管理员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(15, 'ROLE_REGISTERED', '注册会员', 'rbac-admin', 'zh-CN', 1, NULL, 1),
(16, 'ROLE_REGISTERED_DESCRIPTION', '已经注册的用户', 'rbac-admin', 'zh-CN', 1, NULL, 1);

-- --------------------------------------------------------

--
-- 表的结构 `y_theme_styles`
--

CREATE TABLE `y_theme_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_theme_styles`
--

INSERT INTO `y_theme_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(1, 'basic', 1, 1, 'BASIC_TITLE', '{\"showSiteName\":\"1\"}'),
(2, 'adminlte', 1, 0, 'ADMINLTE_TITLE', '{\"showSiteName\":\"1\"}');

-- --------------------------------------------------------

--
-- 表的结构 `y_user`
--

CREATE TABLE `y_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_user`
--

INSERT INTO `y_user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', '0sbDtzB03MoPn_Ez4IHQmaq-HyxRCCE-', '$2y$13$BWBb78DeEBiJ.oU7hN8iUeYIyUBmI4yBnu89ZHAAy0Co1yWcPLwbG', NULL, 'admin@backend.com', 10, 1565836586, 1576672377, NULL),
(2, 'demo', 'IrAgIbMiFExdzG98Vce_S18lRSwTLvSB', '$2y$13$kM.MHsIeSNIUxHQNpPvkXuZVNWkTC6/krXPmJjo8.oNrvvSNjzcVO', NULL, 'demo@frontend.com', 10, 1565836609, 1565836609, 'OROS8yA1pqomUWAwBczIw2x2bhzyhw0P_1565836609'),
(3, 'loong', 'wJG_p3n6uo13ezk9O85K71RaHGI1tZyH', '$2y$13$t1irQ43yYHA6idXLE3whB.K0UBYcV//E0P54fhx2L7e4ZY1kn0OiK', NULL, 'loong@admin.com', 10, 1576409189, 1576494676, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `y_widgets`
--

CREATE TABLE `y_widgets` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `publish_up` int(11) DEFAULT NULL,
  `publish_down` int(11) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `widget` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- 转存表中的数据 `y_widgets`
--

INSERT INTO `y_widgets` (`id`, `title`, `note`, `content`, `ordering`, `position`, `publish_up`, `publish_down`, `published`, `widget`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, '信息提示框', '', '', 0, 'content-header', NULL, NULL, 1, 'alert', 0, '', 1, '*'),
(2, '操作日志 - 最新操作', '', '', 0, 'cpanel', NULL, NULL, 1, 'latest_actions', 1, '{\"count\":\"5\",\"bootstrap_size\":\"6\"}', 1, '*');

--
-- 转储表的索引
--

--
-- 表的索引 `y_action_logs`
--
ALTER TABLE `y_action_logs`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `y_admin`
--
ALTER TABLE `y_admin`
  ADD UNIQUE KEY `uid` (`uid`);

--
-- 表的索引 `y_auth_assignment`
--
ALTER TABLE `y_auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `y_idx-auth_assignment-user_id` (`user_id`);

--
-- 表的索引 `y_auth_item`
--
ALTER TABLE `y_auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `y_idx-auth_item-type` (`type`);

--
-- 表的索引 `y_auth_item_child`
--
ALTER TABLE `y_auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- 表的索引 `y_auth_rule`
--
ALTER TABLE `y_auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- 表的索引 `y_categories`
--
ALTER TABLE `y_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- 表的索引 `y_clients`
--
ALTER TABLE `y_clients`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- 表的索引 `y_content`
--
ALTER TABLE `y_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_alias` (`alias`),
  ADD KEY `idx_status` (`status`) USING BTREE,
  ADD KEY `idx_xreference` (`xreference`) USING BTREE;

--
-- 表的索引 `y_extensions`
--
ALTER TABLE `y_extensions`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `y_languages`
--
ALTER TABLE `y_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_code` (`code`);

--
-- 表的索引 `y_member`
--
ALTER TABLE `y_member`
  ADD UNIQUE KEY `uid` (`uid`);

--
-- 表的索引 `y_menu`
--
ALTER TABLE `y_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent` (`parent`),
  ADD KEY `idx_left_right` (`lft`,`rgt`);

--
-- 表的索引 `y_menu_types`
--
ALTER TABLE `y_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menutype` (`menutype`);

--
-- 表的索引 `y_migration`
--
ALTER TABLE `y_migration`
  ADD PRIMARY KEY (`version`);

--
-- 表的索引 `y_overrides`
--
ALTER TABLE `y_overrides`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `y_theme_styles`
--
ALTER TABLE `y_theme_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_client_id` (`client_id`),
  ADD KEY `idx_client_id_home` (`client_id`,`home`);

--
-- 表的索引 `y_user`
--
ALTER TABLE `y_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- 表的索引 `y_widgets`
--
ALTER TABLE `y_widgets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`),
  ADD KEY `newsfeeds` (`widget`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `y_action_logs`
--
ALTER TABLE `y_action_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=264;

--
-- 使用表AUTO_INCREMENT `y_categories`
--
ALTER TABLE `y_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `y_clients`
--
ALTER TABLE `y_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `y_content`
--
ALTER TABLE `y_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `y_extensions`
--
ALTER TABLE `y_extensions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- 使用表AUTO_INCREMENT `y_languages`
--
ALTER TABLE `y_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `y_menu`
--
ALTER TABLE `y_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- 使用表AUTO_INCREMENT `y_menu_types`
--
ALTER TABLE `y_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `y_overrides`
--
ALTER TABLE `y_overrides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- 使用表AUTO_INCREMENT `y_theme_styles`
--
ALTER TABLE `y_theme_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `y_user`
--
ALTER TABLE `y_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `y_widgets`
--
ALTER TABLE `y_widgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 限制导出的表
--

--
-- 限制表 `y_admin`
--
ALTER TABLE `y_admin`
  ADD CONSTRAINT `fk-admin-uid` FOREIGN KEY (`uid`) REFERENCES `y_user` (`id`) ON DELETE CASCADE;

--
-- 限制表 `y_auth_assignment`
--
ALTER TABLE `y_auth_assignment`
  ADD CONSTRAINT `y_auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `y_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `y_auth_item`
--
ALTER TABLE `y_auth_item`
  ADD CONSTRAINT `y_auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `y_auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- 限制表 `y_auth_item_child`
--
ALTER TABLE `y_auth_item_child`
  ADD CONSTRAINT `y_auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `y_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `y_auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `y_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- 限制表 `y_member`
--
ALTER TABLE `y_member`
  ADD CONSTRAINT `fk-member-uid` FOREIGN KEY (`uid`) REFERENCES `y_user` (`id`) ON DELETE CASCADE;

--
-- 限制表 `y_menu`
--
ALTER TABLE `y_menu`
  ADD CONSTRAINT `y_menu_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `y_menu` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
