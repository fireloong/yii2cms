<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php'
//    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-api',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser'
            ]
        ],
        'response' => [
            'class' => \yii\web\Response::class,
            'on beforeSend' => function($event) {
                $response = $event->sender;
                if ($response->data !== null && !empty(Yii::$app->request->get('suppress_response_code'))) {
                    $response->data = [
                        'success' => $response->isSuccessful,
                        'data' => $response->data
                    ];
                    $response->statusCode = 200;
                }
            },
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => yii\web\JsonResponseFormatter::class,
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES || JSON_UNESCAPED_UNICODE
                ]
            ]
        ],
        'user' => [
            'identityClass' => 'api\models\User',
            'enableSession' => false,
            'enableAutoLogin' => true,
            //'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
        ],
//        'session' => [
//            // this is the name of the session cookie used for login on the api
//            'name' => 'advanced-api',
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => null,
            'rules' => [
                [
                    'class' => yii\rest\UrlRule::class,
                    'controller' => 'user'
                ],
                'PUT,PATCH /<controller>/update/<id>' => '/<controller>/update',
                'DELETE /<controller>/delete/<id>' => '/<controller>/delete',
                'GET,HEAD /<controller>/view/<id>' => '/<controller>/view',
                'POST /<controller>/create' => '/<controller>/create',
                'GET,HEAD /<controller>/index' => '/<controller>/index',
                '/<controller>/options/<id>' => '/<controller>/options',
                '/<controller>/options' => '/<controller>/options'
            ],
        ],
    ],
    'params' => $params,
];
