<?php


namespace api\controllers;

use Yii;
use yii\rest\ActiveController;

class SiteController extends ActiveController
{
    public $modelClass = 'api\models\Admin';
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items'
    ];

    public function actionIndex()
    {
        $admins = \api\models\Admin::find()->all();
        return $admins;
    }

    public function actionError()
    {
        $response = Yii::$app->response;
        $error = [
            'statusCode' => $response->statusCode,
            'statusText' => $response->statusText,
            'message' => Yii::t('yii', 'Page not found.')
        ];
        return $error;
    }
}
