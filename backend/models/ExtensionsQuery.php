<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Extensions]].
 *
 * @see Extensions
 */
class ExtensionsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Extensions[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Extensions|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
