<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%languages}}".
 *
 * @property int $id
 * @property string $code
 * @property string $title
 * @property string $title_native
 * @property string $sef
 * @property string $image
 * @property string $description
 * @property string $metakey
 * @property string $metadesc
 * @property string $sitename
 * @property int $published
 * @property int $ordering
 */
class Languages extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%languages}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'title', 'title_native', 'sef', 'image'], 'required'],
            [['metakey', 'metadesc'], 'string'],
            [['published', 'ordering'], 'integer'],
            [['code'], 'string', 'max' => 7],
            [['title', 'title_native', 'sef', 'image'], 'string', 'max' => 50],
            [['title', 'title_native', 'sitename', 'description', 'metakey', 'metadesc'], 'trim'],
            [['description'], 'string', 'max' => 512],
            [['sitename'], 'string', 'max' => 1024],
            [['code', 'sef'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('languages', 'ID'),
            'code' => Yii::t('languages', 'LANGUAGE_TAG'),
            'title' => Yii::t('languages', 'TITLE'),
            'title_native' => Yii::t('languages', 'TITLE_NATIVE'),
            'sef' => Yii::t('languages', 'SEF'),
            'image' => Yii::t('languages', 'IMAGE'),
            'description' => Yii::t('languages', 'DESCRIPTION'),
            'metakey' => Yii::t('languages', 'METAKEY'),
            'metadesc' => Yii::t('languages', 'METADESC'),
            'sitename' => Yii::t('languages', 'SITENAME'),
            'published' => Yii::t('languages', 'PUBLISHED'),
            'ordering' => Yii::t('languages', 'ORDER'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return LanguagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LanguagesQuery(get_called_class());
    }

}
