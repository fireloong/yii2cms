<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%clients}}".
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string $path
 * @property string $description
 * @property int $ordering
 * @property int $status
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%clients}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['ordering', 'status'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [['url'], 'url'],
            [['url'], 'string', 'max' => 1024],
            [['path'], 'string', 'max' => 200],
            [['description'], 'string', 'max' => 255],
            [['ordering', 'status'], 'default', 'value' => 0],
            [['name'], 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('clients', 'ID'),
            'name' => Yii::t('clients', 'NAME'),
            'url' => Yii::t('clients', 'URL'),
            'path' => Yii::t('clients', 'PATH'),
            'description' => Yii::t('clients', 'DESCRIPTION'),
            'ordering' => Yii::t('common', 'ORDER'),
            'status' => Yii::t('common', 'STATUS'),
        ];
    }

    /**
     * 应用列表
     * @param array $firstItem 第一项数组，如：[''=>'请选择应用']
     * @param string $key 应用键值
     * @param string $value 应用显示的值
     * @return array 返回所有有效的应用数组
     */
    public static function getList($firstItem = [], $key = 'id', $value = 'name')
    {
        $clients = static::find()->where(['status' => 1])->orderBy('ordering')->all();
        if (empty($firstItem)) {
            $list = [];
        } else {
            foreach ($firstItem as $k => $val) {
                $list[$k] = $val;
            }
        }

        foreach ($clients as $client) {
            $list[$client->$key] = $client->$value;
        }
        return $list;
    }

}
