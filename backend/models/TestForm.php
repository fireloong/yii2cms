<?php


namespace backend\models;

use Yii;
use yii\base\Model;

/**
 * description of TestForm
 *
 * @author FireLoong
 */
class TestForm extends Model
{
    public $title;
    public $author;
    public $content;
    public $date;

    public function rules()
    {
        return [
            [['title', 'content'], 'required'],
            [['title'],'string','max' => 10],
            [['author', 'date'], 'safe']
        ];
    }
}
