<?php

namespace backend\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Extensions as ExtensionsModel;

/**
 * Extensions represents the model behind the search form of `backend\models\Extensions`.
 */
class Extensions extends ExtensionsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'package_id', 'client_id', 'enabled', 'protected', 'ordering', 'status'], 'integer'],
            [['name', 'type', 'element', 'folder', 'manifest_cache', 'params'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param array $condition
     * @return ActiveDataProvider
     */
    public function search($params, $condition = [])
    {
        $query = ExtensionsModel::find();//->select(['*','version' => 'JSON_EXTRACT(manifest_cache, "$.version")']);

        if (!empty($condition)) {
            $query->where($condition);
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'package_id' => $this->package_id,
            'client_id' => $this->client_id,
            'enabled' => $this->enabled,
            'protected' => $this->protected,
            'ordering' => $this->ordering,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'element', $this->element])
            ->andFilterWhere(['like', 'folder', $this->folder])
            ->andFilterWhere(['like', 'manifest_cache', $this->manifest_cache])
            ->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }
}
