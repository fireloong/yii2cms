<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%extensions}}".
 *
 * @property int $id
 * @property int $package_id
 * @property string $name
 * @property string $type
 * @property string $element
 * @property string $folder
 * @property int $client_id
 * @property int $enabled
 * @property int $protected
 * @property array $manifest_cache
 * @property string $params
 * @property int $ordering
 * @property int $status
 */
class Extensions extends \yii\db\ActiveRecord
{
    public $title_native;
    public $code;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%extensions}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['package_id', 'client_id', 'enabled', 'protected', 'ordering', 'status'], 'integer'],
            [['name', 'type', 'client_id', 'manifest_cache'], 'required'],
            [['name', 'element', 'folder'], 'trim'],
            [['manifest_cache'], 'safe'],
            [['params'], 'string'],
            [['params'], 'default', 'value' => ''],
            [['name', 'element', 'folder'], 'string', 'max' => 100],
            [['type'], 'string', 'max' => 20]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('extensions', 'ID'),
            'package_id' => Yii::t('extensions', 'PACKAGE_ID'),
            'name' => Yii::t('extensions', 'NAME'),
            'type' => Yii::t('extensions', 'TYPE'),
            'element' => Yii::t('extensions', 'ELEMENT'),
            'folder' => Yii::t('extensions', 'FOLDER'),
            'client_id' => Yii::t('extensions', 'CLIENT'),
            'enabled' => Yii::t('extensions', 'ENABLED'),
            'protected' => Yii::t('extensions', 'PROTECTED'),
            'manifest_cache' => Yii::t('extensions', 'MANIFEST_CACHE'),
            'params' => Yii::t('extensions', 'PARAMS'),
            'ordering' => Yii::t('extensions', 'ORDER'),
            'status' => Yii::t('extensions', 'STATUS'),
            'title_native' => Yii::t('mod_langs', 'NATIVE_TITLE'),
            'code' => Yii::t('mod_langs', 'LANGUAGE_TAG')
        ];
    }

    /**
     * {@inheritdoc}
     * @return ExtensionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ExtensionsQuery(get_called_class());
    }

    public function getClients()
    {
        return $this->hasOne(Clients::class, ['id' => 'client_id']);
    }

}
