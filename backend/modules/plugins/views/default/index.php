<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this \yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('mod_plugins', 'PLUGINS');
$this->params['breadcrumbs'][] = ['label' => Yii::t('extensions', 'EXTENSIONS'), 'url' => ['installer']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\', html: true});');

$iconHtml = [
    'edit' => Html::icon('edit', ['class' => 'text-primary']),
    'enable' => Html::icon('ok', ['class' => 'text-success']),
    'disable' => Html::icon('remove-sign', ['class' => 'text-danger'])
];
?>
<div class="plugins-default-index">
    <p class="btn-toolbar">
        <?= Html::button($iconHtml['edit'] . ' ' . Yii::t('common', 'EDIT'), [
            'class' => 'btn btn-default btn-sm'
        ]) ?>
        <?= Html::button($iconHtml['enable'] . ' ' . Yii::t('common', 'ENABLE'), [
            'class' => 'btn btn-default btn-sm'
        ]) ?>
        <?= Html::button($iconHtml['disable'] . ' ' . Yii::t('common', 'DISABLE'), [
            'class' => 'btn btn-default btn-sm'
        ]) ?>
    </p>
    <?=
    Html::beginForm('', 'post', ['id' => 'plugins_form']) .
    GridView::widget([
        'id' => 'plugins_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '1%'],
                'checkboxOptions' => function ($model) {
                    return ['id' => 'cb' . $model->id];
                }
            ],
            [
                'attribute' => 'enabled',
                'label' => Yii::t('common','STATUS'),
                'format' => 'raw',
                'headerOptions' => ['width' => '2%', 'class' => 'text-nowrap'],
                'value' => function ($model) use ($iconHtml) {
                    $status = [
                        'enabled' => Html::a($iconHtml['enable'], '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('mod_plugins', 'DISABLE_PLUGIN'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ]),
                        'disabled' => Html::a($iconHtml['disable'], '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('mod_plugins', 'ENABLE_PLUGIN'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ])
                    ];

                    return $model->enabled === 0 ? $status['disabled'] : $status['enabled'];
                }
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    $langCat = $model->manifest_cache['langCat'] ?? null;
                    $name = $langCat ? Yii::t($langCat, $model->name) : $model->name;
                    return Html::a($name, ['edit', 'id' => $model->id], [
                        'title' => Yii::t('common', 'EDIT'),
                        'data-toggle' => 'tooltip'
                    ]);
                }
            ],
            [
                'attribute' => 'folder',
                'label' => Yii::t('mod_plugins', 'TYPE'),
                'headerOptions' => ['width' => '8%']
            ],
            [
                'attribute' => 'element',
                'label' => Yii::t('mod_plugins','ELEMENT'),
                'headerOptions' => ['width' => '8%']
            ],
            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '2%']
            ]
        ]
    ]) . Html::endForm()
    ?>
</div>
