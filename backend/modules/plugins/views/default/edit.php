<?php

use yii\bootstrap\Html;
use common\components\form\Form;

/* @var $this \yii\web\View */
/* @var $model \backend\models\Extensions */
/* @var $fields */

$langCat = $model->manifest_cache['langCat'] ?? null;
$pluginName = $langCat ? Yii::t($langCat, $model->name) : $model->name;
$params = json_decode($model->params, true);

$this->title = Yii::t('mod_plugins', 'PLUGINS_EDIT_TITLE', $pluginName);
$this->params['breadcrumbs'][] = ['label' => Yii::t('extensions', 'EXTENSIONS'), 'url' => ['installer']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_plugins', 'PLUGINS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $pluginName;

$css = <<<CSS

.plugins-default-edit .nav.nav-tabs{margin-bottom:18px;}
.plugins-default-edit .labels{margin-top:-5px;margin-bottom:10px;}
.plugins-default-edit .tooltip .tooltip-inner,td .tooltip .tooltip-inner{text-align:left;max-width:none;}

CSS;
$this->registerCss($css);

$js = <<<JS
$('[data-toggle="tooltip"]').tooltip({trigger: 'hover', html: true});
JS;
$this->registerJs($js);
if (is_object($fields)) {
    $fieldNamespace = (string)$fields->attributes()->addfieldnamespace;
} else {
    $fieldNamespace = null;
}
?>
<div class="plugins-default-edit">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <?= Html::a(Yii::t('mod_plugins', 'PLUGIN'), '#general', [
                'aria-controls' => 'general',
                'data-toggle' => 'tab',
                'role' => 'tab'
            ]) ?>
        </li>
        <?php
        if (is_object($fields)) {
            foreach ($fields->fieldset as $fieldset) {
                $name = (string)$fieldset->attributes()->name;
                $label = (string)$fieldset->attributes()->label;
                if ($name !== 'basic') {
                    $alink = Html::a(is_null($langCat) ? $label : Yii::t($langCat, $label), '#' . $name, [
                        'aria-controls' => $name,
                        'data-toggle' => 'tab',
                        'role' => 'tab'
                    ]);
                    echo Html::tag('li', $alink, ['role' => 'presentation']);
                }
            }
        }
        ?>
    </ul>

    <!-- Tab panes -->
    <?php $form = Form::begin(['id' => 'plugin_form', 'fieldNamespace' => $fieldNamespace]); ?>
    <div class="tab-content">
        <div id="general" class="tab-pane fade in active" role="tabpanel">
            <div class="row">
                <div class="col-sm-9 form-horizontal">
                    <h3><?= $pluginName ?></h3>
                    <div class="labels">
                        <?= Html::tag('span', $model->folder, [
                            'class' => 'label label-default',
                            'title' => Html::tag('strong', Yii::t('mod_plugins', 'PLUGIN_TYPE'))
                                . '<br>' . Yii::t('mod_plugins', 'PLUGIN_TYPE_DESC'),
                            'data-toggle' => 'tooltip'
                        ]) ?>
                        <span>/</span>
                        <?= Html::tag('span', $model->element, [
                            'class' => 'label label-default',
                            'title' => Html::tag('strong', Yii::t('mod_plugins', 'PLUGIN_FILE'))
                                . '<br>' . Yii::t('mod_plugins', 'PLUGIN_FILE_DESC'),
                            'data-toggle' => 'tooltip'
                        ]) ?>
                    </div>
                    <div><?= Yii::t($langCat, $model->manifest_cache['description']) ?></div>
                    <?php if (is_object($fields)): ?>
                        <hr>
                        <?php
                        foreach ($fields->fieldset as $fieldset) {
                            if ((string)$fieldset->attributes()->name === 'basic') {
                                echo $form->genXmlFields($fieldset->field, $model, $langCat);
                            }
                        }
                        ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-3 form-default">
                    <?= $form->field($model, 'enabled', 'dropDownList', ['setupParams' => [[
                        '1' => Yii::t('common', 'ENABLED'),
                        '0' => Yii::t('common', 'DISABLED')
                    ]]])->label(Yii::t('common', 'STATUS')) ?>

                    <?= $form->field($model, 'ordering', 'number') ?>

                    <?= $form->field($model, 'folder', 'text', [
                        'setupParams' => [['disabled' => true]]
                    ])->label(Yii::t('mod_plugins', 'PLUGIN_TYPE')) ?>

                    <?= $form->field($model, 'element', 'text', [
                        'setupParams' => [['disabled' => true]]
                    ])->label(Yii::t('mod_plugins', 'PLUGIN_FILE')) ?>
                </div>
            </div>
        </div>
        <?php
        if (is_object($fields)) {
            foreach ($fields->fieldset as $fieldset) {
                $fieldsetName = (string)$fieldset->attributes()->name;
                if ($fieldsetName !== 'basic') {
                    $fieldHtml = $form->genXmlFields($fieldset->field, $model, $langCat);
                    $content = Html::tag('div', Html::tag('div', $fieldHtml, ['class' => 'col-sm-9']), [
                        'class' => 'row form-horizontal'
                    ]);
                    echo Html::tag('div', $content, [
                        'id' => $fieldsetName,
                        'class' => 'tab-pane fade',
                        'role' => 'tabpanel'
                    ]);
                }
            }
        }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>
    <?php Form::end(); ?>

</div>
