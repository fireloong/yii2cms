<?php

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\components\Helper;
use backend\themes\basic\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $images */
/* @var $allFolders */
/* @var $get */
/* @var $folders */
/* @var $media */
/* @var $mediaRootPath */
/* @var $upload */

AppAsset::register($this);
$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', strtoupper(Helper::getClient()));

$js = <<<JS
$('#folder').change(function () {
  var id = '#folder_' + md5($(this).val());
  $(id).click();
});
$('#up_level').click(function() {
  var folder = $('#folder').val();
  var folderArr = folder.split('/');
  folderArr.pop();
  var id = '#folder_' + md5(folderArr.join('/'));
  $(id).click();
});
$(document).on('click', '.thumbnails-media .image', function() {
  $('#img_url').val($(this).find('img').attr('src'));
  $('#img_alt').val($(this).find('img').attr('title'));
  $(this).siblings('.active').removeClass('active');
  $(this).addClass('active');
});
JS;
if (isset($get['type']) && $get['type'] === 'tinymce') {
    $js .= <<<JS

$('#modal_cancel').click(function(){
    window.parent.postMessage({ mceAction: 'close' });
});
$('#modal_insert').click(function(){
    let img = $('#img_url').val();
    let alt = $('#img_alt').val();
    window.parent.postMessage({
       mceAction: 'customAction',
       data: {
           url: img,
           objVals: {
               alt: alt
           }
       }
    });
});
JS;
}
$this->registerJs($js);
$this->registerJsFile('https://cdn.jsdelivr.net/npm/js-md5@0.7.3/src/md5.min.js');

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php $this->registerCsrfMetaTags() ?>
<title><?= Yii::t('mod_media', 'INSERT_IMAGE') ?> - <?= $appName ?> - <?= $client ?></title>
<?php $this->head() ?>

<style>
.media-images-index .well {padding: 9px;margin-bottom: 10px;}
.media-images-index .well label {margin-bottom: 0;padding-top: 7px;}
.thumbnails-media li {
    width: 100px;
    height: 100px;
    text-align: center;
    background-color: #f4f4f4;
    box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.05) inset;
    border: 0;
    padding: 0;
    margin: 8px;
    border-radius: 3px;
    overflow: hidden;
}
.thumbnails-media li a {
    width: auto;
    height: 60px;
    display: block;
    position: relative;
    top: 50%;
    margin: -25px auto 0;
}
.thumbnails-media li a .glyphicon {font-size: 32px;}
.thumbnails-media li.image {cursor: pointer;line-height: 100px;}
.thumbnails-media li.image img {max-width: 100%;max-height: 100%;display: inline-block;}
.images-thumbnail {height: 450px;overflow-x: hidden;overflow-y: auto;}
.images-thumbnail .thumbnails-media li.image.active:before {
    font-family: 'Glyphicons Halflings';
    content: '\e013';
    color: #fff;
    background-color: #46a546;
    position: absolute;
    top: 0;
    right: 0;
    width: 22px;
    height: 22px;
    line-height: 22px;
    font-weight: normal;
    font-size: 12px;
}
.images-thumbnail .thumbnails-media li.image.active:after {
    position: absolute;
    bottom: 0;
    right: 0;
    border: 2px solid #5cb85c;
    border-radius: 5px;
    content: ' ';
    width: 100%;
    height: 100%;
}
.images-thumbnail ul li {margin: 6px;position: relative;}
</style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid media-images-index">
    <?= Helper::widget('alert') ?>
    <div class="row">
        <div class="col-md-12">
            <div class="well">
                <div class="row">
                    <div class="col-sm-8">
                        <label for="folder" class="col-sm-2 text-right"><?= Yii::t('mod_media', 'FOLDER') ?></label>
                        <div class="col-sm-6">
                            <?= Html::dropDownList('folder', $get['folder'], $allFolders, [
                                'id' => 'folder',
                                'class' => 'form-control'
                            ]) ?>
                        </div>
                        <div class="col-sm-3">
                            <?= Html::button(Yii::t('mod_media', 'UP'), ['id' => 'up_level', 'class' => 'btn btn-default btn-sm']) ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="pull-right">
                            <?= Html::button(Yii::t('mod_media', 'INSERT'), ['id' => 'modal_insert', 'class' => 'btn btn-success btn-sm']) ?>
                            <?= Html::button(Yii::t('common', 'CANCEL'), ['id' => 'modal_cancel', 'class' => 'btn btn-default btn-sm']) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="images-list">
                <?php Pjax::begin(); ?>
                <div class="hidden">
                    <?php foreach ($allFolders as $key => $item) {
                        $getAlias = $get;
                        unset($getAlias['folder']);
                        if (empty($key)) {
                            $url = array_merge(['index'], $getAlias);
                            echo Html::a($item, $url, ['id' => 'folder_' . md5($key)]);
                        } else {
                            $url = array_merge(['index', 'folder' => $key], $getAlias);
                            echo Html::a($item, $url, ['id' => 'folder_' . md5($key)]);
                        }
                    } ?>
                </div>
                <div class="row images-thumbnail">
                    <div class="col-sm-12">
                        <?= Html::ul($media, ['item' => function ($item) use ($get, $mediaRootPath) {
                            $urlParam = ($get['folder'] ? $get['folder'] . '/' : '') . basename($item);
                            if (is_dir($item)) {
                                $aContent = Html::icon('folder-close') . Html::tag('div', basename($item), ['class' => 'small']);
                                $getAlias = $get;
                                unset($getAlias['folder']);
                                $aUrl = array_merge(['index', 'folder' => $urlParam], $getAlias);
                                return Html::tag('li', Html::a($aContent, $aUrl), ['class' => 'folder']);
                            } else {
                                $assetManager = Yii::$app->assetManager;
                                $assetManager->hashCallback = function ($path) {
                                    return basename($path);
                                };
                                $url = $assetManager->getPublishedUrl($mediaRootPath);
                                if (!is_dir(Yii::getAlias('@webroot') . substr($url, strlen(Yii::getAlias('@web'))))) {
                                    $assetManager->publish($mediaRootPath);
                                }
                                $assetManager->hashCallback = null;
                                $src = rtrim($url, '/') . '/' . $urlParam;
                                return Html::tag('li', Html::img($src, ['title' => basename($item)]), ['class' => 'image']);
                            }
                        }, 'class' => 'list-inline thumbnails-media']) ?>
                    </div>
                </div>
                <?php $uploadAction = $get['folder'] ? Url::to(['upload', 'folder' => $get['folder']]) : Url::to(['upload']); ?>
                <script>if (typeof ($) === 'function') {
                        $('#folder').val('<?= $get['folder'] ?>');
                        $('#upload_form').attr('action', '<?= $uploadAction ?>');
                    }</script>
                <?php Pjax::end() ?>
            </div>
            <div class="well">
                <div class="row">
                    <div class="col-sm-2 text-right" style="padding-top: 7px;"><?= Yii::t('mod_media', 'IMAGE_URL') ?></div>
                    <div class="col-sm-8"><input id="img_url" class="form-control" type="text"></div>
                </div>
                <div class="row<?= (isset($get['alt']) && $get['alt'] === '1') ? '' : ' hidden' ?>">
                    <div class="col-sm-2 text-right" style="padding-top: 7px;"><?= Yii::t('mod_media', 'IMAGE_URL') ?></div>
                    <div class="col-sm-8"><input id="img_alt" class="form-control" type="text"></div>
                </div>
            </div>
            <div class="well">
                <div class="row">
                    <?php $form = ActiveForm::begin(['id' => 'upload_form', 'action' => ['upload', 'folder' => $get['folder']]]); ?>
                    <div class="col-sm-2 text-right" style="padding-top: 7px;"><?= Yii::t('mod_media', 'FILE_UPLOAD') ?></div>
                    <div class="col-sm-4">
                        <?= $form->field($upload, 'file')->fileInput()->label(false) ?>
                    </div>
                    <div class="col-sm-2">
                        <?= Html::submitButton(Yii::t('mod_media', 'START_UPLOAD'), ['class' => 'btn btn-primary btn-sm']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>