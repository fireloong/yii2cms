<?php

use yii\bootstrap\Html;
use yii\helpers\FileHelper;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
use backend\modules\media\views\assets\MediaAsset;

/* @var $this \yii\web\View */
/* @var $media \backend\modules\media\controllers\DefaultController */
/* @var $upload \backend\models\UploadForm */


MediaAsset::register($this);

$this->title = Yii::t('mod_media', 'MEDIA');
$this->params['breadcrumbs'][] = $this->title;

$lang = [
    'filename' => Yii::t('mod_media', 'FILENAME'),
    'path' => Yii::t('mod_media', 'PATH'),
    'type' => Yii::t('mod_media', 'TYPE'),
    'modify' => Yii::t('mod_media', 'MODIFY'),
    'size' => Yii::t('mod_media', 'SIZE'),
    'imgWidth' => Yii::t('mod_media', 'IMGWIDTH'),
    'imgHeight' => Yii::t('mod_media', 'IMGHEIGHT'),
    'deleteItemsConfirm' => Yii::t('mod_media', 'DELETE_SELECTED_ITEM_CONFIRM')
];

$this->registerJs('var _lang = ' . \yii\helpers\Json::htmlEncode($lang) . ';', static::POS_HEAD);

$mediaRootPath = Yii::getAlias($this->context->module->mediaPath);
$getFolder = Yii::$app->request->get('folder');
foreach ($media as &$medium) {
    $basename = basename($medium);
    if (is_dir($medium)) {
        if (StringHelper::startsWith($medium, $mediaRootPath)) {
            $folder = trim(substr($medium, strlen($mediaRootPath)), '/');
            $url = ['', 'folder' => $folder];
        } else {
            $url = null;
        }
        $checkbox = Html::checkbox('check[]', false, ['class' => 'pull-left', 'value' => $basename]);
        $delete = Html::a('x', ['delete', 'folder' => $getFolder], [
            'class' => 'pull-right',
            'title' => Yii::t('mod_media', 'DELETE_FILE'),
            'data' => [
                'method' => 'post',
                'params' => [
                    'check[]' => $basename
                ],
                'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM')
            ]
        ]);
        $medium = Html::tag('div', $checkbox . $delete, ['class' => 'media-top']);
        $medium .= Html::a(Html::icon('folder-close'), $url, ['class' => 'media-folder']);
        $medium .= Html::tag('div', Html::a($basename, $url), ['class' => 'media-bottom']);
    } elseif (is_file($medium)) {
        $mimeType = FileHelper::getMimeTypeByExtension($medium) ?? FileHelper::getMimeType($medium);
        $assetManager = Yii::$app->assetManager;
        $assetManager->hashCallback = function ($path){
            return basename($path);
        };
        $url = $assetManager->getPublishedUrl($mediaRootPath);
        if (!is_dir(Yii::getAlias('@webroot') . substr($url, strlen(Yii::getAlias('@web'))))) {
            $assetManager->publish($mediaRootPath);
        }
        $assetManager->hashCallback = null;

        if (StringHelper::startsWith($mimeType, 'image/')) {
            $imageSize = getimagesize($medium);
            $src = $url . '/' . ($getFolder ? $getFolder . '/' : '') . $basename;
            $aHtml = Html::a(Html::icon('search') . $basename, null, [
                'title' => $basename,
                'data' => [
                    'info' => [
                        'name' => $basename,
                        'src' => $src,
                        'type' => 'image',
                        'mimeType' => $mimeType,
                        'size' => Yii::$app->formatter->asShortSize(filesize($medium)),
                        'path' => ($getFolder ? $getFolder . '/' : '') . $basename,
                        'md5' => md5_file($medium),
                        'filemtime' => Yii::$app->formatter->asDatetime(filemtime($medium)),
                        'imageWidth' => $imageSize[0],
                        'imageHeight' => $imageSize[1]
                    ],
                    'toggle' => 'modal',
                    'target' => '.media-modal'
                ]
            ]);
            $medium = Html::tag('div', Html::a(Html::icon('remove'), ['delete', 'folder' => $getFolder], [
                'title' => Yii::t('mod_media', 'DELETE_FILE'),
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'check[]' => $basename
                    ],
                    'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM')
                ]
            ]), ['class' => 'file-delete']);
            $medium .= Html::label(Html::checkbox('check[]', false, ['class' => 'hidden', 'value' => $basename]) . Html::img($src, ['class' => 'img-thumb']));
            $medium .= Html::tag('div', $aHtml, ['class' => 'media-bottom']);
        } else {
            $suffix = pathinfo($medium, PATHINFO_EXTENSION);
            $svgName = is_file(Yii::getAlias('@webroot/image/' . $suffix . '.svg')) ? $suffix : 'unknown';
            $src = '/image/' . $svgName . '.svg';
            $aHtml = Html::a($basename, null, [
                'title' => $basename,
                'data' => [
                    'info' => [
                        'name' => $basename,
                        'src' => $src,
                        'mimeType' => $mimeType,
                        'size' => Yii::$app->formatter->asShortSize(filesize($medium)),
                        'path' => ($getFolder ? $getFolder . '/' : '') . $basename,
                        'md5' => md5_file($medium),
                        'filemtime' => Yii::$app->formatter->asDatetime(filemtime($medium))
                    ],
                    'toggle' => 'modal',
                    'target' => '.media-modal'
                ]
            ]);

            $checkbox = Html::checkbox('check[]', false, ['class' => 'pull-left', 'value' => $basename]);
            $delete = Html::a('x', ['delete', 'folder' => $getFolder], [
                'class' => 'pull-right',
                'title' => Yii::t('mod_media', 'DELETE_FILE'),
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'check[]' => $basename
                    ],
                    'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM')
                ]
            ]);
            $medium = Html::tag('div', $checkbox . $delete, ['class' => 'media-top']);
            $medium .= Html::tag('div', Html::img($src, ['class' => 'img-thumb']), ['class' => 'file-icon']);
            $medium .= Html::tag('div', $aHtml, ['class' => 'media-bottom']);
        }
    } elseif ($medium === 'uplevel') {
        $arrayForlder = explode('/', $getFolder);
        array_pop($arrayForlder);
        $url = empty($arrayForlder) ? [''] : ['', 'folder' => implode('/', $arrayForlder)];
        $medium = Html::a(Html::icon('level-up'), $url, [
            'class' => 'btn btn-default up-level',
            'data-toggle' => 'tooltip',
            'title' => Yii::t('mod_media', 'BACK_UP_LEVEL')
        ]);
    }
}
?>
<div class="media-default-index">
    <div class="btn-toolbar">
        <?= Html::button(Html::icon('plus-sign') . ' ' . Yii::t('mod_media', 'UPLOAD'), [
            'class' => 'btn btn-success btn-sm',
            'data' => [
                'toggle' => 'collapse',
                'target' => '#media_upload'
            ],
            'aria-expanded' => 'false',
            'aria-controls' => 'media_upload'
        ]) ?>
        <?= Html::button(Html::icon('folder-open') . Yii::t('mod_media', 'CREATE_NEW_FOLDER'), [
            'class' => 'btn btn-default btn-sm',
            'data' => [
                'toggle' => 'collapse',
                'target' => '#folder_create'
            ],
            'aria-expanded' => 'false',
            'aria-controls' => 'folder_create'
        ]) ?>
        <?= Html::button(Html::icon('remove') . ' ' . Yii::t('common', 'DELETE'), [
            'id' => 'media_delete',
            'class' => 'btn btn-default btn-sm'
        ]) ?>
    </div>
    <div id="media_upload" class="collapse">
        <div class="row">
            <div class="col-sm-6">
                <?php $form = ActiveForm::begin(['action' => ['upload', 'folder' => $getFolder]]) ?>
                <?= $form->field($upload,'file')->fileInput()->label(Yii::t('mod_media', 'FILE_UPLOAD')) ?>
                <?= Html::submitButton(Html::icon('open') . Yii::t('mod_media', 'START_UPLOAD'), ['class' => 'btn btn-primary btn-sm']) ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div id="folder_create" class="collapse">
        <div class="row">
            <div class="col-sm-6">
                <?= Html::beginForm(['create', 'folder' => $getFolder]) ?>
                <div class="input-group">
                    <span class="input-group-addon">./<?= basename($mediaRootPath) ?>/<?= $getFolder ? $getFolder . '/' : '' ?></span>
                    <input class="form-control" type="text" name="dirname" required>
                    <div class="input-group-btn">
                        <?= Html::submitButton(Html::icon('folder-open') . Yii::t('mod_media', 'CREATE_FOLDER'), [
                            'class' => 'btn btn-default'
                        ]) ?>
                    </div>
                </div>
                <?= Html::endForm() ?>
            </div>
        </div>
    </div>
    <p class="media-path">
        <?= Html::icon('folder-open') ?><?= basename($mediaRootPath) ?><?= $getFolder ? '/' . $getFolder : '' ?>
    </p>
    <div style="margin-bottom: 10px;">
        <label class="btn btn-default btn-sm">
            <input id="checkall_toggle" type="checkbox">
            <?= Yii::t('common', 'CHECK_ALL') ?>
        </label>
    </div>
    <?= Html::beginForm(['delete', 'folder' => $getFolder], 'post', ['id' => 'media_form']) ?>
    <?= Html::ul($media, ['class' => 'list-inline manager-media', 'encode' => false]) ?>
    <?= Html::endForm() ?>
    <div class="modal fade media-modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"><?= Yii::t('mod_media', 'PREVIEW') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-7 media-preview"></div>
                        <div class="col-lg-5 media-info"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        <?= Yii::t('common', 'CLOSE') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>