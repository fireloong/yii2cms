$(function ($) {
    $('[data-toggle="tooltip"]').tooltip();
    $('.media-modal').on('show.bs.modal', function (e) {
        var info = $(e.relatedTarget).data('info');
        var preview = '';
        var infoView = '<dl>';
        infoView += '<dt>' + _lang.filename + '</dt><dd>' + info.name + '</dd>';
        infoView += '<dt>' + _lang.path + '</dt><dd>' + info.path + '</dd>';
        infoView += '<dt>' + _lang.type + '</dt><dd>' + info.mimeType + '</dd>';
        infoView += '<dt>' + _lang.modify + '</dt><dd>' + info.filemtime + '</dd>';
        infoView += '<dt>' + _lang.size + '</dt><dd>' + info.size + '</dd>';
        infoView += '<dt>md5</dt><dd>' + info.md5 + '</dd>';
        if (info.type === 'image') {
            preview = '<img class="img-thumbnail" src="' + info.src + '" alt="preview">';
            infoView += '<dt>' + _lang.imgWidth + '</dt><dd>' + info.imageWidth + '</dd>';
            infoView += '<dt>' + _lang.imgHeight + '</dt><dd>' + info.imageHeight + '</dd>';
        } else {
            preview = '<img class="img-thumbnail" src="' + info.src + '" alt="preview">';
        }
        infoView += '</dl>';
        $(e.currentTarget).find('.media-preview').html(preview);
        $(e.currentTarget).find('.media-info').html(infoView);
    });
    $('.manager-media li').hover(function () {
        $(this).find('.file-delete').toggle('fast');
    });
    $('#checkall_toggle').change(function () {
        $('[name="check[]"]').prop('checked', this.checked).change();
    });
    $('.manager-media label [name="check[]"]').change(function () {
        $(this).closest('label').toggleClass('check-file');
    });
    $('#media_delete').click(function () {
        yii.confirm(_lang.deleteItemsConfirm, function () {
            $('#media_form').submit();
        })
    });
});