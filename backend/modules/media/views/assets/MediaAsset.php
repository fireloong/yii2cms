<?php


namespace backend\modules\media\views\assets;

use yii\web\AssetBundle;

/**
 * description of MediaAsset
 *
 * @author FireLoong
 */
class MediaAsset extends AssetBundle
{
    public $sourcePath = '@backend/modules/media/views/media';
    public $css = [
        'media.css'
    ];
    public $js = [
        'media.js'
    ];
    public $depends = [
        \backend\themes\basic\assets\AppAsset::class
    ];
}
