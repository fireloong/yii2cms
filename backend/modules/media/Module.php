<?php

namespace backend\modules\media;

use Yii;
/**
 * media module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @var string 默认上传路径
     */
    public $mediaPath = '@common/media';
    /**
     * @var int 最大上传文件大小 单位：MB
     */
    public $uploadMaxsize = 10;
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $mediaPath = Yii::getAlias($this->mediaPath);
        if(!is_dir($mediaPath)){
            \yii\helpers\FileHelper::createDirectory($mediaPath);
        }
        // custom initialization code goes here
    }
}
