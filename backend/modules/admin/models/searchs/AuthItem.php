<?php

namespace backend\modules\admin\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use backend\modules\admin\components\Configs;
use yii\rbac\Item;

/**
 * AuthItemSearch represents the model behind the search form about AuthItem.
 * 
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class AuthItem extends Model
{

    const TYPE_ROUTE = 101;

    public $name;
    public $type;
    public $description;
    public $ruleName;
    public $data;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ruleName', 'description'], 'safe'],
            [['type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_NAME'),
            'item_name' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_ITEM_NAME'),
            'type' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_TYPE'),
            'description' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_DESCRIPTION'),
            'ruleName' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_RULE_NAME'),
            'data' => Yii::t('rbac-admin', 'AUTH_ITEM_MODEL_DATA'),
        ];
    }

    /**
     * Search authitem
     * @param array $params
     * @return \yii\data\ActiveDataProvider|\yii\data\ArrayDataProvider
     */
    public function search($params)
    {
        /* @var \yii\rbac\Manager $authManager */
        $authManager = Configs::authManager();
        if ($this->type == Item::TYPE_ROLE) {
            $items = $authManager->getRoles();
        } else {
            $items = array_filter($authManager->getPermissions(), function($item) {
                $strncmp = strncmp($item->name, '/', 1) === 0 || strncmp($item->name, '@', 1) === 0;
                return $this->type == Item::TYPE_PERMISSION xor $strncmp;
            });
        }
        $this->load($params);
        if ($this->validate()) {

            $search = mb_strtolower(trim($this->name));
            $desc = mb_strtolower(trim($this->description));
            $ruleName = $this->ruleName;
            foreach ($items as $name => $item) {
                $f = (empty($search) || mb_strpos(mb_strtolower($item->name), $search) !== false) &&
                    (empty($desc) || mb_strpos(mb_strtolower($item->description), $desc) !== false) &&
                    (empty($ruleName) || $item->ruleName == $ruleName);
                if (!$f) {
                    unset($items[$name]);
                }
            }
        }

        return new ArrayDataProvider([
            'allModels' => $items,
        ]);
    }

}
