<?php

namespace backend\modules\admin\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\admin\models\MenuTypes as MenuTypesModel;

/**
 * MenuTypes represents the model behind the search form of `backend\modules\admin\models\MenuTypes`.
 */
class MenuTypes extends MenuTypesModel
{
    /**
     * @var string 搜索词
     */
    public $search;
    /**
     * @var int 每页数据条目数量
     */
    public $limit;
    /**
     * @var int 应用ID
     */
    public $client_id = 2;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'limit'], 'integer'],
            [['search'], 'string', 'max' => '50'],
            [['menutype', 'title', 'description'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MenuTypesModel::find()->alias('a')->joinWith('titleKey b')->joinWith('descKey c');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->limit) {
            $dataProvider->pagination->pageSize = $this->limit;
        } else {
            $this->limit = $dataProvider->pagination->pageSize;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'a.id' => $this->id,
            'a.client_id' => $this->client_id,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', 'a.menutype', $this->search],
            ['like', 'b.override_value', $this->search],
            ['like', 'c.override_value', $this->search]
        ]);

        return $dataProvider;
    }
}
