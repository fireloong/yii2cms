<?php

namespace backend\modules\admin\models\form;

use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use backend\models\Admin;
use backend\modules\admin\models\User;
use backend\modules\admin\components\UserStatus;

/**
 * Signup form
 */
class Signup extends Model
{
    public $username;
    public $email;
    public $password;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $class = Yii::$app->getUser()->identityClass ?: 'backend\modules\admin\models\User';
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => $class, 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => $class, 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['retypePassword', 'required'],
            ['retypePassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $class = Yii::$app->getUser()->identityClass ?: 'backend\modules\admin\models\User';
            $user = new $class();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->status = ArrayHelper::getValue(Yii::$app->params, 'user.defaultStatus', UserStatus::ACTIVE);
            $user->setPassword($this->password);
            $user->generateAuthKey();
            if ($user->save()) {
                $admin = new Admin();
                $admin->uid = $user->id;
                $admin->lastvisitDate = time();
                return $admin->save() ? $user : null;
            }
        }

        return null;
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('rbac-admin', 'USERNAME'),
            'email' => Yii::t('rbac-admin', 'EMAIL'),
            'password' => Yii::t('rbac-admin', 'PASSWORD'),
            'retypePassword' => Yii::t('rbac-admin', 'RETYPE_PASSWORD')
        ];
    }
}
