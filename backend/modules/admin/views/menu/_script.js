$('#parent_name').autocomplete({
    source: function (request, response) {
        var result = [];
        var limit = 10;
        var term = request.term.toLowerCase();
        $.each(_opts.menus, function () {
            var menu = this;
            if (term == '' || menu.name.toLowerCase().indexOf(term) >= 0 ||
                (menu.parent_name && menu.parent_name.toLowerCase().indexOf(term) >= 0) ||
                (menu.route && menu.route.toLowerCase().indexOf(term) >= 0)) {
                result.push(menu);
                limit--;
                if (limit <= 0) {
                    return false;
                }
            }
        });
        response(result);
    },
    select: function (event, ui) {
        $('#parent_name').val(ui.item.name);
        $('#parent_id').val(ui.item.id);
        return false;
    },
    search: function () {
        $('#parent_id').val('');
    }
}).autocomplete("instance")._renderItem = function (ul, item) {
    var parent_name = item.parent_name === '' ? 'NULL' : item.parent_name;
    var route = item.route === '' ? 'NULL' : item.route;
    return $("<li>")
        .append($('<a>').append($('<b>').text(item.name)).append('<br>')
            .append($('<i>').text(parent_name + ' | ' + route)))
        .appendTo(ul);
};

$('#route').autocomplete({
    source: _opts.routes
});

$('.field-menu-name .control-label').after($('.field-menu-name .hint-block'));
