<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;
use backend\modules\admin\models\Menu;
use backend\modules\admin\AutocompleteAsset;
use backend\modules\languages\models\form\Filter;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Menu */
/* @var $client_id int */
/* @var $form yii\widgets\ActiveForm */
/* @var $menuTypeList backend\modules\admin\controllers\MenuController */
/* @var $langList backend\modules\admin\controllers\MenuController */

$this->params['active'] = 'menu';

$css = <<<CSS
.field-menu-name .hint-block{display: inline-block;margin-left: 1rem;}
CSS;
$this->registerCss($css);

AutocompleteAsset::register($this);
$opts = Json::htmlEncode([
    'menus' => Menu::getMenuSource(),
    'routes' => Menu::getSavedRoutes(),
]);
$this->registerJs("var _opts = $opts;");
$this->registerJs($this->render('_script.js'));

$statusList = [
    1 => Yii::t('rbac-admin', 'PUBLISHED'),
    0 => Yii::t('rbac-admin', 'UNPUBLISHED'),
    -1 => Yii::t('rbac-admin', 'TRASHED')
];
$typeList = [
    'module' => Yii::t('rbac-admin', 'MODULE'),
    'URL' => Yii::t('rbac-admin', 'URL'),
    'alias' => Yii::t('rbac-admin', 'ALIAS'),
    'separator' => Yii::t('rbac-admin', 'SEPARATOR'),
    'heading' => Yii::t('rbac-admin', 'HEADING')
];
$client = \backend\models\Clients::findOne($client_id);
?>
<div class="menu-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= Html::activeHiddenInput($model, 'parent', ['id' => 'parent_id']); ?>
    <div class="row">
        <div class="col-sm-6">
            <?php if ($model->getIsNewRecord()): ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])
                    ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                        '/languages/overrides/index'
                    ], ['target' => '_blank'])) ?>
            <?php else: ?>
                <?= $form->field($model, 'name')->textInput(['maxlength' => true])
                    ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                        '/languages/overrides/update',
                        'id' => Filter::getId($client->name, 'rbac-admin', $model->name)
                    ], ['target' => '_blank'])) ?>
            <?php endif; ?>

            <?= $form->field($model, 'alias')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'parent_name')->textInput(['id' => 'parent_name']) ?>

            <?= $form->field($model, 'route')->textInput(['id' => 'route']) ?>

            <?= $form->field($model, 'published')->dropDownList($statusList) ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'ordering')->input('number') ?>

            <?= $form->field($model, 'menutype')->dropDownList($menuTypeList) ?>

            <?= $form->field($model, 'type')->dropDownList($typeList) ?>

            <?= $form->field($model, 'language')->dropDownList($langList) ?>

            <?= $form->field($model, 'data')->textarea(['rows' => 4]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('rbac-admin', 'SAVE'), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
