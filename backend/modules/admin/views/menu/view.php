<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Menu */

$this->title = $model->getNameLang($model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENU_ITEMS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['active'] = 'menu';
?>
<div class="menu-view">

    <p>
        <?= Html::a(Yii::t('yii', 'UPDATE'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(Yii::t('yii', 'DELETE'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM'),
                'method' => 'post',
            ],
        ])
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => Yii::t('rbac-admin', 'PARENT'),
                'value' => $model->getNameLang($model->parent)
            ],
            [
                'attribute' => 'name',
                'value' => $model->getNameLang($model->id)
            ],
            [
                'attribute' => 'type',
                'value' => Yii::t('rbac-admin', strtoupper($model->type))
            ],
            'route',
            'language',
            'ordering',
            'params',
            'data'
        ],
    ])
    ?>

</div>
