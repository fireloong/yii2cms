<?php

use backend\components\Icon;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\Helper;
use common\components\form\Form;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Menu */
/* @var $client_id int */
/* @var $form yii\widgets\ActiveForm */
/* @var $menuTypeList backend\modules\admin\controllers\MenuController */
/* @var $langList backend\modules\admin\controllers\MenuController */
/* @var $data array */
/* @var $menuParentList array */
/* @var $xmlFields object */

$this->params['active'] = 'menu';

$css = <<<CSS
#menu-alias{width:220px;}.tab-content > .tab-pane{padding-top: 1.5rem;}.modal-body{padding: 0;}
.input-group-btn .btn i{margin-right: 3px;}.tooltip-inner{white-space: nowrap;}
.input-group-btn > .btn{border-top-left-radius:0;border-bottom-left-radius:0;}
CSS;
$this->registerCss($css);
$parentItemsUrl = Url::to(['parent-items']);
$js = <<<JS
$('[data-toggle="tooltip"]').tooltip();
$('#menuTypeModal').on('shown.bs.modal', function () {
  $('#menu_type_iframe').contents().find('.list-group-item').on('click',function() {
      if($(this).data('route') !== ''){
          $('#menu-route').val($(this).data('route'));
      }
    $('#type_alias').val($(this).find('span').text());
    $('#menu-type').val($(this).data('type'));
    $('input[name="task"]').val('setType');
    $('input[name="path"]').val($(this).data('path'));
    $('input[name="langCat"]').val($(this).data('langcat'));
    $('#menuTypeModal').modal('hide');
    $('#menu_form').yiiActiveForm('destroy');
    $('#menu_form').submit();
  });
});
$('#menu-menutype').on('change',function() {
  $.ajax({
    type: 'GET',
    dataType: 'json',
    url: '$parentItemsUrl',
    data: {menuType: $(this).val()}
  }).done(function(data){
      $('#menu-parent option').remove();
      $.each(data, function(i,v) {
        var option = $('<option>');
        option.text(v.label).val(v.value);
        $('#menu-parent').append(option);
      });
  });
});
$('#menu_form').on('beforeSubmit',function() {
  $('input[name="task"]').val('apply');
  if ($('#menu-type').val() !== 'module' && $('#menu-type').val() !== 'url'){
      $('#menu-route').val('');
  }
});
JS;

if ($model->type === 'alias') {
    $js .= <<<JS

window.modalClose = function(data) {
    $('#menu-params-menu_id').val(data.id);
    $('#alias_menu_id').val(data.title);
    $('#aliasMenuIdModal').modal('hide');
};
JS;
}
$this->registerJs($js);
$client = \backend\models\Clients::findOne($client_id);

if (isset($data['path'])) {
    $dir = dirname($data['path']) . '/field';
    $fieldNamespace = substr($dir, strlen(Yii::getAlias('@root/')));
} else {
    $fieldNamespace = null;
}
?>
<div class="menu-form">
    <?php $form = Form::begin(['id' => 'menu_form', 'fieldNamespace' => $fieldNamespace]); ?>
    <div class="row">
        <div class="col-sm-6 form-inline">
            <?= $form->field($model, 'name', 'text', ['inputOptions' => ['maxlength' => true]]) ?>
        </div>
        <div class="col-sm-4 form-inline">
            <?= $form->field($model, 'alias', 'text', ['inputOptions' => [
                'placeholder' => Yii::t('rbac-admin', 'MENU_ALIAS_PLACEHOLDER')
            ]]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active" role="presentation">
                    <?= Html::a(Yii::t('rbac-admin', 'MENU_ITEM_SET'), '#details', [
                        'aria-controls' => 'details',
                        'role' => 'tab',
                        'data-toggle' => 'tab'
                    ]) ?>

                </li>
                <?php
                if (!is_null($xmlFields) && isset($xmlFields->params)) {
                    foreach ($xmlFields->params as $fieldset) {
                        $fieldsetName = (string)$fieldset->attributes()->name;
                        $fieldsetLabel = (string)$fieldset->attributes()->label;
                        echo Html::tag('li', Html::a(Yii::t($data['langCat'], $fieldsetLabel), '#' . $fieldsetName, [
                            'aria-controls' => $fieldsetName,
                            'role' => 'tab',
                            'data-toggle' => 'tab'
                        ]), ['role' => 'presentation']);
                    }
                }
                ?>
            </ul>
            <div class="tab-content">
                <div id="details" class="tab-pane fade in active" role="tabpanel">
                    <div class="row">
                        <div class="col-sm-9 form-horizontal">
                            <div class="form-group field-menu-type">
                                <?= Html::label(Yii::t('rbac-admin', 'MENU_ITEM_TYPES'), 'type_alias', [
                                    'class' => 'col-sm-3 control-label'
                                ]) ?>

                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <?= Html::textInput('type_alias', $data['type_alias'] ?? '', [
                                            'id' => 'type_alias',
                                            'class' => 'form-control',
                                            'readonly' => true
                                        ]) ?>

                                        <span class="input-group-btn">
                                                <?= Html::button(Html::tag('i', '', [
                                                        'class' => 'icon-list',
                                                        'aria-hidden' => 'true'
                                                    ]) . Yii::t('common', 'SELECT'), [
                                                    'class' => 'btn btn-default',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#menuTypeModal'
                                                ]) ?>
                                            </span>
                                    </div>
                                    <?= $form->field($model, 'type', 'hidden', ['inputOptions' => ['class' => null]]) ?>
                                    <p class="help-block help-block-error"></p>
                                </div>
                            </div>
                            <?php
                            if (!is_null($xmlFields) && isset($xmlFields->request)) {
                                foreach ($xmlFields->request as $fieldset) {
                                    if ((string)$fieldset->attributes()->name === 'request') {
                                        foreach ($fieldset->field as $field) {
                                            echo $form->xField($model, $field, [
                                                'layout' => 'horizontal',
                                                'data' => $data
                                            ],'request');
                                        }
                                    }
                                }
                            }
                            if ($model->type === 'module' || is_null($model->type)) {
                                echo $form->field($model, 'route', 'text', [
                                    'layout' => 'horizontal',
                                    'inputOptions' => ['readonly' => true]
                                ]);
                                echo $form->field($model, 'browserNav', 'dropDownList', [
                                    'layout' => 'horizontal',
                                    'setupParams' => [[
                                        '0' => Yii::t('rbac-admin', 'BROWSER_TARGET_PARENT'),
                                        '1' => Yii::t('rbac-admin', 'BROWSER_TARGET_NEW')
                                    ]]
                                ]);
                            } elseif ($model->type === 'url') {
                                echo $form->field($model, 'route', 'text', ['layout' => 'horizontal']);
                                echo $form->field($model, 'browserNav', 'dropDownList', [
                                    'layout' => 'horizontal',
                                    'setupParams' => [[
                                        '0' => Yii::t('rbac-admin', 'BROWSER_TARGET_PARENT'),
                                        '1' => Yii::t('rbac-admin', 'BROWSER_TARGET_NEW')
                                    ]]
                                ]);
                            } elseif ($model->type === 'alias') {
                                $aliasMenuName = $model->params['menu_id'] ?? Yii::t('rbac-admin', 'SELECT_ONE_MENU_ITEM');
                                ?>
                                <div class="form-group field-menu-params-menu_id">
                                    <label class="control-label col-sm-3"><?= Yii::t('rbac-admin', 'ALIAS_MENU') ?></label>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <?= Html::textInput('alias_menu_id', $aliasMenuName, [
                                                'id' => 'alias_menu_id',
                                                'class' => 'form-control',
                                                'readonly' => true
                                            ]) ?>
                                            <span class="input-group-btn" title="<?= Yii::t('rbac-admin', 'CHANGE_MENUITEM') ?>" data-toggle="tooltip">
                                                <?= Html::button(Html::tag('i', '', [
                                                        'class' => 'icon-file-empty',
                                                        'aria-hidden' => 'true'
                                                    ]) . Yii::t('common', 'SELECT'), [
                                                    'class' => 'btn btn-default',
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#aliasMenuIdModal'
                                                ]) ?>
                                            </span>
                                        </div>
                                        <?= $form->field($model, 'params[menu_id]', 'hidden') ?>
                                        <p class="help-block help-block-error"></p>
                                    </div>
                                </div>
                                <?php
                                echo $form->field($model, 'params[alias_redirect]', 'boolean', [
                                    'layout' => 'horizontal'
                                ])->label(Yii::t('rbac-admin', 'ALIAS_REDIRECT'));
                                echo $form->field($model, 'browserNav', 'dropDownList', [
                                    'layout' => 'horizontal',
                                    'setupParams' => [[
                                        '0' => Yii::t('rbac-admin', 'BROWSER_TARGET_PARENT'),
                                        '1' => Yii::t('rbac-admin', 'BROWSER_TARGET_NEW')
                                    ]]
                                ]);
                                echo $form->field($model, 'route', 'hidden');
                            } else {
                                echo $form->field($model, 'route', 'hidden');
                                echo $form->field($model, 'browserNav', 'hidden');
                            }
                            if (!is_null($xmlFields) && isset($xmlFields->request)) {
                                foreach ($xmlFields->request as $fieldset) {
                                    if ((string)$fieldset->attributes()->name === 'params') {
                                        foreach ($fieldset->field as $field) {
                                            echo $form->xmlField($model, 'params', $field, [
                                                'layout' => 'horizontal',
                                                'data' => $data
                                            ]);
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="col-sm-3">
                            <?= $form->field($model, 'menutype', 'dropDownList', ['setupParams' => [$menuTypeList]]) ?>
                            <?= $form->field($model, 'parent', 'dropDownList', ['setupParams' => [$menuParentList]]) ?>
                            <?= $form->field($model, 'published', 'dropDownList', ['setupParams' => [[
                                1 => Yii::t('rbac-admin', 'PUBLISHED'),
                                0 => Yii::t('rbac-admin', 'UNPUBLISHED'),
                                -1 => Yii::t('rbac-admin', 'TRASHED')
                            ]]]) ?>
                            <?= $form->field($model, 'language', 'dropDownList', [
                                'options' => [
                                    'class' => $client->name == Helper::getClient() ? 'hidden' : 'form-group'
                                ],
                                'setupParams' => [$langList]
                            ]) ?>
                            <?= $form->field($model, 'ordering', 'number') ?>
                            <?= $form->field($model, 'note', 'text') ?>
                            <?php if ($client->name == Helper::getClient()): ?>
                                <?= $form->field($model, 'data[lang-cat]', 'text')->label(Yii::t('rbac-admin', 'LANGUAGE_CATEGORY')) ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php
                if (!is_null($xmlFields) && isset($xmlFields->params)) {
                    foreach ($xmlFields->params as $fieldset) {
                        $fieldsetName = (string)$fieldset->attributes()->name;
                        $fieldsHtml = '';
                        foreach ($fieldset->field as $field) {
                            $fieldName = 'params[' . (string)$field->attributes()->name . ']';
                            $fieldType = (string)$field->attributes()->type;
                            $fieldLabel = Yii::t($data['langCat'], (string)$field->attributes()->label);
                            $fieldDesc = Yii::t($data['langCat'], (string)$field->attributes()->description);
                            $fieldValue =  Html::getAttributeValue($model,$fieldName) ?? (string)$field->attributes()->default;
                            if (boolval($fieldDesc)){
                                $spanOptions['title'] = $fieldLabel;
                                $spanOptions['data'] = [
                                    'toggle' => 'popover',
                                    'trigger' => 'hover',
                                    'container' => 'body',
                                    'content' => $fieldDesc
                                ];
                                $helpIcon = ' ' . Icon::i('fa-question-circle-o', ['class' => 'icon-sm text-muted']);
                                $fieldLabel = Html::tag('span',$fieldLabel . $helpIcon, $spanOptions);
                                $this->registerJs('$(\'[data-toggle="popover"]\').popover();');
                            }
                            $fieldsHtml = $form->field($model, $fieldName, $fieldType, [
                                'layout' => 'horizontal',
                                'setupParams' => [['value' => $fieldValue]]
                            ])->label($fieldLabel);
                        }
                        echo Html::tag('div', Html::tag('div', Html::tag('div', $fieldsHtml, [
                            'class' => 'col-sm-12 form-horizontal'
                        ]), ['class' => 'row']), [
                            'id' => $fieldsetName,
                            'class' => 'tab-pane fade',
                            'role' => 'tabpanel'
                        ]);
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-primary']) ?>

                </div>
            </div>
        </div>
    </div>
    <?= Html::hiddenInput('task') ?>

    <?= Html::hiddenInput('path') ?>

    <?= Html::hiddenInput('langCat') ?>

    <?php Form::end(); ?>

</div>
<div id="menuTypeModal" class="modal fade" role="dialog" aria-labelledby="menuTypeModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?= Yii::t('rbac-admin', 'MENU_ITEM_TYPES') ?></h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-1by1">
                    <?= Html::tag('iframe', '', [
                        'id' => 'menu_type_iframe',
                        'class' => 'embed-responsive-item',
                        'src' => Url::to(['menu-types/types', 'client_id' => $client_id])
                    ]) ?>

                </div>
            </div>
            <div class="modal-footer">
                <?= Html::button(Yii::t('common', 'CLOSE'), [
                    'class' => 'btn btn-default',
                    'type' => 'button',
                    'data-dismiss' => 'modal'
                ]) ?>

            </div>
        </div>
    </div>
</div>
<?php if ($model->type === 'alias'): ?>
    <div id="aliasMenuIdModal" class="modal fade" role="dialog" aria-labelledby="aliasMenuIdModal" tabindex="-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"><?= Yii::t('rbac-admin', 'CHANGE_MENUITEM') ?></h4>
                </div>
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <?= Html::tag('iframe', '', [
                            'id' => 'alias_menu_id_iframe',
                            'class' => 'embed-responsive-item',
                            'src' => Url::to(['menu/index', 'layout' => 'modal', 'client_id' => $client_id])
                        ]) ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <?= Html::button(Yii::t('common', 'CLOSE'), [
                        'class' => 'btn btn-default',
                        'type' => 'button',
                        'data-dismiss' => 'modal'
                    ]) ?>

                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
