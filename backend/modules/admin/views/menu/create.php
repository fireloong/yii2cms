<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Menu */
/* @var $client_id int */
/* @var $menuTypeList backend\modules\admin\controllers\MenuController */
/* @var $langList backend\modules\admin\controllers\MenuController */
/* @var $data array */
/* @var $menuParentList array */
/* @var $xmlFields object */

$this->title = Yii::t('rbac-admin', 'NEW');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENU_ITEMS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model->ordering = $model->ordering ?? 0;
?>
<div class="menu-create">

    <?=
    $this->render('_form2', [
        'model' => $model,
        'client_id' => $client_id,
        'menuTypeList' => $menuTypeList,
        'langList' => $langList,
        'data' => $data,
        'menuParentList' => $menuParentList,
        'xmlFields' => $xmlFields
    ])
    ?>

</div>
