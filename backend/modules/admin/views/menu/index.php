<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use backend\components\Icon;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\modules\admin\models\searchs\Menu */
/* @var $langList backend\modules\admin\controllers\MenuController */
/* @var $clientList array */
/* @var $layout string */

$this->title = Yii::t('rbac-admin', 'MENU_ITEMS');
$this->params['breadcrumbs'][] = $this->title;
if ($layout === 'modal') {
    $this->params['breadcrumbs'] = false;
    $this->params['navShow'] = false;
    $this->params['submenusShow'] = false;
    $this->params['footerShow'] = false;
    $css = <<<CSS
body{padding-top: 0;}.zmh-main{padding-bottom: 0;}.main{padding:1.5rem;}
CSS;
    $this->registerCss($css);
    $js = <<<JS
$('.select-link').on('click', function() {
    window.parent.modalClose($(this).data());
});
JS;
    $this->registerJs($js);
}
$this->params['active'] = 'menu';

$js = '$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\', html: true});';
$this->registerJs($js);

$returnUrl = [
    'route' => '/' . Yii::$app->controller->route,
    'get' => Yii::$app->request->get()
];

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'published',
        'format' => 'raw',
        'headerOptions' => ['width' => '2%', 'class' => 'text-nowrap'],
        'value' => function ($model) use ($returnUrl, $layout) {
            $data = [
                'toggle' => 'tooltip',
                'method' => 'post',
                'params' => [
                    'id' => $model->id,
                    'returnUrl' => json_encode($returnUrl)
                ]
            ];
            if ($layout === 'modal') {
                unset($data['method'], $data['params']);
            }
            $link = $layout === 'modal' ? null : ['set-published'];
            $status = [
                1 => Html::a(Icon::i('fa-check', ['class' => 'text-success']), $link, [
                    'class' => 'btn btn-default btn-xs',
                    'title' => Yii::t('rbac-admin', $layout === 'modal' ? 'PUBLISHED_MENU_ITEM' : 'UNPUBLISH_MENU_ITEM'),
                    'disabled' => $layout === 'modal',
                    'data' => $data
                ]),
                0 => Html::a(Icon::i('fa-times-circle-o', ['class' => 'text-danger']), $link, [
                    'class' => 'btn btn-default btn-xs',
                    'title' => Yii::t('rbac-admin', $layout === 'modal' ? 'UNPUBLISHED_MENU_ITEM' : 'PUBLISH_MENU_ITEM'),
                    'data' => $data
                ]),
                2 => Html::a(Icon::i('fa-trash-o'), $link, [
                    'class' => 'btn btn-default btn-xs',
                    'title' => Yii::t('rbac-admin', $layout === 'modal' ? 'TRASHED_MENU_ITEM' : 'PUBLISH_MENU_ITEM'),
                    'data' => $data
                ])
            ];
            return $status[$model->published];
        }
    ],
    [
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model) use ($layout) {
            $prefix = str_repeat(Html::tag('span', '&ensp;&#9482;', ['class' => 'text-muted']), $model->level);

            if ($model->level > 0) {
                $prefix .= '&ensp;';
            }

            $name = $model->getNameLang($model->id);

            if ($layout === 'modal' && in_array($model->type, ['module', 'url'])) {
                $name = Html::a($name, 'javascript:void(0)', [
                    'class' => 'select-link',
                    'data' => [
                        'id' => $model->id,
                        'title' => $name
                    ]
                ]);
            }

            return $prefix . $name;
        }
    ],
    [
        'attribute' => 'alias',
        'format' => 'raw',
        'value' => function($model){
            return Html::tag('span',$model->alias,['class'=>'text-muted']);
        }
    ],
    [
        'attribute' => 'menuTypes.title',
        'value' => function ($model) {
            return Yii::t('rbac-admin', $model->menuTypes->title);
        }
    ],
    [
        'attribute' => 'home',
        'format' => 'raw',
        'headerOptions' => [
            'class' => 'text-center'
        ],
        'contentOptions' => [
            'class' => 'text-center'
        ],
        'value' => function ($model) use ($searchModel) {
            $isDefault = $model->home > 0;
            $icon = $isDefault ? Icon::i('star-full', ['class' => 'icon-default']) : Icon::i('star-empty');
            return Html::tag('span', Html::a($icon, null, [
                'class' => 'btn btn-default btn-xs' . ($isDefault ? ' disabled' : ''),
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'id' => $model->id,
                        'client' => $searchModel->client_id
                    ]
                ]
            ]), [
                'title' => Yii::t('rbac-admin', $isDefault ? 'DEFAULT' : 'SET_DEFAULT'),
                'data' => [
                    'toggle' => 'tooltip'
                ]
            ]);
        }
    ],
    [
        'attribute' => 'menuParent.name',
        'label' => Yii::t('rbac-admin', 'PARENT'),
        'value' => function ($model) {
            return $model->getNameLang($model->parent);
        }
    ],
    'route',
    [
        'attribute' => 'type',
        'value' => function ($model) {
            return Yii::t('rbac-admin', strtoupper($model->type));
        }
    ],
    [
        'attribute' => 'language',
        'value' => function ($model) use ($langList) {
            $lang = empty($model->language) ? '*' : $model->language;
            return $langList[$lang];
        }
    ],
    'id'
];

if ($layout !== 'modal') {
    $columns[] = ['class' => 'yii\grid\ActionColumn'];
}

?>
<div class="menu-index">
    <?php if ($layout !== 'modal'): ?>
        <p class="btn-toolbar">
            <?= Html::a(Yii::t('rbac-admin', 'NEW'), [
                'create',
                'client_id' => $searchModel->client_id
            ], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>
    <?= $this->render('_search', ['model' => $searchModel, 'clientList' => $clientList, 'layout' => $layout]); ?>
    <?=
    GridView::widget([
        'layout' => '{items}{summary}{pager}',
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => $columns
    ]);
    ?>

</div>
