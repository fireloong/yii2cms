<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\Menu */
/* @var $client_id int */
/* @var $menuTypeList backend\modules\admin\controllers\MenuController */
/* @var $langList backend\modules\admin\controllers\MenuController */
/* @var $typeAlias string */
/* @var $menuParentList array */
/* @var $xmlFields object */

$this->title = Yii::t('rbac-admin', 'UPDATE') . ': ' . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENU_ITEMS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getNameLang($model->id), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'UPDATE');
?>
<div class="menu-update">

    <?=
    $this->render('_form2', [
        'model' => $model,
        'data' => $data,
        'langList' => $langList,
        'menuTypeList' => $menuTypeList,
        'client_id' => $client_id,
        'xmlFields' => $xmlFields,
        'menuParentList' => $menuParentList,
    ])
    ?>

</div>
