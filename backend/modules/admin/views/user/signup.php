<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\themes\basic\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\modules\admin\models\form\Signup */

AppAsset::register($this);
(new AppAsset(['sourcePath' => '@backend/modules/admin/assets']))->addCss('main.css', true, '');

$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode(Yii::t('rbac-admin', 'SIGNUP')) ?></title>
        <?php $this->head() ?>

    </head>
    <body>
        <?php $this->beginBody() ?>
        <div class="container-fluid site-signup">

            <p class="text-center"><?= Yii::t('rbac-admin','USER_SIGNUP_FORM_HEAD')?></p>
            <?= Html::errorSummary($model)?>
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
                        <?= $form->field($model, 'username') ?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <?= $form->field($model, 'retypePassword')->passwordInput() ?>
                        <div class="form-group">
                            <?= Html::submitButton(Yii::t('rbac-admin', 'SIGNUP'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-lg-4"></div>
            </div>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
