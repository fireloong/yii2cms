<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \backend\modules\admin\models\User */

$this->title = Yii::t('rbac-admin', 'UPDATE_USER');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'USERS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'user';

$formatter = Yii::$app->formatter;
?>
<div class="user-update">
    <?php
    $form = ActiveForm::begin([
        'id' => 'user_form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-lg-2',
                'hint' => 'col-lg-4'
            ]
        ]
    ])
    ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
    <?= $form->field($model, 'created_at')->textInput([
        'value' => $formatter->asDatetime($model->created_at),
        'disabled' => true
    ]) ?>
    <?php
    if ($model->admin) {
        echo $form->field($model, 'admin')->textInput([
            'value' => $formatter->asDatetime($model->admin->lastvisitDate),
            'disabled' => true
        ])->label(Yii::t('site', 'LAST_VISIT_DATE'));
    }
    ?>
    <?= $form->field($model, 'status')->dropDownList([
        0 => Yii::t('rbac-admin', 'INACTIVE'),
        10 => Yii::t('rbac-admin', 'ACTIVATE')
    ], ['disabled' => Yii::$app->user->id == $model->id]) ?>
    <?= $form->field($model, 'id')->input('number', ['disabled' => true]) ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('common', 'RESET'), ['update', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
