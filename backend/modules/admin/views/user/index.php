<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\searchs\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $type */

$this->title = Yii::t('rbac-admin', 'USERS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'user';
?>
<div class="user-index">
    <?= $this->render('_search', ['model' => $searchModel, 'type' => $type]); ?>
    <?=
    GridView::widget([
        'options' => ['class' => 'grid-view table-responsive'],
        'layout' => '{items}{summary}{pager}',
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'username',
            'email:email',
            'updated_at:datetime',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->status == 0 ? 'INACTIVE' : 'ACTIVE');
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => Helper::filterActionColumn(['view', 'activate', 'delete']),
                'buttons' => [
                    'activate' => function ($url, $model) {
                        if ($model->status == 10) {
                            return '';
                        }
                        $options = [
                            'title' => Yii::t('rbac-admin', 'ACTIVATE'),
                            'aria-label' => Yii::t('rbac-admin', 'ACTIVATE'),
                            'data-confirm' => Yii::t('rbac-admin', 'ACTIVATE_CONFIRM'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ];
                        return Html::a('<span class="glyphicon glyphicon-ok"></span>', $url, $options);
                    }
                ]
            ],
        ],
    ]);
    ?>
</div>
