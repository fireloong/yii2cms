<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use backend\modules\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'USERS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'user';

$controllerId = $this->context->uniqueId . '/';
?>
<div class="user-view">

    <p>
        <?php
        if ($model->status == 0 && Helper::checkRoute($controllerId . 'activate')) {
            echo Html::a(Yii::t('rbac-admin', 'ACTIVATE'), ['activate', 'id' => $model->id], [
                'class' => 'btn btn-primary',
                'data' => [
                    'confirm' => Yii::t('rbac-admin', 'ACTIVATE_CONFIRM'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if (Helper::checkRoute($controllerId . 'delete')) {
            echo Html::a(Yii::t('yii', 'DELETE'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM'),
                    'method' => 'post',
                ],
            ]);
        }
        ?>
        <?php
        if (Helper::checkRoute($controllerId . 'update')) {
            echo Html::a(Yii::t('yii', 'UPDATE'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <?php
    $attributes = [
        'username',
        'email:email',
        'updated_at:datetime',
        'created_at:datetime',
        [
            'attribute' => 'status',
            'value' => Yii::t('rbac-admin', $model->status == 0 ? 'INACTIVE' : 'ACTIVE')
        ]
    ];
    if ($model->admin) {
        array_splice($attributes, 2, 0, [
            [
                'attribute' => 'admin',
                'label' => Yii::t('site', 'LAST_VISIT_DATE'),
                'format' => 'datetime',
                'value' => function ($model) {
                    return $model->admin->lastvisitDate;
                }
            ]
        ]);
    }
    ?>
    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => $attributes,
    ])
    ?>

</div>
