<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model \backend\modules\admin\models\User */
/* @var $adminModel \backend\models\Admin */

$this->title = Yii::t('site', 'EDIT_PROFILE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'USERS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'user';

$formatter = Yii::$app->formatter;
?>
<div class="user-profile">
    <div class="btn-toolbar">
        <?= Html::a(Yii::t('rbac-admin', 'CHANGE_PASSWORD'), ['change-password'], ['class' => 'btn btn-primary btn-sm']) ?>
    </div>
    <hr>
    <?php
    $form = ActiveForm::begin([
        'id' => 'user_form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-lg-2',
                'hint' => 'col-lg-4'
            ]
        ]
    ])
    ?>

    <?= $form->field($adminModel, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->input('email', ['maxlength' => true]) ?>
    <?= $form->field($model, 'created_at')->textInput([
        'value' => $formatter->asDatetime($model->created_at),
        'disabled' => true
    ]) ?>
    <?= $form->field($adminModel, 'lastvisitDate')->textInput([
        'value' => $formatter->asDatetime($adminModel->lastvisitDate),
        'disabled' => true
    ]) ?>
    <?= $form->field($model, 'status')->dropDownList([
        0 => Yii::t('rbac-admin', 'INACTIVE'),
        10 => Yii::t('rbac-admin', 'ACTIVATE')
    ], ['disabled' => true]) ?>
    <?= $form->field($model, 'id')->input('number', ['disabled' => true]) ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-success']) ?>
            <?= Html::resetButton(Yii::t('common', 'RESET'), ['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
