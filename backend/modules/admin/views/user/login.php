<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Helper;
use backend\themes\basic\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \backend\modules\admin\models\form\Login */

AppAsset::register($this);
(new AppAsset(['sourcePath' => '@backend/modules/admin/assets']))->addCss('main.css', true, '');

$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', strtoupper(Helper::getClient()));

$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::t('rbac-admin', 'LOGIN')) ?> - <?= $appName ?> - <?= $client ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid site-login">

    <p class="text-center"><?= Yii::t('rbac-admin', 'USER_LOGIN_FORM_HEAD') ?></p>

    <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div style="color:#999;margin:1em 0">
                <?= Yii::t('rbac-admin', 'USER_RESET_PASSWORD_AND_SIGNUP', [
                    'reset' => Html::a(Yii::t('rbac-admin', 'RESET_IT'), ['user/request-password-reset']),
                    'signup' => Html::a(Yii::t('rbac-admin', 'SIGNUP'), ['user/signup'])
                ]) ?>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('rbac-admin', 'LOGIN'), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-4"></div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
