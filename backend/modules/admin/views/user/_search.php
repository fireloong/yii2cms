<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\components\Icon;

/* @var $this \yii\web\View */
/* @var $model \backend\modules\admin\models\searchs\User */
/* @var $type */

$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\'});');
$typeList = [
    'admin' => Yii::t('rbac-admin', 'TYPE_ADMIN_LABEL'),
    'member' => Yii::t('rbac-admin', 'TYPE_MEMBER_LABEL')
];
?>
<div class="user-search">
    <div class="btn-group pull-left" style="margin-right: 5px;">
        <?= Html::button($typeList[$type] . ' ' . Html::tag('span', '', ['class' => 'caret']), [
            'class' => 'btn btn-default dropdown-toggle',
            'data-toggle' => 'dropdown',
            'aria-haspopup' => 'true',
            'aria-expanded' => 'false'
        ]) ?>

        <ul class="dropdown-menu">
            <?php
            foreach ($typeList as $key => $value) {
                $activeOptions = $key == $type ? ['class' => 'active'] : [];
                echo Html::tag('li', Html::a($value, ['index', 'type' => $key]), $activeOptions);
            }
            ?>
        </ul>
    </div>
    <?php
    $form = ActiveForm::begin([
        'action' => ['index', 'type' => $type],
        'method' => 'get',
        'layout' => 'inline',
        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'sr-only'
            ]
        ]
    ]);
    ?>
    <?php
    $button = Html::submitButton(Icon::i('fa-search'), [
        'class' => 'btn btn-default',
        'title' => Yii::t('common', 'SEARCH'),
        'data-toggle' => 'tooltip'
    ]);
    $addon = Html::tag('span', $button, ['class' => 'input-group-btn']);
    $searchInputTemplate = Html::tag('div', '{input}' . $addon, ['class' => 'input-group']);
    ?>
    <?=
    $form->field($model, 'username', [
        'inputOptions' => [
            'title' => Yii::t('rbac-admin', 'SEARCH_USERNAME'),
            'placeholder' => Yii::t('common', 'SEARCH'),
            'data-toggle' => 'tooltip'
        ],
        'inputTemplate' => $searchInputTemplate,
    ])->textInput(['maxlength' => true])
    ?>
    <?=
    $form->field($model, 'limit', [
        'options' => [
            'class' => 'form-group pull-right'
        ]
    ])->dropDownList([
        '5' => '5', '15' => '15', '20' => '20',
        '30' => '30', '50' => '50', '100' => '100',
        '0' => Yii::t('common', 'ALL')
    ])
    ?>
    <?php ActiveForm::end(); ?>
</div>
