<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this  yii\web\View */
/* @var $model backend\modules\admin\models\BizRule */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\modules\admin\models\searchs\BizRule */

$this->title = Yii::t('rbac-admin', 'RULES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'rule';
?>
<div class="role-index">

    <p>
        <?= Html::a(Yii::t('rbac-admin', 'CREATE'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('rbac-admin', 'NAME'),
            ],
            ['class' => 'yii\grid\ActionColumn',],
        ],
    ]);
    ?>

</div>
