<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model backend\modules\admin\models\BizRule */

$this->title = Yii::t('rbac-admin', 'CREATE_RULE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'RULES'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">

    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>

</div>
