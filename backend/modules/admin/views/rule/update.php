<?php

use yii\helpers\Html;

/* @var $this  yii\web\View */
/* @var $model backend\modules\admin\models\BizRule */

$this->title = Yii::t('rbac-admin', 'UPDATE_RULE') . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'RULES'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'UPDATE');
?>
<div class="auth-item-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>
</div>
