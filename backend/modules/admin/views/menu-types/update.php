<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\MenuTypes */
/* @var $clients backend\modules\admin\controllers\MenuTypesController */

$this->title = Yii::t('rbac-admin', 'UPDATE_MENU_TYPES');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENUS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $model->title), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'UPDATE');
?>
<div class="menu-types-update">

    <?= $this->render('_form', [
        'model' => $model,
        'clients' => $clients
    ]) ?>

</div>
