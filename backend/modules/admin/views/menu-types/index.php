<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\admin\models\searchs\MenuTypes */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $clientList array */

$this->title = Yii::t('rbac-admin', 'MENUS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'menu-types';
?>
<div class="menu-types-index">
    <p class="btn-tools">
        <?= Html::a(Yii::t('rbac-admin', 'NEW'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= $this->render('_search', ['model' => $searchModel, 'clientList' => $clientList]) ?>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => '{items}{summary}{pager}',
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'menutype',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->title);
                }
            ],
            [
                'attribute' => 'description',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->description);
                }
            ],
            'id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
