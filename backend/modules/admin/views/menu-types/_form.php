<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\components\Helper;
use backend\modules\languages\models\form\Filter;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\MenuTypes */
/* @var $clients backend\modules\admin\controllers\MenuTypesController */
/* @var $form yii\bootstrap\ActiveForm */

$this->params['active'] = 'menu-types';
?>

<div class="menu-types-form">

    <?php
    $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'offset' => 'col-sm-offset-2',
                'wrapper' => 'col-sm-5'
            ]
        ]
    ]);
    ?>

    <?php if ($model->getIsNewRecord()): ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])
            ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                '/languages/overrides/index'
            ], ['target' => '_blank']).Html::tag('span',' rbac-admin')) ?>
    <?php else: ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true])
            ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                '/languages/overrides/update',
                'id' => Filter::getId(Helper::getClient(), 'rbac-admin', $model->title)
            ], ['target' => '_blank']).Html::tag('span',' rbac-admin')) ?>
    <?php endif; ?>
    <?= $form->field($model, 'menutype')->textInput(['maxlength' => true]) ?>

    <?php if ($model->getIsNewRecord()): ?>
        <?= $form->field($model, 'description')->textInput(['maxlength' => true])
            ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                '/languages/overrides/index'
            ], ['target' => '_blank']).Html::tag('span',' rbac-admin')) ?>
    <?php else: ?>
        <?= $form->field($model, 'description')->textInput(['maxlength' => true])
            ->hint(Html::a(Yii::t('common', 'LANGUAGES'), [
                '/languages/overrides/update',
                'id' => Filter::getId(Helper::getClient(), 'rbac-admin', $model->description)
            ], ['target' => '_blank']).Html::tag('span',' rbac-admin')) ?>
    <?php endif; ?>

    <?= $form->field($model, 'client_id')->dropDownList($clients) ?>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('rbac-admin', 'SAVE'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
