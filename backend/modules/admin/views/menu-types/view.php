<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\MenuTypes */

$this->title = Yii::t('rbac-admin', $model->title);
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENUS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'menu-types';
\yii\web\YiiAsset::register($this);
?>
<div class="menu-types-view">

    <p>
        <?= Html::a(Yii::t('yii', 'UPDATE'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('yii', 'DELETE'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'menutype',
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->title);
                }
            ],
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->description);
                }
            ],
            [
                'attribute' => 'client_id',
                'value' => function ($model) {
                    $client = \backend\models\Clients::findOne($model->client_id);
                    return $client->name;
                }
            ],
        ],
    ]) ?>

</div>
