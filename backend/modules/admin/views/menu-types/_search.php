<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\components\Icon;

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\searchs\MenuTypes */
/* @var $form yii\widgets\ActiveForm */
/* @var $clientList array */

$css = <<<CSS
.tooltip .tooltip-inner{white-space:nowrap;}
CSS;
$this->registerCss($css);

$js = <<<JS
$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
$('select[id^="menutypes-"]').change(function(){
    $(this).closest('form').submit();
});
$('.btn-clear').click(function(){
    $('#menutypes-search').val('');
    $(this).closest('form').submit();
});
JS;
$this->registerJs($js);
?>

<div class="menu-types-search">

    <?php
    $form = ActiveForm::begin([
        'layout' => 'inline',
        'fieldConfig' => ['labelOptions' => ['class' => 'sr-only']],
        'action' => ['index'],
        'method' => 'post',
    ]);
    $button = Html::submitButton(Icon::i('search'), [
        'class' => 'btn btn-default',
        'title' => Yii::t('common', 'SEARCH'),
        'data-toggle' => 'tooltip'
    ]);
    $addon = Html::tag('span', $button, ['class' => 'input-group-btn']);
    $searchInputTemplate = Html::tag('div', '{input}' . $addon, ['class' => 'input-group']);
    ?>

    <?= $form->field($model, 'client_id')->dropDownList($clientList) ?>
    <?=
    $form->field($model, 'search', [
        'inputOptions' => [
            'title' => Yii::t('rbac-admin', 'MENU_TYPES_SEARCH_TIP'),
            'placeholder' => $model->getAttributeLabel('search'),
            'data-toggle' => 'tooltip'
        ],
        'inputTemplate' => $searchInputTemplate,
    ])->textInput(['maxlength' => true])
    ?>
    <?= Html::button(Yii::t('common', 'CLEAR'), ['class' => 'btn btn-default btn-clear']) ?>

    <?=
    $form->field($model, 'limit', [
        'options' => ['class' => 'form-group pull-right']
    ])->dropDownList([
        '5' => '5', '15' => '15', '20' => '20', '30' => '30',
        '50' => '50', '100' => '100', '0' => Yii::t('common', 'ALL')])
    ?>

    <?php ActiveForm::end(); ?>

</div>
