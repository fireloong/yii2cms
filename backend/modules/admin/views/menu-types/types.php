<?php

use yii\helpers\Html;

/**
 * @var array $options
 */

$this->params['navShow'] = false;
$this->params['submenusShow'] = false;
$this->params['breadcrumbs'] = false;
$this->params['footerShow'] = false;

$this->title = Yii::t('rbac-admin', 'MENU_ITEM_TYPES');

$css = <<<CSS
body{padding-top: 0;}.zmh-main{padding-bottom: 0;}.main{padding: 10px;}.panel-heading{padding: 0;}
.panel-heading .panel-title > a{padding: 10px 15px;display: block;font-weight: 600;font-size: 14px;}
.list-group{margin-bottom: 0;}a.list-group-item,a.list-group-item:hover,a.list-group-item:focus{color: #337ab7}
a.list-group-item > small{margin-left: 5px;}
CSS;
$this->registerCss($css);
?>
<div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
    <?php foreach ($options as $key => $option): ?>
        <div class="panel panel-default">
            <div id="heading_<?= $option['name'] ?>" class="panel-heading" role="tab">
                <h4 class="panel-title">
                    <?= Html::a($key, '#' . $option['name'], [
                        'class' => 'collapsed',
                        'role' => 'button',
                        'data' => [
                            'toggle' => 'collapse',
                            'parent' => '#accordion'
                        ],
                        'aria-expanded' => 'false',
                        'aria-controls' => $option['name']
                    ]) ?>
                </h4>
            </div>
            <?php
            $items = '';
            foreach ($option['items'] as $item) {
                $title = Html::tag('span', Yii::t($item->langCat, $item->title));
                $description = Html::tag('small', Yii::t($item->langCat, $item->description), ['class' => 'text-muted']);
                $items .= Html::a($title . $description, '#', [
                    'class' => 'list-group-item',
                    'data' => [
                        'route' => $item->route,
                        'type' => $item->type,
                        'langcat' => $item->langCat,
                        'path' => base64_encode($item->path)
                    ]
                ]);
            }
            $listGroup = Html::tag('div', $items, ['class' => 'list-group']);
            ?>
            <?= Html::tag('div', Html::tag('div', $listGroup, ['class' => 'panel-body']), [
                'id' => $option['name'],
                'class' => 'panel-collapse collapse',
                'role' => 'tabpanel',
                'aria-labelledby' => 'heading_' . $option['name']
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>
