<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\MenuTypes */
/* @var $clients backend\modules\admin\controllers\MenuTypesController */

$this->title = Yii::t('rbac-admin', 'CREATE_MENU_TYPES');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', 'MENUS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-types-create">
    <?= $this->render('_form', [
        'model' => $model,
        'clients' => $clients
    ]) ?>

</div>
