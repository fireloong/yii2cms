<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $context backend\modules\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', 'CREATE_' . strtoupper($labels['Item']));
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', strtoupper($labels['Items'])), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = strtolower($labels['Item']);
?>
<div class="auth-item-create">
    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>

</div>
