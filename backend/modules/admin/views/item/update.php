<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\AuthItem */
/* @var $context backend\modules\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', 'UPDATE_' . strtoupper($labels['Item'])) . ': ' . Yii::t('rbac-admin', $model->name);
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', strtoupper($labels['Items'])), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac-admin', $model->name), 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('rbac-admin', 'UPDATE');
$this->params['active'] = strtolower($labels['Item']);
?>
<div class="auth-item-update">
    <?=
    $this->render('_form', [
        'model' => $model,
    ]);
    ?>
</div>
