<?php

use yii\helpers\Html;
use yii\grid\GridView;
use backend\components\Icon;
use backend\modules\admin\components\RouteRule;
use backend\modules\admin\components\Configs;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel backend\modules\admin\models\searchs\AuthItem */
/* @var $context backend\modules\admin\components\ItemController */

$context = $this->context;
$labels = $context->labels();
$this->title = Yii::t('rbac-admin', strtoupper($labels['Items']));
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = strtolower($labels['Item']);

$rules = array_keys(Configs::authManager()->getRules());
$rules = array_combine($rules, $rules);
unset($rules[RouteRule::RULE_NAME]);
?>
<div class="role-index">

    <p>
        <?= Html::a(Yii::t('rbac-admin', 'CREATE_' . strtoupper($labels['Item'])), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => Yii::t('rbac-admin', 'NAME'),
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->name);
                }
            ],
            [
                'attribute' => 'ruleName',
                'label' => Yii::t('rbac-admin', 'RULE_NAME'),
                'filter' => $rules
            ],
            [
                'attribute' => 'description',
                'label' => Yii::t('rbac-admin', 'DESCRIPTION'),
                'value' => function ($model) {
                    return Yii::t('rbac-admin', $model->description);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        if ($model->type == 1 && in_array($model->name, [
                                'ROLE_SUPER_ADMINISTRATOR',
                                'ROLE_REGISTERED',
                                'ROLE_PUBLIC'
                            ])) {
                            return '';
                        } else {
                            return Html::a(Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']), $url, [
                                'title' => Yii::t('common', 'DELETE'),
                                'aria-label' => Yii::t('common', 'DELETE'),
                                'data' => [
                                    'pjax' => '0',
                                    'method' => 'post',
                                    'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM')
                                ]
                            ]);
                        }
                    }
                ]
            ],
        ],
    ])
    ?>

</div>
