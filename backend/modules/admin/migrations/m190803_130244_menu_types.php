<?php

use yii\db\Migration;

/**
 * Class m190803_130244_menu_types
 */
class m190803_130244_menu_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%menu_types}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'menutype' => $this->string(24)->notNull()->unique(),
            'title' => $this->string(48)->notNull(),
            'description' => $this->string(255)->notNull()->defaultValue(''),
            'client_id' => $this->integer()->notNull()->defaultValue(0)
                ], $tableOptions);

        $this->insert('{{%menu_types}}', [
            'menutype' => 'main',
            'title' => 'main menu',
            'description' => 'Background main menu',
            'client_id' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%menu_types}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190803_130244_menu_types cannot be reverted.\n";

      return false;
      }
     */
}
