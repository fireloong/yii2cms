<?php

use backend\modules\admin\components\Configs;

/**
 * Migration table of table_menu
 * 
 * @author Misbahul D Munir <misbahuldmunir@gmail.com>
 * @since 1.0
 */
class m140602_111327_create_menu_table extends \yii\db\Migration
{

    /**
     * @inheritdoc
     */
    public function up()
    {
        $menuTable = Configs::instance()->menuTable;
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable($menuTable, [
            'id' => $this->primaryKey(),
            'name' => $this->string(128)->notNull(),
            'menutype' => $this->string(24)->notNull(),
            'alias' => $this->string(200)->notNull(),
            'type' => $this->string(16)->notNull(),
            'published' => $this->tinyInteger(3)->notNull()->defaultValue(0),
            'parent' => $this->integer(),
            'level' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'route' => $this->string()->notNull(),
            'language' => $this->char(7)->notNull()->defaultValue(''),
            'lft' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'rgt' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'ordering' => $this->integer()->notNull()->defaultValue(0),
            'data' => $this->binary(),
            "FOREIGN KEY ([[parent]]) REFERENCES {$menuTable}([[id]]) ON DELETE SET NULL ON UPDATE CASCADE",
            ], $tableOptions);

        $this->createIndex('idx_left_right', $menuTable, ['lft', 'rgt']);
//        $this->addForeignKey('idx-parent', $menuTable, 'id', $menuTable, 'parent');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $menuTable = Configs::instance()->menuTable;
        $this->dropIndex('idx_left_right', $menuTable);
        $this->dropTable($menuTable);
    }

}
