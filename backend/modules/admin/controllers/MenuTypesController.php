<?php

namespace backend\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\components\Helper;
use backend\models\Clients;
use backend\models\Extensions;
use backend\modules\admin\models\MenuTypes;
use backend\modules\languages\models\Overrides;
use backend\modules\admin\models\searchs\MenuTypes as MenuTypesSearch;

/**
 * MenuTypesController implements the CRUD actions for MenuTypes model.
 */
class MenuTypesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MenuTypes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MenuTypesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->bodyParams);

        $clientList = Clients::getList();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'clientList' => $clientList
        ]);
    }

    /**
     * Displays a single MenuTypes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MenuTypes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MenuTypes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $client = Clients::findOne(['name' => Helper::getClient()]);
            $overrides = new Overrides();
            $overrides->edit('rbac-admin', $model->title, null, Yii::$app->language, $client->id, null, 'menu-types');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $clients = Clients::getList();

        return $this->render('create', [
            'model' => $model,
            'clients' => $clients
        ]);
    }

    /**
     * Updates an existing MenuTypes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $client = Clients::findOne(['name' => Helper::getClient()]);
            $overrides = new Overrides();
            $overrides->edit('rbac-admin', $model->title, null, Yii::$app->language, $client->id, null, 'menu-types');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $clients = Clients::getList();

        return $this->render('update', [
            'model' => $model,
            'clients' => $clients
        ]);
    }

    /**
     * Deletes an existing MenuTypes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionTypes($client_id)
    {
        $menuTypeModel = new MenuTypes();

        $options = $menuTypeModel->getOptions($client_id);

        if ($options === false) {
            throw new NotFoundHttpException(Yii::t('common', 'REQUESTED_PAGE_NOT_EXIST'));
        }

        return $this->render('types', ['options' => $options]);
    }

    /**
     * Finds the MenuTypes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenuTypes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MenuTypes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('common', 'REQUESTED_PAGE_NOT_EXIST'));
    }
}
