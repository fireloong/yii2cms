<?php

namespace backend\modules\categories;

use Yii;
use common\components\Helper;

/**
 * categories module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->setViewPath('@app' . Yii::$app->view->theme->baseUrl . '/mod_' . $this->id);
        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $getExtension = Yii::$app->request->get('extension');
            if (empty($getExtension) || $getExtension === 'mod_content') {
                Helper::addEntries([
                    [
                        'label' => Yii::t('mod_content', 'SUBMENU_ARTICLES'),
                        'url' => ['/content/articles'],
                        'key' => 'articles'
                    ],
                    [
                        'label' => Yii::t('mod_content', 'SUBMENU_CATEGORIES'),
                        'url' => ['/categories/default', 'extension' => 'mod_content'],
                        'key' => 'categories_content'
                    ],
                    [
                        'label' => Yii::t('mod_content', 'SUBMENU_FEATURED'),
                        'url' => ['/content/featured'],
                        'key' => 'featured'
                    ]
                ]);
            }
            return true;
        }
        return false;
    }
}
