<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use backend\components\Icon;

/* @var $this \yii\web\View */
/* @var $searchModel backend\modules\categories\models\searchs\Categories */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $extension */

$this->title = Yii::t('mod_categories', 'CONTENT_CATEGORIES');
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('mod_categories', 'CATEGORIES'),
    'url' => ['/categories/default', 'extension' => $extension]
];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'categories_content';

$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\'});');
?>
<div class="categories-default-index">
    <p class="btn-tools">
        <?= Html::a(Yii::t('common', 'NEW'), ['create', 'extension' => $extension], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => '{items}{summary}{pager}',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'published',
                'format' => 'raw',
                'value' => function ($model) {
                    $status = [
                        1 => Html::a(Icon::i('fa-check', ['class' => 'text-success']), '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('mod_categories', 'UNPUBLISH'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id
                                ]
                            ]
                        ]),
                        0 => Html::a(Icon::i('fa-times-circle', ['class' => 'text-danger']), '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('mod_categories', 'PUBLISH'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id
                                ]
                            ]
                        ]),
                        2 => Html::a(Icon::i('fa-trash-o'), '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('mod_categories', 'PUBLISH'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id
                                ]
                            ]
                        ])
                    ];
                    return $status[$model->published];
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    $alias = ' (' . Yii::t('mod_categories', 'FIELD_ALIAS_LABEL') . ': ' . $model->alias . ')';
                    $prefix = ($model->level > 0 ? '&ensp;' : '') . str_repeat('-&ensp;', $model->level);
                    return $prefix . $model->title . Html::tag('small', $alias, ['class' => 'text-muted']);
                }
            ],
            [
                'attribute' => 'language',
                'value' => function ($model) {
                    return $model->language === '*' ? Yii::t('mod_langs', 'ALL_LANGUAGES') : $model->language;
                }
            ],
            'id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ]
        ]
    ]) ?>
</div>
