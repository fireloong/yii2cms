<?php

use yii\bootstrap\Html;
use common\components\form\Form;

/* @var $this yii\web\View */
/* @var $model backend\modules\categories\models\Categories */
/* @var $form yii\widgets\ActiveForm */
/* @var $extension */
/* @var $languageList */
/* @var $parentList */

$css = <<<CSS
.form-inline .form-group{margin-right: 10px;}
#categories-alias{width: 220px;}
@media (min-width: 768px) {
  .form-inline .form-group{vertical-align: top;}
}
#categories_form .nav.nav-tabs{margin-bottom: 1.5rem;}
CSS;
$this->registerCss($css);
?>
<div class="row">
    <?php $form = Form::begin(['id' => 'categories_form']); ?>
    <?= $form->field($model, 'extension', 'hidden', ['inputOptions' => ['value' => $extension]]) ?>
    <div class="col-sm-12 form-inline">
        <?= $form->field($model, 'title', 'text') ?>
        <?= $form->field($model, 'alias', 'text', ['inputOptions' => [
            'placeholder' => Yii::t('mod_categories', 'ALIAS_PLACEHOLDER'),
            'size' => 45
        ]]) ?>
    </div>
    <div class="col-sm-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="presentation">
                <?= Html::a(Yii::t('mod_categories', 'CATEGORY'), '#general', [
                    'aria-controls' => 'general',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
            <li role="presentation">
                <?= Html::a(Yii::t('mod_categories', 'BASIC_OPTIONS'), '#attrib-basic', [
                    'aria-controls' => 'attrib-basic',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
            <li role="presentation">
                <?= Html::a(Yii::t('mod_categories', 'PUBLISHING_OPTIONS'), '#publishing', [
                    'aria-controls' => 'publishing',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
        </ul>
        <div class="tab-content">
            <div id="general" class="tab-pane fade in active" role="tabpanel">
                <div class="row">
                    <div class="col-sm-9">
                        <?= $form->field($model, 'description', 'editor') ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'parent_id', 'dropDownList', ['setupParams' => [$parentList]]) ?>
                        <?= $form->field($model, 'published', 'dropDownList', ['setupParams' => [[
                            '1' => Yii::t('common', 'PUBLISHED'),
                            '0' => Yii::t('common', 'UNPUBLISHED'),
                            '2' => Yii::t('common', 'TRASHED')
                        ]]]) ?>
                        <?= $form->field($model, 'language', 'dropDownList', ['setupParams' => [$languageList]]) ?>
                        <?= $form->field($model, 'note', 'text') ?>
                    </div>
                </div>
            </div>
            <div id="attrib-basic" class="tab-pane fade" role="tabpanel">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'params[category_layout]', 'dropDownList', [
                            'layout' => 'horizontal',
                            'setupParams' => [[
                                '' => Yii::t('mod_categories', 'USER_DEFAULT_SETTING')
                            ]]
                        ])->label(Yii::t('mod_categories', 'FIELD_CATEGORY_LAYOUT_LABEL')) ?>

                        <?= $form->field($model, 'params[image]', 'media', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_categories', 'FIELD_IMAGE_LABEL')) ?>

                        <?= $form->field($model, 'params[image_alt]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_categories', 'FIELD_IMAGE_ALT_LABEL')) ?>
                    </div>
                </div>
            </div>
            <div id="publishing" class="tab-pane fade" role="tabpanel">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'created_at', 'datetime', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'created_by', 'number', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'modified_at', 'text', [
                            'layout' => 'horizontal',
                            'inputOptions' => [
                                'disabled' => true,
                                'value' => Yii::$app->formatter->asDatetime($model->modified_at)
                            ]
                        ]) ?>
                        <?= $form->field($model, 'modified_by', 'number', [
                            'layout' => 'horizontal',
                            'inputOptions' => ['disabled' => true]
                        ]) ?>
                        <?= $form->field($model, 'hits', 'number', [
                            'layout' => 'horizontal',
                            'inputOptions' => ['disabled' => true]
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'metadesc', 'textarea', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'metakey', 'textarea', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'metadata[author]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_categories', 'FIELD_AUTHOR_LABEL')) ?>
                        <?= $form->field($model, 'metadata[robots]', 'dropDownList', [
                            'layout' => 'horizontal',
                            'setupParams' => [[
                                '' => Yii::t('mod_categories', 'USER_DEFAULT_SETTING'),
                                'index, follow' => Yii::t('mod_categories', 'INDEX_AND_FOLLOW'),
                                'noindex, follow' => Yii::t('mod_categories', 'NOINDEX_AND_FOLLOW'),
                                'index, nofollow' => Yii::t('mod_categories', 'INDEX_AND_NOFOLLOW'),
                                'noindex, nofollow' => Yii::t('mod_categories', 'NOINDEX_AND_NOFOLLOW')
                            ]]
                        ])->label(Yii::t('mod_categories', 'FIELD_ROBOTS_LABEL')) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php Form::end(); ?>
</div>