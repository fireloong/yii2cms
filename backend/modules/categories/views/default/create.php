<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\categories\models\Categories */
/* @var $extension */
/* @var $languageList */
/* @var $parentList */

$this->title = Yii::t('mod_categories', 'CREATE_CATEGORY_' . strtoupper($extension));
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('mod_categories', 'CATEGORIES'),
    'url' => ['/categories/default', 'extension' => $extension]
];
$this->params['breadcrumbs'][] = $this->title;
$this->params['submenusShow'] = false;
?>
<div class="categories-create">
    <?= $this->render('_form', [
        'model' => $model,
        'extension' => $extension,
        'languageList' => $languageList,
        'parentList' => $parentList
    ]) ?>
</div>
