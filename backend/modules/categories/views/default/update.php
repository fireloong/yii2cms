<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\categories\models\Categories */
/* @var $languageList */
/* @var $parentList */

$this->title = Yii::t('mod_categories', 'UPDATE_CATEGORY_' . strtoupper($model->extension));
$this->params['breadcrumbs'][] = [
    'label' => Yii::t('mod_categories', 'CATEGORIES'),
    'url' => ['/categories/default', 'extension' => $model->extension]
];
$this->params['breadcrumbs'][] = $this->title;
$this->params['submenusShow'] = false;
?>
<div class="categories-update">
    <?= $this->render('_form', [
        'model' => $model,
        'extension' => $model->extension,
        'languageList' => $languageList,
        'parentList' => $parentList
    ]) ?>
</div>
