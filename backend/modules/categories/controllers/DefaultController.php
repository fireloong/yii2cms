<?php

namespace backend\modules\categories\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use backend\modules\languages\models\Languages;
use backend\modules\categories\models\Categories;
use backend\modules\categories\models\searchs\Categories as CategoriesSearch;

/**
 * DefaultController implements the CRUD actions for Categories model.
 */
class DefaultController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @param string $extension
     * @return string
     */
    public function actionIndex($extension = 'mod_content')
    {
        $searchModel = new CategoriesSearch(['extension' => $extension]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'extension' => $extension
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param string $extension
     * @return mixed
     */
    public function actionCreate($extension = 'mod_content')
    {
        $model = new Categories();

        if (Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $model->setLftRgt($extension);
                $session->setFlash('success', Yii::t('common', 'SAVE_SUCCESS'));
                return $this->redirect(['index']);
            } else {
                $session->setFlash('error', Yii::t('common', 'SAVE_FAILED'));
                return $this->refresh();
            }
        }

        $languageList = Languages::getList();

        $parentList =  ArrayHelper::merge(['0' => Yii::t('common', 'ROOT_PARENT')], $model->getParents($extension));

        return $this->render('create', [
            'model' => $model,
            'extension' => $extension,
            'languageList' => $languageList,
            'parentList' => $parentList
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $model->setLftRgt($model->extension);
                $session->setFlash('success', Yii::t('common', 'SAVE_SUCCESS'));
                return $this->redirect(['index']);
            } else {
                $session->setFlash('error', Yii::t('common', 'SAVE_FAILED'));
                return $this->refresh();
            }
        }

        $languageList = Languages::getList();
        $parentList = ArrayHelper::merge(['0' => Yii::t('common', 'ROOT_PARENT')], $model->getParents($model->extension, $id));

        return $this->render('update', [
            'model' => $model,
            'languageList' => $languageList,
            'parentList' => $parentList
        ]);
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Categories the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categories::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('mod_categories', 'REQUESTED_PAGE_NOT_EXIST'));
    }
}
