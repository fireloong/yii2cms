<?php


namespace backend\modules\actionlogs;

use Yii;

/**
 * description of Module
 *
 * @author FireLoong
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        $this->setViewPath('@app'.Yii::$app->view->theme->baseUrl.'/mod_' . $this->id);
    }
}
