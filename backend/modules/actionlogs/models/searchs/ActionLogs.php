<?php


namespace backend\modules\actionlogs\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\actionlogs\models\ActionLogs as ActionLogsModel;

/**
 * description of ActionLogs
 *
 * @author FireLoong
 */
class ActionLogs extends ActionLogsModel
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['id', 'uid', 'item_id'], 'integer'],
            [['info', 'message', 'log_date', 'ip_address'], 'safe']
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ActionLogsModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'log_date' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'uid' => $this->uid,
            'ip_address' => $this->ip_address
        ]);

        return $dataProvider;
    }
}
