<?php

use yii\bootstrap\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use backend\components\Icon;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\actionlogs\models\searchs\ActionLogs */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mod_actionlogs', 'USER_ACTIONS_LOGS');

$this->params['breadcrumbs'][] = $this->title;

$selEmpty = Yii::t('mod_actionlogs', 'SELECTION_FROM_LIST');
$confirmMsg = Yii::t('mod_actionlogs', 'DELETE_ITEMS_CONFIRM_LOGS');
$js = <<<JS
$('[data-toggle="tooltip"]').tooltip({trigger: 'hover', html: true});
$('#actionlogs_del').click(function(){
    var keys = $('#actionlogs_list').yiiGridView('getSelectedRows');
    if(keys.length){
        yii.confirm('$confirmMsg',function() {
          $('#actionlogs_form').submit();
        });
    }else{
        alert('$selEmpty');
    }
});
$('#actionlogs_sel').click(function(){
    var keys = $('#actionlogs_list').yiiGridView('getSelectedRows');
    if(keys.length){
        $('#actionlogs_form').append('<input type="hidden" name="exportSelectedLogs" value="1">').submit();
    }else{
        alert('$selEmpty');
    }
});
JS;

$this->registerJs($js);

$delBtnText = Icon::i('fa-remove') . ' ' . Yii::t('mod_actionlogs', 'DELETE');
$purBtnText = Icon::i('fa-remove') . ' ' . Yii::t('mod_actionlogs', 'PURGE');
$expSelBtnText = Icon::i('fa-download') . ' ' . Yii::t('mod_actionlogs', 'EXPORT_SELECTED_AS_CSV');
$expAllBtnText = Icon::i('fa-download') . ' ' . Yii::t('mod_actionlogs', 'EXPORT_ALL_AS_CSV');
?>
<div class="actionlogs-index">
    <p class="btn-toolbar">
        <?= Html::a($delBtnText, null, ['id' => 'actionlogs_del', 'class' => 'btn btn-default btn-sm']) ?>
        <?= Html::a($purBtnText, null, [
            'class' => 'btn btn-default btn-sm',
            'data' => [
                'confirm' => Yii::t('mod_actionlogs', 'DELETE_ITEMS_CONFIRM_ALL_LOGS'),
                'method' => 'post',
                'params' => [
                    'purge' => true
                ]
            ]]) ?>
        <?= Html::a($expSelBtnText, null, ['id' => 'actionlogs_sel', 'class' => 'btn btn-default btn-sm']) ?>
        <?= Html::a($expAllBtnText, null, [
            'class' => 'btn btn-default btn-sm',
            'data' => [
                'method' => 'post',
                'params' => [
                    'exportAllLogs' => true
                ]
            ]
        ]) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?= Html::beginForm('', 'post', ['id' => 'actionlogs_form']) .
    GridView::widget([
        'id' => 'actionlogs_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '2%'],
            ],
            [
                'attribute' => 'info',
                'format' => 'raw',
                'value' => function ($model) {
                    $message = json_decode($model->message, true);
                    return Yii::t('plg_actionlog', $model->info, $message);
                }
            ],
            [
                'attribute' => 'log_date',
                'headerOptions' => ['width' => '11%'],
                'format' => 'raw',
                'value' => function ($model) {
                    $formatter = Yii::$app->formatter;
                    $relativeTime = $formatter->asRelativeTime($model->log_date);
                    $datetime = $formatter->asDatetime($model->log_date);
                    return Html::tag('span', $relativeTime, ['data-toggle' => 'tooltip', 'title' => $datetime]);
                }
            ],
            [
                'attribute' => 'ip_address',
                'headerOptions' => ['width' => '9%']
            ],
            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '4%']
            ]
        ]
    ]) . Html::endForm() ?>
    <?php Pjax::end(); ?>
</div>