<?php

namespace backend\modules\languages;

use Yii;
use common\components\Helper;

/**
 * languages module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\languages\controllers';

    /**
     * {@inheritdoc}
     */
    public $defaultRoute = 'installed';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        //$this->setViewPath('@app/themes/basic/mod_' . $this->id);

        // custom initialization code goes here
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Helper::addEntry(Yii::t('mod_langs', 'SUBMENU_INSTALLED'), ['/languages/installed'], 'installed');
            Helper::addEntry(Yii::t('mod_langs', 'SUBMENU_CONTENT'), ['/languages/languages'], 'languages');
            Helper::addEntry(Yii::t('mod_langs', 'SUBMENU_OVERRIDES'), ['/languages/overrides'], 'overrides');
            return true;
        }
        return false;
    }

}
