<?php

namespace backend\modules\languages\models\form;

use Yii;
use yii\base\Model;

/**
 * Description of Overrides
 *
 * @author loong
 */
class Overrides extends Model
{
    public $id;
    public $language;
    public $client;
    public $category;
    public $source_language;
    public $translate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'source_language', 'translate'], 'required'],
            [['source_language', 'translate'], 'string'],
            [['category'], 'string', 'max' => 30],
            [['id', 'language', 'client'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'language' => Yii::t('mod_langs', 'LANGUAGE'),
            'client' => Yii::t('mod_langs', 'CLIENT'),
            'category' => Yii::t('mod_langs', 'CATEGORY'),
            'source_language' => Yii::t('mod_langs', 'OVERRIDE_FIELD_KEY'),
            'translate' => Yii::t('mod_langs', 'OVERRIDE_FIELD_VALUE')
        ];
    }

}
