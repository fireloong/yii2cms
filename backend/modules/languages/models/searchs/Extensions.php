<?php

namespace backend\modules\languages\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Languages;
use backend\models\Extensions as ExtensionsModel;

/**
 * Extensions represents the model behind the search form of `backend\models\Extensions`.
 */
class Extensions extends ExtensionsModel
{
    public $search;
    public $client_id = 1;
    public $limit;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'limit'], 'integer'],
            [['search'], 'string', 'max' => '50']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ExtensionsModel::find()
            ->alias('e')
            ->select([
                'e.id', 'e.element', 'e.name', 'e.client_id', 'e.manifest_cache',
                'version' => 'JSON_EXTRACT(e.manifest_cache,"$.version")',
                'creationDate' => 'JSON_EXTRACT(e.manifest_cache,"$.creationDate")',
                'author' => 'JSON_EXTRACT(e.manifest_cache,"$.author")',
                'authorEmail' => 'JSON_EXTRACT(e.manifest_cache,"$.authorEmail")',
                'l.title_native', 'l.code'
            ])
            ->leftJoin(['l' => Languages::tableName()], 'l.code = e.element')
            ->where(['e.type' => 'language'])
            ->andWhere(['e.status' => 1])
            ->andWhere(['e.enabled' => 1])
            ->orderBy('e.ordering');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $sort = $dataProvider->getSort();
        $sort->attributes['title_native'] = [
            'asc' => ['l.title_native' => SORT_ASC],
            'desc' => ['l.title_native' => SORT_DESC]
        ];
        $sort->attributes['code'] = [
            'asc' => ['l.code' => SORT_ASC],
            'desc' => ['l.code' => SORT_DESC]
        ];
        $sort->attributes['manifest_cache.version'] = [
            'asc' => ['version' => SORT_ASC],
            'desc' => ['version' => SORT_DESC]
        ];
        $sort->attributes['manifest_cache.creationDate'] = [
            'asc' => ['creationDate' => SORT_ASC],
            'desc' => ['creationDate' => SORT_DESC]
        ];
        $sort->attributes['manifest_cache.author'] = [
            'asc' => ['author' => SORT_ASC],
            'desc' => ['author' => SORT_DESC]
        ];
        $sort->attributes['manifest_cache.authorEmail'] = [
            'asc' => ['authorEmail' => SORT_ASC],
            'desc' => ['authorEmail' => SORT_DESC]
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->limit) {
            $dataProvider->pagination->pageSize = $this->limit;
        } else {
            $this->limit = $dataProvider->pagination->pageSize;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'e.client_id' => $this->client_id
        ]);

        $query->andFilterWhere(['or', ['like', 'e.element', $this->search], ['like', 'e.name', $this->search]]);

        return $dataProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'search' => Yii::t('common', 'SEARCH')
        ]);
    }
}
