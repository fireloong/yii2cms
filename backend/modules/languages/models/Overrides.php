<?php

namespace backend\modules\languages\models;

use Yii;

/**
 * This is the model class for table "{{%overrides}}".
 *
 * @property int $id
 * @property string $override_key
 * @property string $override_value
 * @property string $category
 * @property string $language
 * @property int $client_id
 * @property string $type
 * @property int $is_override
 */
class Overrides extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%overrides}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['override_value'], 'string'],
            [['client_id', 'is_override'], 'integer'],
            [['override_key'], 'string', 'max' => 200],
            [['category', 'type'], 'string', 'max' => 20],
            [['language'], 'string', 'max' => 7],
        ];
    }

    /**
     * 编辑语音内容
     * @param string $category
     * @param string $key
     * @param string $value
     * @param string $language
     * @param int $client_id
     * @param int $is_override
     * @param string $type
     */
    public function edit($category, $key, $value, $language, $client_id, $is_override = null, $type = null)
    {
        $model = self::findOne([
            'category' => $category,
            'override_key' => $key,
            'language' => $language,
            'client_id' => $client_id
        ]);
        if ($model) {
            if ($value !== null) {
                $model->override_value = $value;
            }
            if ($is_override !== null) {
                $model->is_override = $is_override;
            }
            if ($type !== null) {
                $model->type = $type;
            }
            $model->save();
        } else {
            $this->override_key = $key;
            if ($value !== null) {
                $this->override_value = $value;
            }
            if ($is_override !== null) {
                $this->is_override = $is_override;
            }
            $this->category = $category;
            $this->language = $language;
            $this->client_id = $client_id;
            if ($type !== null) {
                $this->type = $type;
            }
            $this->save();
        }
    }
}
