<?php

use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\grid\RadioButtonColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\languages\models\searchs\Extensions */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $clientList backend\models\Clients */
/* @var $languagesParams backend\modules\languages\controllers\InstalledController */

$this->title = Yii::t('mod_langs', 'LANGUAGES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'installed';
$css = 'label[for^=cb]{font-weight:normal;cursor:pointer}';
$this->registerCss($css);

$msg = Yii::t('mod_langs', 'SELECTION_FROM_LIST');
$switchUrl = \yii\helpers\Url::to(['switch']);
$js = <<<JS
$('[data-toggle="tooltip"]').tooltip({trigger: 'hover', html: true});
$('#set_default').click(function() {
  var code = $('#languages_form input[name="code"]:checked').val();
  if(code === undefined){
      alert('$msg');
  }else{
      $('#languages_form').submit();
  }
});
$('#switch_language').click(function() {
  var code = $('#languages_form input[name="code"]:checked').val();
  if(code === undefined){
      alert('$msg');
  }else{
      var jqform = $('#languages_form');
      var action = jqform.attr('action');
      jqform.append('<input type="hidden" name="action" value="'+action+'">').attr('action','$switchUrl').submit();
  }
});
JS;
$this->registerJs($js);

$defaultBtnText = Html::icon('star', ['class' => 'icon-default']) . ' ' . Yii::t('mod_langs', 'DEFAULT');
$switchBtnText = Html::icon('repeat') . ' ' . Yii::t('mod_langs', 'SWITCH_LANGUAGE');
$installBtnText = Html::icon('save') . ' ' . Yii::t('mod_langs', 'INSTALL_LANGUAGES');
$client = strtolower($clientList[$searchModel->client_id]);
?>
<div class="extensions-index">
    <p class="btn-toolbar">
        <?= Html::a($defaultBtnText, null, ['id' => 'set_default', 'class' => 'btn btn-default btn-sm']) ?>
        <?= $client === 'backend' ? Html::a($switchBtnText, null, ['id' => 'switch_language', 'class' => 'btn btn-default btn-sm']) : '' ?>
        <?= Html::a($installBtnText, ['/extensions/languages/index'], ['class' => 'btn btn-default btn-sm']) ?>
    </p>
    <?= $this->render('_search', ['model' => $searchModel, 'clientList' => $clientList]); ?>
    <?= Html::beginForm('', 'post', ['id' => 'languages_form']) ?>
    <?= Html::hiddenInput('client', $client) ?>
    <?=
    GridView::widget([
        'id' => 'languages_list',
        'layout' => '{items}{pager}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => RadioButtonColumn::class,
                'headerOptions' => ['width' => '30'],
                'name' => 'code',
                'radioOptions' => function ($model) {
                    return [
                        'id' => 'cb' . $model->id,
                        'value' => $model->code
                    ];
                }
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::label($model->name, 'cb' . $model->id);
                }
            ],
            'title_native',
            'code',
            [
                'label' => Yii::t('mod_langs', 'DEFAULT'),
                'format' => 'raw',
                'value' => function ($model) use ($client, $languagesParams) {
                    $isDefault = $languagesParams->$client == $model->code;
                    $icon = $isDefault ? Html::icon('star', ['class' => 'icon-default']) : Html::icon('star-empty');
                    $html = Html::a($icon, null, [
                        'class' => 'btn btn-default btn-xs' . ($isDefault ? ' disabled' : ''),
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                'client' => $client,
                                'code' => $model->code
                            ]
                        ]
                    ]);
                    return Html::tag('span', $html, [
                        'title' => Yii::t('mod_langs', $isDefault ? 'DEFAULT' : 'SET_DEFAULT'),
                        'data' => [
                            'toggle' => 'tooltip'
                        ]
                    ]);
                }
            ],
            [
                'attribute' => 'manifest_cache.version',
                'label' => Yii::t('mod_langs', 'VERSION'),
                'format' => 'raw',
                'value' => function ($model) {
                    $version = $model->manifest_cache['version'];
                    $compare = version_compare(Yii::$app->version, $version) === 0 ? 'success' : 'warning';
                    $label = Html::tag('span', $version, ['class' => 'label label-' . $compare]);
                    return $label;
                }
            ],
            [
                'attribute' => 'manifest_cache.creationDate',
                'label' => Yii::t('common', 'DATE'),
                'value' => function ($model) {
                    return $model->manifest_cache['creationDate'];
                }
            ],
            [
                'attribute' => 'manifest_cache.author',
                'label' => Yii::t('mod_langs', 'AUTHOR'),
                'value' => function ($model) {
                    return $model->manifest_cache['author'];
                }
            ],
            [
                'attribute' => 'manifest_cache.authorEmail',
                'label' => Yii::t('mod_langs', 'AUTHOR_EMAIL'),
                'value' => function ($model) {
                    return $model->manifest_cache['authorEmail'];
                }
            ],
            //'folder',
            //'client_id',
            //'enabled',
            //'protected',
            //'manifest_cache:ntext',
            //'params:ntext',
            //'ordering',
            //'status',
            'id',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    <?= Html::endForm() ?>
</div>
