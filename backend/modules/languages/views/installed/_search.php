<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\components\Icon;

/* @var $this yii\web\View */
/* @var $model backend\modules\languages\models\searchs\Extensions */
/* @var $form yii\widgets\ActiveForm */
/* @var $clientList backend\models\Clients */

$js = <<<JS
$('select[id^="extensions-"]').change(function(){
    $(this).closest('form').submit();
});
$('.btn-clear').click(function(){
    $('#extensions-search').val('');
    $(this).closest('form').submit();
});
JS;
$this->registerJs($js);
?>

<div class="extensions-search">

    <?php
    $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
                'layout' => 'inline',
                'fieldConfig' => [
                    'labelOptions' => [
                        'class' => 'sr-only'
                    ]
                ]
    ]);
    ?>

    <?= $form->field($model, 'client_id')->dropDownList($clientList) ?>

    <?php
    $button = Html::submitButton(Icon::i('fa-search'), ['class' => 'btn btn-default']);
    $addon = Html::tag('span', $button, ['class' => 'input-group-btn']);
    $searchInputTemplate = Html::tag('div', '{input}' . $addon, ['class' => 'input-group']);
    ?>

    <?=
    $form->field($model, 'search', [
        'inputOptions' => [
            'title' => Yii::t('mod_langs', 'FILTER_SEARCH_DESC'),
            'placeholder' => $model->getAttributeLabel('search'),
            'data-toggle' => 'tooltip'
        ],
        'inputTemplate' => $searchInputTemplate,
    ])->textInput(['maxlength' => true])
    ?>

    <?= Html::button(Yii::t('common', 'CLEAR'), ['class' => 'btn btn-default btn-clear']) ?>

    <?=
    $form->field($model, 'limit', [
        'options' => [
            'class' => 'form-group pull-right'
        ]
    ])->dropDownList(['5' => '5', '15' => '15', '20' => '20', '30' => '30', '50' => '50', '100' => '100', '0' => Yii::t('common', 'ALL')])
    ?>

    <?php ActiveForm::end(); ?>

</div>
