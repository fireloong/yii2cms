<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\languages\models\Languages */

$this->title = Yii::t('mod_langs', 'NEW_CONTENT_LANGUAGE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_langs', 'SUBMENU_CONTENT'), 'url' => ['/langs/languages']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languages-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
