<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\languages\models\searchs\Languages */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mod_langs', 'SUBMENU_CONTENT');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'languages';

$css = '.label-code{font-weight:400;cursor:pointer;}.tooltip .tooltip-inner{text-align:left;max-width:none;}';
$this->registerCss($css);

$js = '$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\', html: true});';
$this->registerJs($js);
?>
<div class="languages-index">
    <p>
        <?= Html::a(Yii::t('mod_langs', 'NEW'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]);  ?>

    <?=
    GridView::widget([
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '1%'],
                'checkboxOptions' => function($model) {
                    return ['id' => 'cb' . $model->id];
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function($model) {
                    $a = Html::a($model->title, ['update', 'id' => $model->id]);
                    return Html::tag('span', $a, [
                                'title' => Html::tag('strong', Yii::t('mod_langs', 'EDIT_ITEM')).'<br>'.$model->title,
                                'data-toggle' => 'tooltip'
                    ]);
                    //return Html::label($model->title, 'cb' . $model->id, ['class' => 'label-code']);
                }
            ],
            'title_native',
            'code',
            'sef',
            'image',
            //'description',
            //'metakey:ntext',
            //'metadesc:ntext',
            //'sitename',
            //'published',
            //'ordering',
            'id',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>


</div>
