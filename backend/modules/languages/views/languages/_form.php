<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\languages\models\Languages */
/* @var $form yii\bootstrap\ActiveForm */

$this->params['active'] = 'languages';

$css = 'h3.languages-title{font-weight:600;font-size:22px;}.nav-tabs{margin-bottom:18px;}';
$this->registerCss($css);

$statusList = [
    '1' => Yii::t('common', 'PUBLISHED'),
    '0'=> Yii::t('common', 'UNPUBLISHED'),
    '-1' => Yii::t('common', 'TRASHED')
];
?>

<div class="languages-form">
    <?php
    $form = ActiveForm::begin([
        'id' => 'language_form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
                'wrapper' => 'col-sm-4',
                'hint' => 'col-sm-4'
            ]
        ]
    ]);
    ?>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active">
            <a href="#details" aria-controls="details" role="tab" data-toggle="tab"><?= Yii::t('mod_langs', 'DETAILS') ?></a>
        </li>
        <li>
            <a href="#metadata" aria-controls="metadata" role="tab" data-toggle="tab"><?= Yii::t('mod_langs', 'METADATA') ?></a>
        </li>
        <li>
            <a href="#site_name" aria-controls="site_name" role="tab" data-toggle="tab"><?= Yii::t('mod_langs', 'SITE_NAME') ?></a>
        </li>
    </ul>

    <div class="tab-content">
        <div id="details" role="tabpanel" class="tab-pane active">
            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'title_native')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'sef')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'published')->dropDownList($statusList) ?>
            <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>
            <?= $form->field($model, 'id')->textInput(['disabled' => 'disabled']) ?>
        </div>
        <div id="metadata" role="tabpanel" class="tab-pane">
            <?= $form->field($model, 'metakey')->textarea(['maxlength' => true])->label(Yii::t('mod_langs', 'META_KEYWORDS')) ?>
            <?= $form->field($model, 'metadesc')->textarea(['maxlength' => true])->label(Yii::t('mod_langs', 'META_DESCRIPTION')) ?>
        </div>
        <div id="site_name" role="tabpanel" class="tab-pane">
            <?= $form->field($model, 'sitename')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
