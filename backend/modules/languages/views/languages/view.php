<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\languages\models\Languages */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_langs', 'CONTENT'), 'url' => ['/langs/languages']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'languages';
?>
<div class="languages-view">
    <p>
        <?= Html::a(Yii::t('common', 'UPDATE'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('common', 'DELETE'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('yii', 'DELETE_ITEM_CONFIRM'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'title',
            'title_native',
            'sef',
            'image',
            'description',
            'metakey:ntext',
            'metadesc:ntext',
            'sitename',
            'published',
            'ordering',
        ],
    ]) ?>

</div>
