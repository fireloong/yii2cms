<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $languageShow \backend\modules\languages\controllers\OverridesController */
/* @var $clientShow \backend\modules\languages\controllers\OverridesController */

$this->title = Yii::t('mod_langs', 'EDIT_OVERRIDE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_langs', 'Overrides'), 'url' => ['/langs/overrides']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['active'] = 'overrides';
?>
<div class="overrides-update">
    <div class="row">
        <div class="col-sm-6">
            <h3 class="page-header"><?= Yii::t('mod_langs', 'Create a New Override') ?></h3>
            <?php
            $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'horizontalCssClasses' => [
                                'wrapper' => 'col-sm-8'
                            ]
                        ]
            ]);

            $languageShowHtml = Html::textInput(null, $languageShow, ['class' => 'form-control', 'disabled' => true]);
            $clientShowHtml = Html::textInput(null, $clientShow, ['class' => 'form-control', 'disabled' => true]);
            print_r($clientShowHtml);exit;
            ?>

            <?=
            $form->field($model, 'language', [
                'template' => "{label}\n{beginWrapper}\n{input}$languageShowHtml\n{error}\n{endWrapper}\n{hint}"
            ])->hiddenInput()
            ?>

            <?=
            $form->field($model, 'client', [
                'template' => "{label}\n{beginWrapper}\n{input}$clientShowHtml\n{error}\n{endWrapper}\n{hint}"
            ])->hiddenInput()
            ?>

            <?= $form->field($model, 'category',[
                'template' => "{label}\n{beginWrapper}\n{input}$clientShowHtml\n{error}\n{endWrapper}\n{hint}"
            ])->hiddenInput() ?>

            <?= $form->field($model, 'source_language')->textarea() ?>

            <?= $form->field($model, 'translate')->textarea() ?>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <?= Html::submitButton(Yii::t('common', 'Save'), ['class' => 'btn btn-success']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-sm-6">
            <h3 class="page-header"><?= Yii::t('mod_langs', 'Search text you want to change') ?></h3>
        </div>
    </div>
</div>