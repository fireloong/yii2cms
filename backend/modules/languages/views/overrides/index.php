<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use backend\components\Icon;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $languageList array */
/* @var $clientList array */

$this->title = Yii::t('mod_langs', 'SUBMENU_OVERRIDES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'overrides';

$this->registerCss('#override-list{margin-top:10px;}');
$deleteTip = Yii::t('mod_langs', 'DELETE_SELECTED_ITEM_CONFIRM');
$deleteEmptyTip = Yii::t('mod_langs', 'SELECTION_FROM_LIST');
$js = <<<JS
$('select[id^="filter-"]').change(function(){
    $(this).closest('form').submit();
});
$('.btn-clear').click(function(){
    $('#filter-search').val('');
    $(this).closest('form').submit();
});
$('#overrides_delete').click(function(){
    var keys = $('#override-list').yiiGridView('getSelectedRows');
    if(keys.length){
        yii.confirm('$deleteTip', function(){
            $('#overrides_form').submit();
        });
    }else{
       alert('$deleteEmptyTip');
    }
});
JS;
$this->registerJs($js);
?>
<div class="overrides-index">
    <p>
        <?= Html::a(Yii::t('common', 'NEW'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::button(Yii::t('common', 'DELETE'), ['id' => 'overrides_delete', 'class' => 'btn btn-danger']) ?>
    </p>

    <?php
    $form = ActiveForm::begin([
        'id' => 'overrides-filter',
        'layout' => 'inline',
        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'sr-only'
            ]
        ]
    ]);
    ?>

    <?= $form->field($model, 'language')->dropDownList($languageList) ?>

    <?= $form->field($model, 'client')->dropDownList($clientList) ?>

    <?php
    $button = Html::submitButton(Icon::i('search'), ['class' => 'btn btn-default']);
    $addon = Html::tag('span', $button, ['class' => 'input-group-btn']);
    $searchInputTemplate = Html::tag('div', '{input}' . $addon, ['class' => 'input-group']);
    ?>

    <?=
    $form->field($model, 'search', [
        'inputOptions' => ['placeholder' => $model->getAttributeLabel('search')],
        'inputTemplate' => $searchInputTemplate,
    ])->textInput(['maxlength' => true])
    ?>

    <?= Html::button(Yii::t('mod_langs', 'CLEAR'), ['class' => 'btn btn-default btn-clear']) ?>

    <?=
    $form->field($model, 'limit', [
        'options' => [
            'class' => 'form-group pull-right'
        ]
    ])->dropDownList([
        '5' => '5', '15' => '15', '20' => '20', '30' => '30',
        '50' => '50', '100' => '100', '0' => Yii::t('common', 'ALL')])
    ?>

    <?php ActiveForm::end(); ?>

    <?=
    Html::beginForm(['delete'], 'post', ['id' => 'overrides_form']) .
    GridView::widget([
        'id' => 'override-list',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '1%'],
                'name' => 'ids',
                'checkboxOptions' => function ($model) {
                    return [
                        'id' => 'cb' . $model['id'],
                        'value' => $model['id']
                    ];
                }
            ],
            [
                'attribute' => 'key',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model['key'], ['update', 'id' => $model['id']]);
                }
            ],
            'value',
            'client',
            'language',
            'category'
        ]
    ]) . Html::endForm()
    ?>
</div>
