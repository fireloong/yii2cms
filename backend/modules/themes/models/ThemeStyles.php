<?php

namespace backend\modules\themes\models;

use Yii;

/**
 * This is the model class for table "{{%theme_styles}}".
 *
 * @property int $id
 * @property string $template
 * @property int $client_id
 * @property int $home
 * @property string $title
 * @property string $params
 */
class ThemeStyles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%theme_styles}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'home'], 'integer'],
            [['params'], 'string'],
            [['template'], 'string', 'max' => 50],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template' => Yii::t('mod_themes', 'TEMPLATE_NAME'),
            'home' => Yii::t('mod_themes', 'DEFAULT'),
            'title' => Yii::t('mod_themes', 'STYLE_NAME')
        ];
    }
}
