<?php


namespace backend\modules\themes\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\themes\models\ThemeStyles as ThemeStylesModel;

/**
 * description of ThemeStyles
 *
 * @author FireLoong
 */
class ThemeStyles extends ThemeStylesModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'home'], 'integer'],
            [['template', 'title', 'params'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ThemeStylesModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'home' => $this->home,
        ]);

        $query->andFilterWhere(['like', 'template', $this->template])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'params', $this->params]);

        return $dataProvider;
    }
}
