<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this \yii\web\View */
/* @var $dataProvider */

$this->title = Yii::t('mod_themes', 'SUBMENU_TEMPLATES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'templates';
?>
<div class="themes-templates-index">
    <?= GridView::widget([
        'id' => 'templates_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'label' => Yii::t('mod_themes', 'TEMPLATE_IMAGE'),
                'headerOptions' => ['width' => '215'],
                'format' => 'raw',
                'value' => function ($model) {
                    $client = \backend\models\Clients::findOne($model->client_id);
                    $themePath = Yii::getAlias('@root/') . trim($client->path, '/') . '/themes/';
                    $templateThumbnail = $themePath . $model->element . '/template_thumbnail.png';
                    if (is_file($templateThumbnail)) {
                        $assetUrl = Yii::$app->assetManager->getPublishedUrl($templateThumbnail);
                        if (!is_file(Yii::getAlias('@webroot' . $assetUrl))) {
                            Yii::$app->assetManager->publish($templateThumbnail);
                        }
                        return Html::img($assetUrl, ['class' => 'img-thumbnail', 'alt' => 'Template thumbnail']);
                    } else {
                        return '';
                    }
                }
            ],
            [
                'attribute' => 'element',
                'label' => Yii::t('mod_themes', 'TEMPLATE_NAME'),
                'format' => 'raw',
                'value' => function ($model) {
                    $text = Yii::t('mod_themes', 'EDIT_TEMPLATE', ucfirst($model->element));
                    return Html::a($text, ['edit', 'id' => $model->id]);
                }
            ],
            [
                'label' => Yii::t('mod_themes', 'TEMPLATE_VERSION'),
                'value' => function ($model) {
                    return $model->manifest_cache['version'];
                }
            ],
            [
                'label' => Yii::t('mod_themes', 'TEMPLATE_DATE'),
                'value' => function ($model) {
                    return $model->manifest_cache['creationDate'];
                }
            ],
            [
                'label' => Yii::t('mod_themes', 'TEMPLATE_AUTHOR'),
                'format' => 'raw',
                'value' => function ($model) {
                    $author = $model->manifest_cache['author'];
                    $authorEmail = $model->manifest_cache['authorEmail'];
                    $authorUrl = $model->manifest_cache['authorUrl'];
                    $html = '';
                    if (!empty($author)) {
                        $html .= Html::tag('div', $author);
                    }
                    if (!empty($authorEmail)) {
                        $html .= Html::tag('div', $authorEmail);
                    }
                    if (!empty($authorUrl)) {
                        $html .= Html::a($authorUrl, $authorUrl, ['target' => '_blank']);
                    }
                    return $html;
                }
            ]
        ]
    ]) ?>
</div>
