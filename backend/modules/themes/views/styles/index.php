<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this \yii\web\View */
/* @var $dataProvider */

$this->title = Yii::t('mod_themes', 'SUBMENU_STYLES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'styles';

$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip({trigger: \'hover\', html: true});');

?>
<div class="themes-style-index">
    <?=
    Html::beginForm('', 'post', ['id' => 'themes_form']) .
    GridView::widget([
        'id' => 'themes_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '3%'],
                'checkboxOptions' => function ($model) {
                    return ['id' => 'cb' . $model->id];
                }
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    $title = Yii::t('mod_themes', $model->title);
                    if ($model->title === $title) {
                        $title = ucfirst($model->template) . ' - Default';
                    }
                    return Html::a($title, ['edit', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'home',
                'format' => 'raw',
                'value' => function ($model) {
                    $icon = $model->home ? Html::icon('star', ['class' => 'icon-default']) : Html::icon('star-empty');
                    $html = Html::a($icon, null, [
                        'class' => 'btn btn-default btn-xs' . ($model->home ? ' disabled' : ''),
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                'id' => $model->id,
                                'client_id' => $model->client_id
                            ]
                        ]
                    ]);
                    return Html::tag('span', $html, [
                        'title' => Yii::t('mod_themes', $model->home ? 'DEFAULT' : 'SET_DEFAULT'),
                        'data-toggle' => 'tooltip'
                    ]);
                }
            ],
            [
                'attribute' => 'template',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(ucfirst($model->template), ['template/edit', 'id' => $model->id]);
                }
            ],
            'id'
        ]
    ]) . Html::endForm()
    ?>
</div>