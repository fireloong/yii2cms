<?php

namespace backend\modules\themes;

use Yii;
use common\components\Helper;

/**
 * themes module definition class
 */
class Module extends \yii\base\Module
{
    public $defaultRoute = 'styles';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->setViewPath('@app' . Yii::$app->view->theme->baseUrl . '/mod_' . $this->id);
        // custom initialization code goes here
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            Helper::addEntry(Yii::t('mod_themes', 'SUBMENU_STYLES'), ['/themes/styles'], 'styles');
            Helper::addEntry(Yii::t('mod_themes', 'SUBMENU_TEMPLATES'), ['/themes/templates'], 'templates');
            return true;
        }
        return false;
    }
}
