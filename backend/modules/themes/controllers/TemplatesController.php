<?php


namespace backend\modules\themes\controllers;

use Yii;
use yii\web\Controller;
use backend\models\searchs\Extensions;

/**
 * description of TemplatesController
 *
 * @author FireLoong
 */
class TemplatesController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new Extensions();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ['type' => 'theme']);
        return $this->render('index', ['dataProvider' => $dataProvider]);
    }
}
