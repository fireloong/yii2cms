<?php

use yii\bootstrap\Html;
use common\components\form\Form;

/* @var $this yii\web\View */
/* @var $model backend\modules\categories\models\Categories */
/* @var $languageList */
/* @var $categoryList */

$css = <<<CSS
.form-inline .form-group{margin-right: 10px;}
#content-alias{width: 220px;}
@media (min-width: 768px) {
  .form-inline .form-group{vertical-align: top;}
}
#content_articles_form .nav.nav-tabs{margin-bottom: 1.5rem;}
CSS;
$this->registerCss($css);
?>
<div class="row">
    <?php $form = Form::begin(['id' => 'content_articles_form']); ?>
    <div class="col-sm-12 form-inline">
        <?= $form->field($model, 'title', 'text') ?>
        <?= $form->field($model, 'alias', 'text', ['inputOptions' => [
            'placeholder' => Yii::t('mod_content', 'ALIAS_PLACEHOLDER'),
            'size' => 45
        ]]) ?>
    </div>
    <div class="col-sm-12">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active" role="presentation">
                <?= Html::a(Yii::t('mod_content', 'ARTICLE_CONTENT'), '#general', [
                    'aria-controls' => 'general',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
            <li role="presentation">
                <?= Html::a(Yii::t('mod_content', 'URLS_AND_IMAGES'), '#images', [
                    'aria-controls' => 'images',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
            <!--            <li role="presentation">-->
            <!--                --><? //= Html::a(Yii::t('mod_content', 'SHOW_OPTIONS'), '#attrib-basic', [
            //                    'aria-controls' => 'attrib-basic',
            //                    'role' => 'tab',
            //                    'data-toggle' => 'tab'
            //                ]) ?>
            <!---->
            <!--            </li>-->
            <li role="presentation">
                <?= Html::a(Yii::t('mod_content', 'PUBLISHING_OPTIONS'), '#publishing', [
                    'aria-controls' => 'publishing',
                    'role' => 'tab',
                    'data-toggle' => 'tab'
                ]) ?>

            </li>
            <!--            <li role="presentation">-->
            <!--                --><? //= Html::a(Yii::t('mod_content', 'EDITOR_CONFIG'), '#editor', [
            //                    'aria-controls' => 'editor',
            //                    'role' => 'tab',
            //                    'data-toggle' => 'tab'
            //                ]) ?>
            <!---->
            <!--            </li>-->
        </ul>
        <div class="tab-content">
            <div id="general" class="tab-pane fade in active" role="tabpanel">
                <div class="row">
                    <div class="col-sm-9">
                        <?= $form->field($model, 'articletext', 'editor')->label(false) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'status', 'dropDownList', ['setupParams' => [[
                            '1' => Yii::t('common', 'PUBLISHED'),
                            '0' => Yii::t('common', 'UNPUBLISHED'),
                            '2' => Yii::t('common', 'TRASHED')
                        ]]]) ?>
                        <?= $form->field($model, 'catid', 'dropDownList', ['setupParams' => [$categoryList]]) ?>
                        <?= $form->field($model, 'featured', 'dropDownList', [
                            'setupParams' => [
                                [
                                    '1' => Yii::t('common', 'YES'),
                                    '0' => Yii::t('common', 'NO')
                                ]
                            ]]) ?>
                        <?= $form->field($model, 'language', 'dropDownList', ['setupParams' => [$languageList]]) ?>
                        <?= $form->field($model, 'note', 'text') ?>
                    </div>
                </div>
            </div>
            <div id="images" class="tab-pane fade in" role="tabpanel">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'images[image_intro]', 'media', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_IMAGE_INTRO_LABEL')) ?>
                        <?= $form->field($model, 'images[align_intro]', 'dropDownList', [
                            'layout' => 'horizontal',
                            'setupParams' => [[
                                '' => Yii::t('mod_content', 'USER_DEFAULT_SETTING_VALUE', Yii::t('mod_content', 'ALIGN_LEFT')),
                                'right' => Yii::t('mod_content', 'ALIGN_RIGHT'),
                                'left' => Yii::t('mod_content', 'ALIGN_LEFT'),
                                'center' => Yii::t('mod_content', 'ALIGN_CENTER')
                            ]]])->label(Yii::t('mod_content', 'FIELD_IMAGES_ALIGN_INTRO_LABEL')) ?>
                        <?= $form->field($model, 'images[alt_intro]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_ALT_INTRO_LABEL')) ?>
                        <?= $form->field($model, 'images[caption_intro]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_CAPTION_INTRO_LABEL')) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'images[image_full]', 'media', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_IMAGE_FULL_LABEL')) ?>
                        <?= $form->field($model, 'images[align_full]', 'dropDownList', [
                            'layout' => 'horizontal',
                            'setupParams' => [[
                                '' => Yii::t('mod_content', 'USER_DEFAULT_SETTING_VALUE', Yii::t('mod_content', 'ALIGN_LEFT')),
                                'right' => Yii::t('mod_content', 'ALIGN_RIGHT'),
                                'left' => Yii::t('mod_content', 'ALIGN_LEFT'),
                                'center' => Yii::t('mod_content', 'ALIGN_CENTER')
                            ]]])->label(Yii::t('mod_content', 'FIELD_IMAGES_ALIGN_FULL_LABEL')) ?>
                        <?= $form->field($model, 'images[alt_full]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_ALT_FULL_LABEL')) ?>
                        <?= $form->field($model, 'images[caption_full]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_IMAGES_CAPTION_FULL_LABEL')) ?>
                    </div>
                </div>
            </div>
            <!--            <div id="attrib-basic" class="tab-pane fade in" role="tabpanel"></div>-->
            <div id="publishing" class="tab-pane fade in" role="tabpanel">
                <div class="row form-horizontal">
                    <div class="col-sm-6">
                        <?= $form->field($model, 'publish_up', 'datetime', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'publish_down', 'datetime', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'created_at', 'datetime', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'created_by', 'number', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'created_by_alias', 'text', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'modified_at', 'datetime', [
                            'layout' => 'horizontal',
                            'inputOptions' => ['disabled' => true]
                        ]) ?>
                        <?= $form->field($model, 'modified_by', 'number', [
                            'layout' => 'horizontal',
                            'inputOptions' => ['disabled' => true]
                        ]) ?>
                        <?= $form->field($model, 'hits', 'number', [
                            'layout' => 'horizontal',
                            'inputOptions' => ['disabled' => true]
                        ]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'metadesc', 'textarea', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'metakey', 'textarea', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'xreference', 'text', ['layout' => 'horizontal']) ?>
                        <?= $form->field($model, 'metadata[robots]', 'dropDownList', [
                            'layout' => 'horizontal',
                            'setupParams' => [[
                                '' => Yii::t('mod_content', 'USER_DEFAULT_SETTING'),
                                'index, follow' => Yii::t('mod_content', 'INDEX_AND_FOLLOW'),
                                'noindex, follow' => Yii::t('mod_content', 'NOINDEX_AND_FOLLOW'),
                                'index, nofollow' => Yii::t('mod_content', 'INDEX_AND_NOFOLLOW'),
                                'noindex, nofollow' => Yii::t('mod_content', 'NOINDEX_AND_NOFOLLOW')
                            ]]
                        ])->label(Yii::t('mod_content', 'FIELD_ROBOTS_LABEL')) ?>
                        <?= $form->field($model, 'metadata[author]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_AUTHOR_LABEL')) ?>
                        <?= $form->field($model, 'metadata[rights]', 'textarea', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_RIGHTS_LABEL')) ?>
                        <?= $form->field($model, 'metadata[xreference]', 'text', ['layout' => 'horizontal'])
                            ->label(Yii::t('mod_content', 'FIELD_METADATA_XREFERENCE_LABEL')) ?>
                    </div>
                </div>
            </div>
            <!--            <div id="editor" class="tab-pane fade in" role="tabpanel"></div>-->
        </div>
    </div>
    <div class="col-sm-12">
        <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-primary']) ?>
    </div>
    <?php Form::end(); ?>
</div>

