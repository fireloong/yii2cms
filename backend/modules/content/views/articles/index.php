<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\content\models\searchs\Content */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('mod_content', 'ARTICLES_LIST');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_content', 'ARTICLES'), 'url' => ['/content/articles']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'articles';

$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip();');
?>
<div class="content-articles-index">
    <p class="btn-tools">
        <?= Html::a(Yii::t('common', 'NEW'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => ['class' => 'table table-striped table-hover'],
        'layout' => '{items}{summary}{pager}',
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'headerOptions' => ['width' => '65%'],
                'value' => function ($model) {
                    $alias = ' (' . Yii::t('mod_content', 'FIELD_ALIAS_LABEL') . ': ' . $model->alias . ')';
                    $title = Html::tag('div', $model->title . Html::tag('small', $alias, ['class' => 'text-muted']));
                    $categoryLabel = Yii::t('mod_content', 'FIELD_CATID_LABEL') . ': ';
                    $categoryLink = Html::a($model->categories['title'], [
                        '/categories/default/update',
                        'id' => $model->catid
                    ], [
                        'data-toggle' => 'tooltip',
                        'title' => Yii::t('mod_content','EDIT_CATEGORY')
                    ]);
                    $categoryTitle = Html::tag('div', $categoryLabel . $categoryLink, ['class' => 'small']);
                    return $title . $categoryTitle;
                }
            ],
            'created_at:date',
            [
                'attribute' => 'language',
                'value' => function ($model) {
                    return $model->language === '*' ? Yii::t('mod_langs', 'ALL_LANGUAGES') : $model->language;
                }
            ],
            'hits',
            'id',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ]
        ]
    ]) ?>
</div>
