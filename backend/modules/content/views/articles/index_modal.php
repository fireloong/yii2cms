<?php

use yii\grid\GridView;
use yii\bootstrap\Html;
use backend\themes\basic\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

AppAsset::register($this);

$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', 'BACKEND');

$this->registerCss('.tooltip .tooltip-inner,td .tooltip .tooltip-inner{text-align:left;max-width:none;white-space:nowrap;}');
$js = <<<JS
$('.select-link').on('click', function() {
    window.parent.articleModalClose($(this).data());
});
$('[data-toggle="tooltip"]').tooltip({html:true});
JS;

$this->registerJs($js);

$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<?= Html::csrfMetaTags() ?>
<title><?= Html::encode(Yii::t('mod_content', 'SELECT_ARTICLE')) ?> - <?= $appName ?> - <?= $client ?></title>
<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <?= GridView::widget([
                'tableOptions' => ['class' => 'table table-striped table-hover'],
                'layout' => '{items}{summary}{pager}',
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $enableHtml = Html::icon('ok',['class'=>'text-success small']);
                            $disableHtml = Html::icon('remove',['class'=>'text-danger small']);
                            return $model->status === 1 ? $enableHtml : $disableHtml;
                        }
                    ],
                    [
                        'attribute' => 'title',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $titleHtml = Html::a($model->title, 'javascript:void(0);', [
                                'class' => 'select-link',
                                'title' => $model->title . '<br>' . Yii::t('mod_content', 'FIELD_ALIAS_LABEL') . ': '
                                    . $model->alias,
                                'data' => [
                                    'toggle' => 'tooltip',
                                    'id' => $model->id,
                                    'title' => $model->title
                                ]
                            ]);
                            $catHtml = Yii::t('mod_content', 'FIELD_CATID_LABEL') . ': '
                                . Html::tag('span', $model->categories['title'], ['class' => 'text-muted']);
                            return Html::tag('div', $titleHtml) . Html::tag('div', $catHtml, ['class' => 'small']);
                        }
                    ],
                    [
                        'attribute' => 'language',
                        'value' => function ($model) {
                            return $model->language === '*' ? Yii::t('mod_langs', 'ALL_LANGUAGES') : $model->language;
                        }
                    ],
                    'created_at:date',
                    'id'
                ]
            ]) ?>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>