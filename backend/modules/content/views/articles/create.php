<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\content\models\Content */
/* @var $languageList */
/* @var $categoryList */

$this->title = Yii::t('mod_content', 'CREATE_ARTICLE');
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_content', 'ARTICLES'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['submenusShow'] = false;
?>
<div class="content-articles-create">
    <?= $this->render('_form', [
        'model' => $model,
        'languageList' => $languageList,
        'categoryList' => $categoryList
    ]) ?>
</div>
