<?php

namespace backend\modules\content\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\content\models\Content as ContentModel;

/**
 * Content represents the model behind the search form of `backend\modules\content\models\Content`.
 */
class Content extends ContentModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'id', 'status', 'catid', 'created_at', 'created_by', 'modified_at', 'modified_by',
                    'publish_up', 'publish_down', 'ordering', 'hits', 'featured'
                ],
                'integer'
            ],
            [
                [
                    'title', 'alias', 'introtext', 'fulltext', 'created_by_alias', 'images', 'urls',
                    'attribs', 'metakey', 'metadesc', 'metadata', 'language', 'xreference', 'note'
                ],
                'safe'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ContentModel::find()->joinwith('categories');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'catid' => $this->catid,
            'featured' => $this->featured,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
