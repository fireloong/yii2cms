<?php

namespace backend\modules\content\models;

use Yii;
use yii\helpers\Inflector;
use backend\modules\categories\models\Categories;

/**
 * This is the model class for table "{{%content}}".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $introtext
 * @property string $fulltext
 * @property int $status
 * @property int $catid
 * @property int $created_at
 * @property int $created_by
 * @property string $created_by_alias
 * @property int $modified_at
 * @property int $modified_by
 * @property int $publish_up
 * @property int $publish_down
 * @property string $images
 * @property string $urls
 * @property string $attribs
 * @property int $ordering
 * @property string $metakey
 * @property string $metadesc
 * @property int $hits
 * @property string $metadata
 * @property int $featured
 * @property string $language
 * @property string $xreference
 * @property string $note
 */
class Content extends \yii\db\ActiveRecord
{
    public $articletext;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'articletext', 'language'], 'required'],
            [['introtext', 'fulltext', 'metakey', 'metadesc'], 'string'],
            [['status', 'catid', 'created_at', 'created_by', 'modified_at',
                'modified_by', 'publish_up', 'publish_down', 'ordering', 'hits', 'featured'], 'integer'],
            [['title', 'created_by_alias', 'note'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 400],
            [['attribs', 'images'], 'safe'],
            [['language'], 'string', 'max' => 7],
            [['xreference'], 'string', 'max' => 100],
            [['introtext', 'fulltext', 'images', 'urls', 'attribs', 'metadata'], 'default', 'value' => ''],
            [['attribs', 'images', 'metadata'], 'filter', 'filter' => function ($value) {
                return is_array($value) ? json_encode($value) : $value;
            }],
            [['alias'], 'filter', 'filter' => function ($value) {
                return empty($value) ? Inflector::slug($this->title) : $value;
            }],
            [['created_at', 'publish_up'], 'filter', 'filter' => function ($value) {
                return $value ?: ($this->getIsNewRecord() ? time() : $value);
            }],
            [['created_by'], 'filter', 'filter' => function ($value) {
                return $value ?: ($this->getIsNewRecord() ? Yii::$app->user->id : $value);
            }],
            [['modified_at'], 'filter', 'filter' => function () {
                return time();
            }],
            [['modified_by'], 'filter', 'filter' => function () {
                return Yii::$app->user->id;
            }],
            [['fulltext'], 'filter', 'filter' => function () {
                return $this->articletext;
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mod_content', 'ID'),
            'title' => Yii::t('mod_content', 'FIELD_TITLE_LABEL'),
            'alias' => Yii::t('mod_content', 'FIELD_ALIAS_LABEL'),
//            'introtext' => Yii::t('mod_content', 'Introtext'),
//            'fulltext' => Yii::t('mod_content', 'Fulltext'),
            'articletext' => Yii::t('mod_content', 'FIELD_ARTICLE_TEXT_LABEL'),
            'status' => Yii::t('mod_content', 'FIELD_STATUS_LABEL'),
            'catid' => Yii::t('mod_content', 'FIELD_CATID_LABEL'),
            'created_at' => Yii::t('mod_content', 'FIELD_CREATED_AT_LABEL'),
            'created_by' => Yii::t('mod_content', 'FIELD_CREATED_BY_LABEL'),
            'created_by_alias' => Yii::t('mod_content', 'FIELD_CREATED_BY_ALIAS_LABEL'),
            'modified_at' => Yii::t('mod_content', 'FIELD_MODIFIED_AT_LABEL'),
            'modified_by' => Yii::t('mod_content', 'FIELD_MODIFIED_BY_LABEL'),
            'publish_up' => Yii::t('mod_content', 'FIELD_PUBLISH_UP_LABEL'),
            'publish_down' => Yii::t('mod_content', 'FIELD_PUBLISH_DOWN_LABEL'),
//            'images' => Yii::t('mod_content', 'Images'),
//            'urls' => Yii::t('mod_content', 'Urls'),
//            'attribs' => Yii::t('mod_content', 'Attribs'),
//            'ordering' => Yii::t('mod_content', 'Ordering'),
            'metakey' => Yii::t('mod_content', 'FIELD_METAKEY_LABEL'),
            'metadesc' => Yii::t('mod_content', 'FIELD_METADESC_LABEL'),
            'hits' => Yii::t('mod_content', 'FIELD_HITS_LABEL'),
            'featured' => Yii::t('mod_content', 'FIELD_FEATURED_LABEL'),
            'language' => Yii::t('mod_content', 'FIELD_LANGUAGE_LABEL'),
            'xreference' => Yii::t('mod_content', 'FIELD_XREFERENCE_LABEL'),
            'note' => Yii::t('mod_content', 'FIELD_NOTE_LABEL'),
            'categories.title' => Yii::t('mod_content', 'FIELD_CATID_LABEL')
        ];
    }

    public function getCategories()
    {
        return $this->hasOne(Categories::class, ['id' => 'catid']);
    }
}
