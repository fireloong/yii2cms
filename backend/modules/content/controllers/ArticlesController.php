<?php

namespace backend\modules\content\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use backend\modules\content\models\Content;
use backend\modules\languages\models\Languages;
use backend\modules\categories\models\Categories;
use backend\modules\content\models\searchs\Content as ContentSearch;

/**
 * Default controller for the `content` module
 */
class ArticlesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ContentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $layout = Yii::$app->request->get('layout');

        if ($layout === 'modal'){
            return $this->renderPartial('index_modal',['dataProvider' => $dataProvider]);
        }else{
            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Creates a new Content model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Content();
        if (Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $session->setFlash('success', Yii::t('common', 'SAVE_SUCCESS'));
                return $this->redirect(['index']);
            } else {
                $session->setFlash('error', Yii::t('common', 'SAVE_FAILED'));
                return $this->refresh();
            }
        }

        $model->featured = 0;

        $languageList = Languages::getList();
        $categoryList = Categories::getList('mod_content');

        return $this->render('create', [
            'model' => $model,
            'languageList' => $languageList,
            'categoryList' => $categoryList
        ]);
    }

    /**
     * Updates an existing Content model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $session = Yii::$app->session;
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $session->setFlash('success', Yii::t('common', 'SAVE_SUCCESS'));
                return $this->redirect(['index']);
            } else {
                $session->setFlash('error', Yii::t('common', 'SAVE_FAILED'));
                return $this->refresh();
            }
        }

        $model->articletext = $model->fulltext;

        $languageList = Languages::getList();
        $categoryList = Categories::getList('mod_content');

        return $this->render('update', [
            'model' => $model,
            'languageList' => $languageList,
            'categoryList' => $categoryList
        ]);
    }

    /**
     * Deletes an existing Content model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Content model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Content the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('mod_content', 'The requested page does not exist.'));
    }
}
