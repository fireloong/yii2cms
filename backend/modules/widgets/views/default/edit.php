<?php

use yii\bootstrap\Html;
use backend\models\Clients;
use common\components\form\Form;

/* @var $this yii\web\View */
/* @var $model backend\modules\widgets\models\Widgets|null */
/* @var $extension backend\models\Extensions|null */
/* @var $positions */
/* @var $configs */

$langCat = $extension->manifest_cache['langCat'] ?? null;
$desc = $extension->manifest_cache['description'];
$title = $langCat ? Yii::t($langCat, $model->title) : $model->title;
$this->title = Yii::t('mod_widgets', 'WIDGETS') . ': ' . $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('mod_widgets', 'WIDGETS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $title;

$client = Clients::findOne($model->client_id);

$css = <<<CSS
.widgets-default-edit .nav.nav-tabs{margin-bottom: 1.5rem;}
.form-inline{margin-bottom: 1rem;}
.info-label{margin-top: -5px;margin-bottom: 10px;cursor: default;}
.tab-content .tab-pane{min-height: 300px;}
CSS;
$this->registerCss($css);
$this->registerJs('$(\'[data-toggle="tooltip"]\').tooltip();')
?>
<div class="widgets-default-edit">
    <?php $form = Form::begin(['id' => 'widget_form']); ?>
    <div class="form-inline">
        <?= $form->field($model, 'title', 'text', [
            'layout' => 'inline',
            'inputOptions' => [
                'value' => $title
            ]
        ]) ?>
    </div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <?= Html::a(Yii::t('mod_widgets', 'GENERAL'), '#general', [
                'aria-controls' => 'general',
                'data-toggle' => 'tab',
                'role' => 'tab'
            ]) ?>

        </li>
        <?php
        if ($configs && $configs->fields) {
            foreach ($configs->fields->fieldset as $fieldset) {
                $name = $fieldset->attributes()->name;
                $label = $fieldset->attributes()->label ?? $name;
                if ('basic' !== (string)$name) {
                    $aText = is_null($langCat) ? (string)$label : Yii::t($langCat, strtoupper((string)$label));
                    $aLink = Html::a($aText, '#' . (string)$name, [
                        'aria-controls' => $name,
                        'data-toggle' => 'tab',
                        'role' => 'tab',
                        'lang' => $langCat
                    ]);
                    echo Html::tag('li', $aLink, ['role' => 'presentation']);
                }
            }
        }
        ?>

    </ul>
    <div class="tab-content">
        <div id="general" class="tab-pane fade in active" role="tabpanel">
            <div class="row">
                <div class="col-sm-9 form-horizontal">
                    <h3><?= $title ?></h3>
                    <div class="info-label">
                        <?= Html::tag('span', $client->name, [
                            'class' => 'label label-default',
                            'title' => Yii::t('mod_widgets', 'CLIENT_NAME'),
                            'data-toggle' => 'tooltip'
                        ]) ?>

                    </div>
                    <p><?= $langCat ? Yii::t($langCat, $desc) : $desc ?></p>
                    <?php
                    if ($configs && $configs->fields) {
                        echo '<hr>' . PHP_EOL;
                        foreach ($configs->fields->fieldset as $fieldset) {
                            if ((string)$fieldset->attributes()->name === 'basic') {
                                echo $form->genXmlFields($fieldset->field, $model, $langCat);
                            }
                        }
                    }
                    ?>
                </div>
                <div class="col-sm-3 form-default">
                    <?= $form->field($model, 'showtitle', 'dropDownList', ['setupParams' => [[
                        '1' => Yii::t('common', 'SHOW'),
                        '0' => Yii::t('common', 'HIDE')
                    ]]]) ?>
                    <?= $form->field($model, 'position', 'dropDownList', ['setupParams' => [$positions]]) ?>
                    <?= $form->field($model, 'published', 'dropDownList', ['setupParams' => [[
                        '1' => Yii::t('common', 'PUBLISHED'),
                        '0' => Yii::t('common', 'UNPUBLISHED'),
                        '2' => Yii::t('common', 'TRASHED')
                    ]]]) ?>
                    <?= $form->field($model, 'publish_up', 'datetime', ['setupParams' => [['drops' => 'up']]]) ?>
                    <?= $form->field($model, 'publish_down', 'datetime', ['setupParams' => [['drops' => 'up']]]) ?>
                    <?= $form->field($model, 'ordering', 'number') ?>
                    <?= $form->field($model, 'note', 'text') ?>
                </div>
            </div>
        </div>
        <?php
        if ($configs && $configs->fields) {
            foreach ($configs->fields->fieldset as $fieldset) {
                $fieldsetName = (string)$fieldset->attributes()->name;
                if ($fieldsetName !== 'basic') {
                    $content = $form->genXmlFields($fieldset->field, $model, $langCat);
                    $html = Html::tag('div', Html::tag('div', $content, ['class' => 'col-sm-9']), [
                        'class' => 'row form-horizontal'
                    ]);
                    echo Html::tag('div', $html, [
                        'id' => $fieldsetName,
                        'class' => 'tab-pane fade',
                        'role' => 'tabpanel'
                    ]);
                }
            }
        }
        ?>
    </div>
    <?= Html::submitButton(Yii::t('common', 'SAVE'), ['class' => 'btn btn-primary']) ?>
    <?php Form::end(); ?>
</div>
