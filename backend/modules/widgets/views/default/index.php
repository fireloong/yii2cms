<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this \yii\web\View */
/* @var $dataProvider */

$this->title = Yii::t('mod_widgets', 'WIDGETS');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="widgets-default-index">
    <?=
    Html::beginForm('', 'post', ['id' => 'widgets_form']) .
    GridView::widget([
        'id' => 'widgets_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '3%'],
                'checkboxOptions' => function ($model) {
                    return ['id' => 'cb' . $model->id];
                }
            ],
            [
                'attribute' => 'published'
            ],
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model) {
                    $langCat = $model->extensions->manifest_cache['langCat'] ?? null;
                    $title = $langCat ? Yii::t($langCat, $model->title) : $model->title;
                    return Html::a($title, ['edit', 'id' => $model->id]);
                }
            ],
            'position',
            [
                'attribute' => 'language'
            ],
            'id'
        ]
    ]) . Html::endForm()
    ?>
</div>
