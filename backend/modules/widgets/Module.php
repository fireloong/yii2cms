<?php

namespace backend\modules\widgets;

use Yii;

/**
 * widgets module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->setViewPath('@app' . Yii::$app->view->theme->baseUrl . '/mod_' . $this->id);
        // custom initialization code goes here
    }
}
