<?php


namespace backend\modules\widgets\models\searchs;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\widgets\models\Widgets as WidgetsModel;

/**
 * description of Widgets
 *
 * @author FireLoong
 */
class Widgets extends WidgetsModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ordering', 'publish_up', 'publish_down', 'published', 'showtitle', 'client_id'], 'integer'],
            [['title', 'note', 'content', 'position', 'widget', 'params', 'language'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WidgetsModel::find()->alias('w')->joinWith('extensions e')->where(['e.type' => 'widget']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'a.id' => $this->id,
//            'a.ordering' => $this->ordering,
//            'a.publish_up' => $this->publish_up,
//            'a.publish_down' => $this->publish_down,
//            'a.published' => $this->published,
//            'a.showtitle' => $this->showtitle,
//            'a.client_id' => $this->client_id,
//        ]);

//        $query->andFilterWhere(['like', 'title', $this->title])
//            ->andFilterWhere(['like', 'note', $this->note])
//            ->andFilterWhere(['like', 'content', $this->content])
//            ->andFilterWhere(['like', 'position', $this->position])
//            ->andFilterWhere(['like', 'widget', $this->widget])
//            ->andFilterWhere(['like', 'params', $this->params])
//            ->andFilterWhere(['like', 'language', $this->language]);

        return $dataProvider;
    }
}
