<?php

namespace backend\modules\widgets\models;

use Yii;
use backend\models\Extensions;

/**
 * This is the model class for table "{{%widgets}}".
 *
 * @property int $id
 * @property string $title
 * @property string $note
 * @property string $content
 * @property int $ordering
 * @property string $position
 * @property int $publish_up
 * @property int $publish_down
 * @property int $published
 * @property string|null $widget
 * @property int $showtitle
 * @property string $params
 * @property int $client_id
 * @property string $language
 */
class Widgets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%widgets}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'string'],
            [['ordering', 'published', 'showtitle', 'client_id'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['note'], 'string', 'max' => 255],
            [['position', 'widget'], 'string', 'max' => 50],
            [['language'], 'string', 'max' => 7],
            [['publish_up', 'publish_down'], 'integer', 'message' => Yii::t('yii', 'ATTRIBUTE_FORMAT_INVALID')],
            [['params'], 'safe'],
            [['params'], 'filter', 'filter' => function ($value) {
                return is_array($value) ? json_encode($value) : $value;
            }]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mod_widgets', 'ID'),
            'title' => Yii::t('mod_widgets', 'TITLE'),
            'note' => Yii::t('mod_widgets', 'NOTE'),
            'content' => Yii::t('mod_widgets', 'CONTENT'),
            'ordering' => Yii::t('common', 'ORDER'),
            'position' => Yii::t('mod_widgets', 'POSITION'),
            'publish_up' => Yii::t('mod_widgets', 'PUBLISH_UP'),
            'publish_down' => Yii::t('mod_widgets', 'PUBLISH_DOWN'),
            'published' => Yii::t('common', 'STATUS'),
            'widget' => Yii::t('mod_widgets', 'WIDGET'),
            'showtitle' => Yii::t('mod_widgets', 'SHOW_TITLE'),
            'params' => Yii::t('mod_widgets', 'PARAMS'),
            'client_id' => Yii::t('mod_widgets', 'CLIENT_ID'),
            'language' => Yii::t('mod_widgets', 'LANGUAGE'),
        ];
    }

    public function getExtensions()
    {
        return $this->hasOne(Extensions::class, ['element' => 'widget']);
    }
}
