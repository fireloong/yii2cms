<?php


namespace backend\widgets\latest_actions;

use backend\modules\actionlogs\models\ActionLogs;

/**
 * description of Widget
 *
 * @author FireLoong
 */
class Widget extends \common\components\yii\Widget
{
    public $widget = 'latest_actions';
    public $count;
    public $bootstrap_size;

    public function run()
    {
        $logs = ActionLogs::find()
            ->orderBy('log_date DESC')
            ->limit($this->count)
            ->all();
        return $this->render('run', [
            'logs' => $logs,
            'bootstrap_size' => $this->bootstrap_size
        ]);
    }
}
