<?php

return [
    'admin' => [
        'class' => 'backend\modules\admin\Module',
    ],
    'plugins' => [
        'class' => 'backend\modules\plugins\Module',
    ],
    'media' => [
        'class' => 'backend\modules\media\Module',
    ],
    'languages' => [
        'class' => 'backend\modules\languages\Module',
    ],
    'actionlogs' => [
        'class' => 'backend\modules\actionlogs\Module',
    ],
    'themes' => [
        'class' => 'backend\modules\themes\Module',
    ],
    'widgets' => [
        'class' => 'backend\modules\widgets\Module',
    ],
    'categories' => [
        'class' => 'backend\modules\categories\Module',
    ],
    'content' => [
        'class' => 'backend\modules\content\Module',
    ],
];
