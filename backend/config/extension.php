<?php

$admin = [
    'components' => [
        'user' => [
            'identityClass' => 'backend\modules\admin\models\User',
            'loginUrl' => [
                'admin/user/login',
            ],
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend',
                'httpOnly' => true,
            ],
        ],
    ],
    'as access' => [
        'class' => 'backend\modules\admin\components\AccessControl',
        'allowActions' => [
            '/site/*',
            '/admin/user/request-password-reset',
            '/admin/user/signup',
            '/admin/user/profile',
            '/debug/*',
        ],
    ],
];

return array_merge($admin);
