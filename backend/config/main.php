<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require ROOT_PATH . '/common/config/params.php',
    require ROOT_PATH . '/common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$bundlesFile = APP_PATH.'/themes/'.$params['global']['theme'].'/assets/bundles.php';
$bundles = is_file($bundlesFile) ? require $bundlesFile : [];

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => require __DIR__ . '/modules.php',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            //'loginUrl' => ['admin/user/login'],
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/' . $params['global']['theme'],
                'baseUrl' => '@web/themes/' . $params['global']['theme'],
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['global']['theme']
                ]
            ]
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/themes/' . $params['global']['theme'] . '/messages',
                    'on missingTranslation' => [
                        'common\components\TranslationEventHandler',
                        'handleMissingTranslation'
                    ]
                ],
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii/messages',
                    'on missingTranslation' => [
                        'common\components\YiiTranslationEventHandler',
                        'handleMissingTranslation'
                    ]
                ]
            ]
        ],
        'urlManagerFrontend' => [
            'class' => yii\web\UrlManager::class,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'suffix' => '.html',
            'hostInfo' => $params['global']['hostInfo']['frontend']
        ],
        'assetManager' => [
            'bundles' => $bundles
        ],
        'formatter' => [
            'nullDisplay' => ''
        ]
    ],
    'params' => $params,
];
