<?php

return [
    'adminEmail' => 'admin@example.com',
    'mdm.admin.configs' => [
        'advanced' => [
            'frontend' => [
                '@common/config/main.php',
                '@common/config/main-local.php',
                '@common/config/extension.php',
                '@frontend/config/main.php',
                '@frontend/config/main-local.php',
                '@frontend/config/extension.php',
            ],
            'backend' => [
                '@common/config/main.php',
                '@common/config/main-local.php',
                '@common/config/extension.php',
                '@backend/config/main.php',
                '@backend/config/main-local.php',
                '@backend/config/extension.php',
            ],
        ],
    ],
    'global' => [
        'theme' => 'basic',
    ],
];
