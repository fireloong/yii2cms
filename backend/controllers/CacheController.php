<?php

namespace backend\controllers;

use Yii;
use yii\helpers\FileHelper;
use backend\models\Clients;

class CacheController extends \yii\web\Controller
{

    public function actionIndex()
    {
        $request = Yii::$app->request;
        if ($request->isPost) {
            $path = $request->post('path');
            FileHelper::removeDirectory($path);
            return $this->redirect(['index']);
        }
        $clients = Clients::getList();
        return $this->render('index', ['clients' => $clients]);
    }

}
