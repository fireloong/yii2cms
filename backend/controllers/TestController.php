<?php


namespace backend\controllers;

use Yii;

/**
 * description of TestController
 *
 * @author FireLoong
 */
class TestController extends \yii\web\Controller
{
    const EVENT_AFTER_HELLO = 'afterHello';

    public function init()
    {
        $plugin = new \common\components\Plugin();
        $this->on(self::EVENT_AFTER_HELLO, [$plugin, 'hello'], 'abc', true);
    }

    public function actionIndex()
    {
        $this->trigger(self::EVENT_AFTER_HELLO);
    }

    public function actionForm()
    {
        $model = new \backend\models\TestForm();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            print_r($model);
            exit;
        } else {
            return $this->render('form', ['model' => $model]);
        }
    }
}
