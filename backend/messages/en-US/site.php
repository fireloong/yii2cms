<?php

return [
    'LOGIN' => 'Login',
    'LOGOUT' => 'Logout',
    'USER_LOGIN_FORM_HEAD' => 'Please fill out the following fields to login:'
];
