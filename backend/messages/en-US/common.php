<?php

return [
    'EMAIL' => 'Email',
    'STATUS' => 'Status',
    'ACTIVE' => 'Active',
    'INACTIVE' => 'Inactive',
    'PAGE_NOT_FOUND_MSG' => 'The requested page does not exist.'
];