<?php

return [
    'LOGIN' => 'Login',
    'USERNAME' => 'Username',
    'PASSWORD' => 'Password',
    'EMAIL' => 'Email',
    'REMEMBER_ME' => 'Remember Me',
    'RESET_IT' => 'reset it',
    'SIGNUP' => 'signup',
    'SEND' => 'Send',
    'ASSIGN' => 'Assign',
    'REMOVE' => 'Remove',
    'CHANGE_PASSWORD' => 'Change Password',
    'RETYPE_PASSWORD' => 'Retype Password',
    'REQUEST_PASSWORD_RESET' => 'Request password reset',
    'USER_LOGIN_FORM_HEAD' => 'Please fill out the following fields to login:',
    'USER_RESET_PASSWORD_AND_SIGNUP' => 'If you forgot your password you can {reset}. For new user you can {signup}.',
    'USER_RESET_PASSWORD_FORM_HEAD' => 'Please fill out your email. A link to reset password will be sent there.',
    'USER_SIGNUP_FORM_HEAD' => 'Please fill out the following fields to signup:',
    'ASSIGNMENT_BREADCRUMBS' => 'Assignments',
    'ASSIGNMENT_VIEW_TITLE' => 'Assignment',
    'ASSIGNMENT_VIEW_SEARCH_AVAILABLE' => 'Search for available',
    'ASSIGNMENT_VIEW_SEARCH_ASSIGNED' => 'Search for assigned',
    'USER_INDEX_TITLE' => 'Users',
    'USER_INDEX_ACTIVATE' => 'Activate',
    'USER_INDEX_ACTIVATE_CONFIRM' => 'Are you sure you want to activate this user?'
];
