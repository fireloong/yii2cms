<?php

return [
    'PLUGINS' => '插件管理',
    'TYPE' => '类型',
    'ELEMENT' => '插件主文件',
    'PLUGINS_EDIT_TITLE' => '插件管理：{0}',
    'PLUGIN_TYPE' => '插件类型',
    'PLUGIN' => '基本选项',
    'PLUGIN_FILE' => '插件文件',
    'PLUGIN_TYPE_DESC' => '插件所在的分类或文件目录名称。',
    'PLUGIN_FILE_DESC' => '插件主文件名称',
    'DISABLE_PLUGIN' => '关闭插件',
    'ENABLE_PLUGIN' => '启用插件'
];
