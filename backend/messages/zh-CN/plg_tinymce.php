<?php

return [
    'EDITORS_TINYMCE' => '编辑器 - TinyMCE 编辑器',
    'XML_DESCRIPTION' => 'TinyMCE 是跨平台的基于 JavaScript 所见即所得 HTML 编辑器。',
    'FIELD_LABEL_ADVANCED_PARAMS' => '高级选项',
    'FIELD_NUMBER_OF_SETS_LABEL' => '编辑器面板数量',
    'FIELD_NUMBER_OF_SETS_DESC' => '编辑器面板的创建数量；最小是 3。',
    'FIELD_HEIGHT_LABEL' => 'HTML 源代码编辑窗口高度',
    'FIELD_HEIGHT_DESC' => 'HTML 源代码编辑窗口高度；此选项仅在高级模式和扩展模式下生效',
    'FIELD_WIDTH_LABEL' => 'HTML 源代码编辑窗口宽度',
    'FIELD_WIDTH_DESC' => 'HTML 源代码编辑窗口宽度；此选项仅在高级模式和扩展模式下生效'
];
