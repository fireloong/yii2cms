<?php

return [
    'ACTIONS_LOGS' => '操作日志',
    'ACTIONS_LOGS_XML_DESCRIPTION' => '显示会员在网站中的操作日志',
    'USER_ACTIONS_LOGS' => '会员操作日志',
    'SELECTION_FROM_LIST' => '请先从列表中选择。',
    'DELETE_ITEMS_CONFIRM_LOGS' => '确定要删除吗? 确定将永久删除选择的条目!',
    'DELETE_ITEMS_CONFIRM_ALL_LOGS' => '确定要删除全部会员操作日志吗?',
    'EXPORT_SELECTED_AS_CSV' => '导出已选日志 CSV 文件',
    'EXPORT_ALL_AS_CSV' => '导出全部日志 CSV 文件',
    'DELETE' => '删除',
    'PURGE' => '清空',
    'ACTION' => '操作',
    'MESSAGE' => '信息',
    'DATE' => '时间',
    'IP_ADDRESS' => 'IP 地址',
    'UID' => '会员 ID',
    'ITEM_ID' => '条目 ID'
];
