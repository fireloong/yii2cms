<?php

return [
    'WIDGETS' => '小部件管理',
    'TITLE' => '名称',
    'POSITION' => '位置',
    'LANGUAGE' => '语言',
    'GENERAL' => '基本选项',
    'CLIENT_NAME' => '应用端名称',
    'SHOW_TITLE' => '小部件名称',
    'CUSTOM_POSITION' => '已放置小部件的位置',
    'PUBLISH_UP' => '发布开始时间',
    'PUBLISH_DOWN' => '发布结束时间',
    'NOTE' => '说明'
];
