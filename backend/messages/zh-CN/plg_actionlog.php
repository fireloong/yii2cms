<?php

return [
    'USER_LOGGED_APP' => '会员 <a href="{accountlink}">{username}</a> 在 {app} 登录',
    'USER_TRIED_LOGIN_APP' => '会员 <a href="{accountlink}">{username}</a> 试图在 {app} 登录',
    'USER_LOGGED_OUT_APP' => '会员 <a href="{accountlink}">{username}</a> 从 {app} 退出',
    'USER_PURGED_ONE_OR_MORE_ROWS' => '会员 <a href="{accountlink}">{username}</a> 删除了一行或多行操作日志',
    'USER_EXPORTED_ONE_OR_MORE_ROWS' => '会员 <a href="{accountlink}">{username}</a> 导出了一行或多行操作日志',
    'ACTION_LOG_NAME' => '操作日志 - 核心扩展操作插件',
    'ACTION_LOG_XML_DESCRIPTION' => '记录会员在 CMS 核心扩展中的操作， 如果需要可以查看日志'
];
