<?php

return [
    'WIDGET_ALERT' => '信息提示框',
    'WIDGET_ALERT_XML_DESCRIPTION' => '用于显示各种不同类型的信息',
    'ADVANCED' => '高级选项'
];
