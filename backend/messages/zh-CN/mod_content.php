<?php

return [
    'MOD_CONTENT' => '内容管理',
    'CONTENT' => '内容管理',
    'CONTENT_XML_DESCRIPTION' => '内容管理模块',
    'SUBMENU_ARTICLES' => '文章',
    'SUBMENU_CATEGORIES' => '文章分类',
    'SUBMENU_FEATURED' => '精选文章',
    'ARTICLES' => '文章管理',
    'ARTICLES_LIST' => '文章列表',
    'CREATE_ARTICLE' => '创建新文章',
    'UPDATE_ARTICLE' => '更新文章',
    'ALIAS_PLACEHOLDER' => '根据标题自动生成',
    'FIELD_TITLE_LABEL' => '标题',
    'FIELD_ALIAS_LABEL' => '别名',
    'ARTICLE_CONTENT' => '文章内容',
    'URLS_AND_IMAGES' => '图片和链接',
    'SHOW_OPTIONS' => '显示设置',
    'PUBLISHING_OPTIONS' => '发布选项',
    'EDITOR_CONFIG' => '编辑界面',
    'FIELD_STATUS_LABEL' => '状态',
    'FIELD_ARTICLE_TEXT_LABEL' => '文章内容',
    'FIELD_NOTE_LABEL' => '注释',
    'FIELD_FEATURED_LABEL' => '精选',
    'FIELD_LANGUAGE_LABEL' => '语言',
    'FIELD_CATID_LABEL' => '分类',
    'FIELD_IMAGES_IMAGE_INTRO_LABEL' => '文章摘要图片',
    'FIELD_IMAGES_ALIGN_INTRO_LABEL' => '图片对齐方式',
    'USER_DEFAULT_SETTING_VALUE' => '使用默认设置 ({0})',
    'ALIGN_RIGHT' => '右对齐',
    'ALIGN_LEFT' => '左对齐',
    'ALIGN_CENTER' => '居中对齐',
    'FIELD_IMAGES_ALT_INTRO_LABEL' => '图片替代文字<br>(图片 Alt 属性)',
    'FIELD_IMAGES_CAPTION_INTRO_LABEL' => '图片说明<br>(图片 Caption 属性)',
    'FIELD_IMAGES_IMAGE_FULL_LABEL' => '正文图片',
    'FIELD_IMAGES_ALIGN_FULL_LABEL' => '图片对齐方式',
    'FIELD_IMAGES_ALT_FULL_LABEL' => '图片替代文字<br>(图片 Alt 属性)',
    'FIELD_IMAGES_CAPTION_FULL_LABEL' => '图片说明<br>(图片 Caption 属性)',
    'FIELD_PUBLISH_UP_LABEL' => '发布开始时间',
    'FIELD_PUBLISH_DOWN_LABEL' => '发布结束时间',
    'FIELD_CREATED_AT_LABEL' => '创建日期',
    'FIELD_CREATED_BY_LABEL' => '创建人',
    'FIELD_CREATED_BY_ALIAS_LABEL' => '创建人别名',
    'FIELD_MODIFIED_AT_LABEL' => '修改日期',
    'FIELD_MODIFIED_BY_LABEL' => '修改人',
    'FIELD_HITS_LABEL' => '点击次数',
    'FIELD_METADESC_LABEL' => '元描述<br>(Meta Description)',
    'FIELD_METAKEY_LABEL' => '元关键字<br>(Meta Keywords)',
    'FIELD_XREFERENCE_LABEL' => '参考文献',
    'FIELD_ROBOTS_LABEL' => '搜索引擎抓取规则',
    'USER_DEFAULT_SETTING' => '使用默认设置',
    'INDEX_AND_FOLLOW' => '索引,跟踪(Index, Follow)',
    'NOINDEX_AND_FOLLOW' => '不索引,跟踪(No index, follow)',
    'INDEX_AND_NOFOLLOW' => '索引,不跟踪(Index, No follow)',
    'NOINDEX_AND_NOFOLLOW' => '不索引,不跟踪(No index, no follow)',
    'FIELD_AUTHOR_LABEL' => '作者',
    'FIELD_RIGHTS_LABEL' => '版权信息',
    'FIELD_METADATA_XREFERENCE_LABEL' => '外部参考',
    'EDIT_CATEGORY' => '编辑分类',
    'SELECT_ARTICLE' => '选择文章'
];
