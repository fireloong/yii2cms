<?php

return [
    'WIDGET_LATEST_ACTIONS' => '操作日志 - 最新操作',
    'WIDGET_LATEST_ACTIONS_XML_DESCRIPTION' => '用于显示最新操作行为列表',
    'ADVANCED' => '高级选项',
    'FIELD_COUNT_LABEL' => '数量',
    'FIELD_COUNT_DESC' => '显示最新操作行为数量',
    'FIELD_BOOTSTRAP_SIZE_LABEL' => '模块显示宽度<br/>(Bootstrap 列数)',
    'FIELD_BOOTSTRAP_SIZE_DESC' => '此选项指定模块使用多少 bootstrap 列，这个列数决定了模块的宽度'
];
