<?php

return [
    'SUBMENU_STYLES' => '风格管理',
    'SUBMENU_TEMPLATES' => '模板管理',
    'TEMPLATE_NAME' => '模板名称',
    'STYLE_NAME' => '风格名称',
    'DEFAULT' => '默认',
    'SET_DEFAULT' => '设为默认',
    'DEFAULT_STYLE_SET_SUCCESS' => '默认风格设置成功！',
    'DEFAULT_STYLE_SET_FAILED' => '默认风格设置失败！',
    'TEMPLATE_IMAGE' => '图片',
    'EDIT_TEMPLATE' => '编辑 {0} 模板',
    'TEMPLATE_VERSION' => '版本',
    'TEMPLATE_DATE' => '日期',
    'TEMPLATE_AUTHOR' => '作者'
];
