<?php

return [
    'FRONTEND_MAIN_MENU_LABEL' => '主菜单',
    'FRONTEND_MAIN_MENU_DESC' => '前台主菜单',
    'BACKEND_MAIN_MENU_LABEL' => '主菜单',
    'BACKEND_MAIN_MENU_DESC' => '后台主菜单',
    'ROLE_PUBLIC' => '公共角色',
    'ROLE_PUBLIC_DESCRIPTION' => '最低权限的角色',
    'ROLE_SUPER_ADMINISTRATOR' => '超级管理员',
    'ROLE_SUPER_ADMINISTRATOR_DESCRIPTION' => '有全部权限的管理员',
    'ROLE_REGISTERED' => '注册会员',
    'ROLE_REGISTERED_DESCRIPTION' => '已经注册的用户',
];
