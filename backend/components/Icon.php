<?php


namespace backend\components;

use yii\helpers\Html;

/**
 * description of Icon
 *
 * @author FireLoong
 */
class Icon
{
    public static function i($name, $options = [])
    {
        $options['class'] = 'icon-' . $name . (isset($options['class']) ? ' ' . $options['class'] : '');
        return Html::tag('i', '', $options);
    }

    public static function span($name, $options = [])
    {
        $options['class'] = 'icon-' . $name . (isset($options['class']) ? ' ' . $options['class'] : '');
        return Html::tag('span', '', $options);
    }
}
