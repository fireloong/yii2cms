<?php


namespace backend\components\adapter;

use Yii;
use backend\models\Clients;
use backend\components\InstallerAdapter;

/**
 * description of ThemeAdapter
 *
 * @author FireLoong
 */
class ThemeAdapter extends InstallerAdapter
{
    protected $clientId;

    /**
     * {@inheritdoc}
     */
    protected function setupInstallPaths()
    {
        $clientName = (string)$this->manifest->attributes()->client;
        $client = Clients::findOne(['name' => $clientName]);
        if (is_null($client)) {
            $params = [
                'action' => Yii::t('installer', strtoupper($this->route)),
                'error' => Yii::t('installer', 'ERROR_UNKNOWN_CLIENT', $clientName)
            ];
            throw new \RuntimeException(Yii::t('installer', 'ABORT_ROLLBACK', $params));
        }
        $this->clientId = $client->id;
        $basePath = Yii::getAlias('@root' . $client->path);

        if (empty($this->element)) {
            $msg = Yii::t('installer', 'ABORT_THEME_NO_FILE', Yii::t('installer', strtoupper($this->route)));
            throw new \RuntimeException($msg);
        }

        $this->parent->setPath('extension_root', $basePath . '/themes/' . $this->element);
    }

    /**
     * {@inheritdoc}
     */
    protected function checkExistingExtension()
    {
        try {
            $this->currentExtensionId = $this->extension->findOne([
                'element' => $this->element,
                'type' => $this->type,
                'client_id' => $this->clientId
            ]);
        } catch (\yii\db\Exception $e) {
            $params = [
                'action' => Yii::t('installer', strtoupper($this->route)),
                'error' => $e->getMessage()
            ];
            throw new \RuntimeException(Yii::t('installer', 'ABORT_ROLLBACK', $params), $e->getCode(), $e);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function copyBaseFiles()
    {
        if ($this->parent->parseFiles($this->manifest->files) === false) {
            throw new \RuntimeException(Yii::t('installer', 'ABORT_THEME_INSTALL_FAIL_COPY_FILES'));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function storeExtension()
    {
        if ($this->currentExtensionId) {
            $this->extension = $this->currentExtensionId;
        } else {
            $this->extension->type = 'theme';
            $this->extension->element = $this->element;
            $this->extension->folder = '';
            $this->extension->enabled = 1;
            $this->extension->protected = 0;
            $this->extension->client_id = $this->clientId;
            $this->extension->params = $this->parent->getParams();
            $this->extension->status = 1;
            $this->extension->ordering = 0;
        }
        $this->extension->name = $this->name;
        $this->extension->manifest_cache = $this->parent->generateManifestCache();

        if (!$this->extension->save()) {
            $params = [
                'action' => Yii::t('installer', strtoupper($this->route)),
                'error' => $this->parent->errorsToString($this->extension->getErrors())
            ];
            throw new \RuntimeException(Yii::t('installer', 'ABORT_ROLLBACK', $params));
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function parseQueries()
    {
        if ($this->route === 'install') {
            $themeStyles = new \backend\modules\themes\models\ThemeStyles();
            $themeStyles->template = $this->element;
            $themeStyles->client_id = $this->clientId;
            $themeStyles->home = 0;
            $themeStyles->title = strtoupper($this->element).'_TITLE';
            $themeStyles->params = $this->extension->params;
            $themeStyles->save();
        }
    }
}
