<?php


namespace backend\themes\adminlte\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * description of AppAsset
 *
 * @author FireLoong
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/themes/adminlte/media';
    public $depends = [
        'yii\web\YiiAsset'
    ];
    public $css = [
        'plugins/fontawesome-free/css/all.min.css',
        'dist/css/adminlte.min.css',
        'plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
        'common.css',
        'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700'
    ];
    public $js = [
        'plugins/bootstrap/js/bootstrap.bundle.min.js',
        'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        'dist/js/adminlte.js'
    ];

    public function init()
    {
        parent::init();
        Yii::$app->assetManager->bundles['yii\bootstrap4\BootstrapAsset'] = [
            'sourcePath' => null,
            'css' => []
        ];
        Yii::$app->assetManager->bundles['yii\bootstrap4\BootstrapPluginAsset'] = [
            'sourcePath' => null,
            'js' => []
        ];
    }
}
