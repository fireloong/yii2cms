<?php

return [
    'SIGN_IN' => '登录',
    'USER_LOGIN_FORM_HEAD' => '登录以开始会话',
    'REGISTER_NEW_MEMBER' => '注册新会员',
    'FORGOT_PASSWORD' => '忘记密码'
];