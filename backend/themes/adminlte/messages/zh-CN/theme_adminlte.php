<?php

return [
    'THEME_ADMINLTE' => 'AdminLTE 后台管理主题',
    'THEME_XML_DESCRIPTION' => 'AdminLTE 是一个完全响应的管理模板。基于 Bootstrap 4 框架。高度可定制和易于使用。适合从小型移动设备到大型台式机的多种屏幕分辨率。',
    'POSITION_CONTENT-HEADER' => '内容头部位置',
    'POSITION_CPANEL' => '控制面板',
    'POSITION_FOOTER' => '页脚位置'
];