<?php

use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\bootstrap4\Html;
use backend\themes\adminlte\assets\AppAsset;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\actionlogs\models\searchs\ActionLogs */
/* @var $dataProvider yii\data\ActiveDataProvider */

$asset = AppAsset::register($this);
array_splice($asset->css, 1, 0, [
    'plugins/icheck-bootstrap/icheck-bootstrap.css'
]);

$this->title = Yii::t('mod_actionlogs', 'USER_ACTIONS_LOGS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'installed';

$this->registerCss('.card-body .table>thead>tr>td, .card-body .table>thead>tr>th{border-top-width: 0;}');
$selEmpty = Yii::t('mod_actionlogs', 'SELECTION_FROM_LIST');
$confirmMsg = Yii::t('mod_actionlogs', 'DELETE_ITEMS_CONFIRM_LOGS');
$js = <<<JS
$('#actionlogs_list input:checkbox').each(function() {
    if($(this).attr('name') === 'selection_all'){
        $(this).attr('id', 'cb_all');
    }
    const outerHTML = $(this).prop('outerHTML');
    const labelHTML =  '<label for="'+$(this).attr('id')+'"></label>';
    $(this).replaceWith('<div class="icheck-primary d-inline">' + outerHTML + labelHTML + '</div>');
});
$('#actionlogs_del').click(function(){
    var keys = $('#actionlogs_list').yiiGridView('getSelectedRows');
    if(keys.length){
        yii.confirm('$confirmMsg',function() {
          $('#actionlogs_form').submit();
        });
    }else{
        alert('$selEmpty');
    }
});
$('#actionlogs_sel').click(function(){
    var keys = $('#actionlogs_list').yiiGridView('getSelectedRows');
    if(keys.length){
        $('#actionlogs_form').append('<input type="hidden" name="exportSelectedLogs" value="1">').submit();
    }else{
        alert('$selEmpty');
    }
});
JS;

$toolbarBtnText = Yii::t('common', 'TOOLBAR') . ' ' . Html::tag('i', '', ['class' => 'fas fa-wrench']);
$delBtnText = Html::tag('i', '', ['class' => 'fas fa-times']) . ' ' . Yii::t('mod_actionlogs', 'DELETE');
$purBtnText = Html::tag('i', '', ['class' => 'fas fa-times']) . ' ' . Yii::t('mod_actionlogs', 'PURGE');
$expSelBtnText = Html::tag('i', '', ['class' => 'fas fa-download']) . ' ' . Yii::t('mod_actionlogs', 'EXPORT_SELECTED_AS_CSV');
$expAllBtnText = Html::tag('i', '', ['class' => 'fas fa-download']) . ' ' . Yii::t('mod_actionlogs', 'EXPORT_ALL_AS_CSV');
?>
<div class="row actionlogs-default-index">
    <div class="col-12">
        <?php Pjax::begin(); ?>
        <div class="card">
            <div class="card-header">
                <?= Html::button($toolbarBtnText, [
                    'class' => 'btn btn-default btn-block btn-sm btn-subhead',
                    'data-toggle' => 'collapse',
                    'data-target' => '#toolbar',
                    'aria-expanded' => 'false'
                ]) ?>
                <div id="toolbar" class="btn-toolbar collapse" role="toolbar">
                    <?= Html::a($delBtnText, null, [
                        'id' => 'actionlogs_del',
                        'class' => 'btn btn-default btn-sm mr-2'
                    ]) ?>
                    <?= Html::a($purBtnText, null, [
                        'class' => 'btn btn-default btn-sm mr-2',
                        'data' => [
                            'confirm' => Yii::t('mod_actionlogs', 'DELETE_ITEMS_CONFIRM_ALL_LOGS'),
                            'method' => 'post',
                            'params' => [
                                'purge' => true
                            ]
                        ]
                    ]) ?>
                    <?= Html::a($expSelBtnText, null, [
                        'id' => 'actionlogs_sel',
                        'class' => 'btn btn-default btn-sm mr-2'
                    ]) ?>
                    <?= Html::a($expAllBtnText, null, [
                        'class' => 'btn btn-default btn-sm',
                        'data' => [
                            'method' => 'post',
                            'params' => [
                                'exportAllLogs' => true
                            ]
                        ]
                    ]) ?>
                </div>
            </div>
            <div class="card-body p-0">
                <?=
                Html::beginForm('', 'post', ['id' => 'actionlogs_form']) .
                GridView::widget([
                    'id' => 'actionlogs_list',
                    'layout' => '{items}',
                    'tableOptions' => ['class' => 'table table-striped table-hover'],
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => \yii\grid\CheckboxColumn::class,
                            'headerOptions' => ['width' => '3%'],
                            'checkboxOptions' => function ($model) {
                                return ['id' => 'cb' . $model->id];
                            }
                        ],
                        [
                            'attribute' => 'info',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $message = json_decode($model->message, true);
                                return Yii::t('plg_actionlog', $model->info, $message);
                            }
                        ],
                        [
                            'attribute' => 'log_date',
                            'headerOptions' => ['width' => '11%'],
                            'format' => 'raw',
                            'value' => function ($model) {
                                $formatter = Yii::$app->formatter;
                                $relativeTime = $formatter->asRelativeTime($model->log_date);
                                $datetime = $formatter->asDatetime($model->log_date);
                                return Html::tag('span', $relativeTime, [
                                    'data-toggle' => 'tooltip',
                                    'title' => $datetime
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'ip_address',
                            'headerOptions' => ['width' => '9%']
                        ],
                        [
                            'attribute' => 'id',
                            'headerOptions' => ['width' => '4%']
                        ]
                    ]
                ]) . Html::endForm()
                ?>
            </div>
            <div class="card-footer clearfix">
                <?= GridView::widget([
                    'layout' => '{summary}{pager}',
                    'summaryOptions' => ['class' => 'summary float-left'],
                    'pager' => [
                        'options' => ['class' => 'pagination m-0 float-right'],
                        'linkContainerOptions' => ['class' => 'page-item'],
                        'linkOptions' => ['class' => 'page-link'],
                        'disabledListItemSubTagOptions' => ['class' => 'page-link']
                    ],
                    'dataProvider' => $dataProvider
                ]) ?>
            </div>
        </div>
        <?php $this->registerJs($js);
        Pjax::end(); ?>
    </div>
</div>