<?php

/* @var $this yii\web\View */
/* @var $model backend\modules\admin\models\form\Login */

// $model->getAttributeLabel('username')
//print_r($model->formName());exit;

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use common\components\Helper;
use backend\themes\adminlte\assets\AppAsset;

$asset = AppAsset::register($this);

array_splice($asset->css, 2, 1);
array_splice($asset->css, 1, 0, [
    //'https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css',
    'plugins/icheck-bootstrap/icheck-bootstrap.css'
]);
array_splice($asset->js, 1, 1);

$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', strtoupper(Helper::getClient()));

$js = <<<JS
$('.invalid-feedback').each(function() {
  if(!$(this).is(':empty')){
      $(this).show().css('margin-top', '-0.75rem');
  }
});
$('.invalid-feedback').bind({
  DOMNodeInserted: function() {
    $(this).show().css('margin-top', '-0.75rem');
  },
  DOMNodeRemoved: function() {
    $(this).hide();
  }
});
JS;
$this->registerJs($js);

$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode(Yii::t('rbac-admin', 'LOGIN')) ?> - <?= $appName ?> - <?= $client ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition login-page">
<?php $this->beginBody() ?>
<div class="login-box">
    <div class="login-logo"><strong>Admin</strong>LTE</div>
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg"><?= Yii::t('rbac-admin', 'USER_LOGIN_FORM_HEAD') ?></p>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username', [
                'inputTemplate' => '<div class="input-group mb-3">
                    {input}
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-user"></i>
                        </div>
                    </div>
                </div>',
                'inputOptions' => ['placeholder' => true]
            ])->label(false) ?>
            <?= $form->field($model, 'password', [
                'inputTemplate' => '<div class="input-group mb-3">
                    {input}
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fas fa-lock"></i>
                        </div>
                    </div>
                </div>',
                'inputOptions' => ['placeholder' => true]
            ])->passwordInput()->label(false) ?>
            <div class="row">
                <div class="col-8">
                    <?= $form->field($model, 'rememberMe', [
                        'options' => ['class' => ['widget' => 'form-group icheck-primary']],
                        'checkTemplate' => '{input}{label}{error}',
                        'checkOptions' => [
                            'class' => '',
                            'labelOptions' => []
                        ]
                    ])->checkbox() ?>
                </div>
                <div class="col-4">
                    <button type="submit"
                            class="btn btn-primary btn-block"><?= Yii::t('rbac-admin', 'SIGN_IN') ?></button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <p class="mb-1">
                <?= Html::a(Yii::t('rbac-admin', 'FORGOT_PASSWORD'), ['user/request-password-reset']) ?>
            </p>
            <p class="mb-0">
                <?= Html::a(Yii::t('rbac-admin', 'REGISTER_NEW_MEMBER'), ['user/signup']) ?>
            </p>
        </div>
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

