<?php

use yii\grid\GridView;
use yii\bootstrap4\Html;
use backend\themes\adminlte\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $dataProvider */

$asset = AppAsset::register($this);
array_splice($asset->css, 1, 0, [
    'plugins/icheck-bootstrap/icheck-bootstrap.css'
]);

$this->title = Yii::t('mod_themes', 'SUBMENU_STYLES');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'styles';

$this->registerCss('.card-body .table>thead>tr>td, .card-body .table>thead>tr>th{border-top-width: 0;}');
$js = <<<JS
$('[data-toggle="tooltip"]').tooltip({trigger: 'hover', html: true});
$('#theme_style_table input:checkbox').each(function() {
    if($(this).attr('name') === 'selection_all'){
        $(this).attr('id', 'cb_all');
    }
    const outerHTML = $(this).prop('outerHTML');
    const labelHTML =  '<label for="'+$(this).attr('id')+'"></label>';
    $(this).replaceWith('<div class="icheck-primary d-inline">' + outerHTML + labelHTML + '</div>');
});
JS;
$this->registerJs($js);

?>
<div class="row themes-style-index">
    <div class="col-12">
        <div class="card">
            <div class="card-body p-0">
                <?=
                Html::beginForm('', 'post', ['id' => 'themes_form']) .
                GridView::widget([
                    'id' => 'themes_list',
                    'layout' => '{items}',
                    'tableOptions' => [
                        'id' => 'theme_style_table',
                        'class' => 'table table-condensed table-striped table-hover'
                     ],
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => \yii\grid\CheckboxColumn::class,
                            'headerOptions' => ['width' => '2%'],
                            'checkboxOptions' => function($model){
                                return ['id' => 'cb' . $model->id];
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $title = Yii::t('mod_themes', $model->title);
                                if ($model->title === $title) {
                                    $title = ucfirst($model->template) . ' - Default';
                                }
                                return Html::a($title, ['edit', 'id' => $model->id]);
                            }
                        ],
                        [
                            'attribute' => 'home',
                            'format' => 'raw',
                            'value' => function($model){
                                $icons = Html::tag('i', '', ['class' => 'fas fa-star text-warning']);
                                $iconr = Html::tag('i', '', ['class' => 'far fa-star']);
                                $link = Html::a($model->home ? $icons : $iconr, null, [
                                    'class' => 'btn btn-default btn-xs'. ($model->home ? ' disabled' : ''),
                                    'data' => [
                                        'method' => 'post',
                                        'params' => [
                                            'id' => $model->id,
                                            'client_id' => $model->client_id
                                        ]
                                    ]
                                ]);
                                return Html::tag('span', $link, [
                                    'title' => Yii::t('mod_themes', $model->home ? 'DEFAULT' : 'SET_DEFAULT'),
                                    'data-toggle' => 'tooltip'
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'template',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::a(ucfirst($model->template), ['template/edit', 'id' => $model->id]);
                            }
                        ]
                    ]
                ]). Html::endForm()
                ?>
        </div>
        <div class="card-footer clearfix">
            <?= GridView::widget(['layout' => '{pager}{summary}','dataProvider' => $dataProvider]) ?>
        </div>
    </div>
</div>
