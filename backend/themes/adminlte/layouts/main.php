<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use common\components\Helper;
use backend\themes\adminlte\assets\AppAsset;

$asset = AppAsset::register($this);
$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', strtoupper(Helper::getClient()));

if (!($this->params['navShow'] ?? true)) {
    $css = <<<CSS
body{margin-top: -20px;padding-top: 20px;}
.content-wrapper{align-items: center;display: flex;}
section.content{margin: 0 auto;}
CSS;
    $this->registerCss($css);
}

$mainTreeMenus = Helper::getTree(Helper::getMenus('main'), null, 3);
$navItems = navItems($mainTreeMenus);
function navItems($menus)
{
    $items = [];
    foreach ($menus as $menu) {
        $data = json_decode($menu['data'], true);
        $isCat = isset($data['lang-cat']) && !empty(isset($data['lang-cat']));
        $lable = $isCat ? Yii::t($data['lang-cat'], $menu['name']) : $menu['name'];
        if (empty($menu['children'])) {
            if ($menu['type'] === 'separator') {
                $items[$menu['id']] = [
                    'label' => '',
                    'url' => null,
                    'options' => ['class' => 'nav-item divider']
                ];
            } else {
                $items[$menu['id']] = [
                    'label' => $lable,
                    'url' => empty($menu['route']) ? '#' : [strstr($menu['route'], '/')],
                    'options' => ['class' => 'nav-item']
                ];
            }
        } else {
            $items[$menu['id']] = [
                'label' => $lable.'<i class="right fas fa-angle-left"></i>',
                'url' => '#',
                'items' => navItems($menu['children']),
                'itemOptions' => ['class' => 'hehe'],
                'options' => ['class' => 'nav-item has-treeview']
            ];
        }
    }
    return $items;
}
$submenus = Helper::addSubmenus($this->params['active'] ?? null) ?? [];

$this->beginPage();
$this->registerCsrfMetaTags();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title><?= Html::encode($this->title) ?> - <?= $appName ?> - <?= $client ?></title>
<?php $this->head() ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<?php $this->beginBody() ?>
<div class="wrapper">
    <?php if ($this->params['navShow'] ?? true): ?>
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown user-menu">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img class="user-image img-circle elevation-2"
                             src="<?= $asset->baseUrl ?>/dist/img/user2-160x160.jpg" alt="Uer Image">
                        <span class="d-none d-md-inline">Alexander Pierce</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <li class="user-header bg-primary">
                            <img class="img-circle elevation-2" src="<?= $asset->baseUrl ?>/dist/img/user2-160x160.jpg"
                                 alt="Uer Image">
                            <p>
                                <span>Alexander Pierce - Web Developer</span>
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <li class="user-body">
                            <div class="row">
                                <div class="col-4 text-center">
                                    <a href="#">Followers</a>
                                </div>
                                <div class="col-4 text-center">
                                    <a href="#">Sales</a>
                                </div>
                                <div class="col-4 text-center">
                                    <a href="#">Friends</a>
                                </div>
                            </div>
                        </li>
                        <li class="user-footer">
                            <?= Html::a(Yii::t('site', 'PROFILE'), '#', [
                                'class' => 'btn btn-default btn-flat'
                            ]) ?>
                            <?= Html::a(Yii::t('site', 'LOGOUT'), ['/site/logout'], [
                                'class' => 'btn btn-default btn-flat float-right',
                                'data-method' => 'post'
                            ]) ?>
                        </li>
                    </ul>
                </li>
                <!-- Messages Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-comments"></i>
                        <span class="badge badge-danger navbar-badge">3</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="<?= $asset->baseUrl ?>/dist/img/user1-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="<?= $asset->baseUrl ?>/dist/img/user8-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        John Pierce
                                        <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">I got your message bro</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <!-- Message Start -->
                            <div class="media">
                                <img src="<?= $asset->baseUrl ?>/dist/img/user3-128x128.jpg" alt="User Avatar"
                                     class="img-size-50 img-circle mr-3">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Nora Silvester
                                        <span class="float-right text-sm text-warning">
                                            <i class="fas fa-star"></i>
                                        </span>
                                    </h3>
                                    <p class="text-sm">The subject goes here</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                            <!-- Message End -->
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                    </div>
                </li>
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="far fa-bell"></i>
                        <span class="badge badge-warning navbar-badge">15</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <span class="dropdown-item dropdown-header">15 Notifications</span>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-users mr-2"></i> 8 friend requests
                            <span class="float-right text-muted text-sm">12 hours</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item">
                            <i class="fas fa-file mr-2"></i> 3 new reports
                            <span class="float-right text-muted text-sm">2 days</span>
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->
    <?php endif; ?>

    <?php if ($this->params['navShow'] ?? true): ?>
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="<?= Yii::$app->homeUrl ?>" class="brand-link">
                <?= Html::img($asset->baseUrl . '/dist/img/AdminLTELogo.png', [
                    'class' => 'brand-image img-circle elevation-3',
                    'alt' => 'AdminLTE Logo',
                    'style' => 'opacity: .8'
                ]) ?>
                <span class="brand-text font-weight-light"><?= Yii::$app->name ?></span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                     <?= Menu::widget([
                        'items' => $navItems,
                        'options' => [
                            'class' => 'nav nav-pills nav-sidebar flex-column nav-child-indent',
                            'data-widget' => 'treeview',
                            'data-accordion' => 'false',
                            'role' => 'menu'
                        ],
                        'submenuTemplate' => '<ul class="nav nav-treeview">{items}</ul>',
                        'linkTemplate' => '<a class="nav-link" href="{url}"><p>{label}</p></a>',
                        'encodeLabels' => false,
                        'activateItems' => false
                    ]) ?>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
    <?php endif; ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper<?= ($this->params['navShow'] ?? true) ? '' : ' ml-0' ?>">
        <?php if ($this->params['navShow'] ?? true): ?>
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-12">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark"><?= $this->title ?></h1>
                        </div>
                        <div class="col-sm-6">
                            <?= Breadcrumbs::widget([
                                'tag' => 'ol',
                                'options' => ['class' => 'breadcrumb float-sm-right'],
                                'itemTemplate' => '<li class="breadcrumb-item">{link}</li>',
                                'activeItemTemplate' => '<li class="breadcrumb-item active">{link}</li>',
                                'links' => $this->params['breadcrumbs'] ?? []
                            ]) ?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <!-- Main content -->
        <section class="content">
            <?= Helper::widget('alert') ?>
            <?= $content ?>
        </section>
    </div>

    <footer class="main-footer<?= ($this->params['navShow'] ?? true) ? '' : ' ml-0' ?>">
        <strong>Copyright &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
            <?= Yii::t('yii', 'Powered by {yii}', [
                'yii' => Html::a(Yii::t('yii', 'Yii Framework'), 'http://www.yiiframework.com/')
            ]) ?>

        </div>
    </footer>
    <aside class="control-sidebar control-sidebar-dark">
        <div class="p-3">
            <?= Nav::widget(['items' => $submenus, 'encodeLabels' => false, 'options' => ['class' => 'nav nav-sidebar flex-column']]) ?>
        </div>
    </aside>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

