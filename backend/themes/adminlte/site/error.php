<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = Yii::t('yii', 'ERROR');
$this->params['breadcrumbs'][] = $this->title;

$this->params['navShow'] = !Yii::$app->user->isGuest;
?>
<div class="site-error error-page">

    <h2 class="headline text-warning"><?= $exception->statusCode ?></h2>

    <div class="error-content">
        <h3><i class="fas fa-exclamation-triangle text-warning"></i> <?= $exception->getName() ?></h3>
        <p>
            <?= nl2br(Html::encode($message)) ?><br/>
            The above error occurred while the Web server was processing your request.<br/>
            Please contact us if you think this is a server error. Thank you.
        </p>
    </div>

    <p><?= Html::a(Yii::t('common', 'RETURN_URL'), Yii::$app->user->returnUrl, [
            'class' => 'btn btn-primary btn-block'
        ]) ?></p>

</div>
