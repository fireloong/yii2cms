<?php

/* @var $this yii\web\View */
/* @var $model backend\models\Languages */

$this->title = Yii::t('languages', 'Create Languages');
$this->params['breadcrumbs'][] = ['label' => Yii::t('languages', 'Languages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languages-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
