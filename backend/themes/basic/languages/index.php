<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LanguagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('languages', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="languages-index">

    <p>
        <?= Html::a(Yii::t('languages', 'Create Languages'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'code',
            'title',
            'title_native',
            //'sef',
            //'image',
            //'sitename',
            //'published',
            //'ordering',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
