<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use backend\themes\basic\assets\AppAsset;

AppAsset::register($this);
(new AppAsset())->addCss('site.css');

$this->title = Yii::t('site', 'LOGIN');

$lockIcon = Html::tag('span', '', ['class' => 'glyphicon glyphicon-lock']) . '&emsp;';

$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>

    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="container">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'form-signin']]) ?>
            <h2 class="form-signin-heading"><?= Yii::t('site', 'Please sign in') ?></h2>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="checkbox">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>

            <?= Html::submitButton($lockIcon . Yii::t('site', 'Sign in'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>


            <!--            <label for="inputEmail" class="sr-only">Email address</label>
                        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                        <label for="inputPassword" class="sr-only">Password</label>
                        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me"> Remember me
                            </label>
                        </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>-->
            <?php ActiveForm::end() ?>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
