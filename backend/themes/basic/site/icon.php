<?php

use backend\components\Icon;

/* @var $this \yii\web\View */
/* @var $icon array */

$this->title = Yii::t('site', 'FONT_ICON');
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.icons{
margin: 1rem 0;
}
.icons li{
padding: 0;
background-color: #eee;
border: 1px solid #e0e4e9;
border-radius: 4px;
font-size: 12px;
text-align: center;
margin: 0.5rem;
width: 100px;
height: 100px;
position: relative;
}
.icons li .icon{
font-size: 36px;
position: absolute;
top: 50%;
left: 50%;
transform: translate(-50%,-50%);
margin-top: -10px;
}
.icons li .name,.icons li .unicode{
color: #666666;
}
.icons li .name{
position: absolute;
bottom: 2px;
left: 0;
width: 100%;
text-align: center;
}
CSS;
$this->registerCss($css);

$js = <<<JS
$('#search').change(function(){
    var val = $.trim($(this).val());
    if(val === ''){
        $('.icons li').show();
    }else{
        $('.icons li').hide();
        $('.icons li[class*="'+val+'"]').show();
    }
});
JS;
$this->registerJs($js);
?>
<div class="site-icon">
    <div class="row">
        <div class="col-sm-6">
            <input id="search" class="form-control" type="text" placeholder="<?= Yii::t('common', 'SEARCH') ?>">
        </div>
        <div class="col-sm-12">
            <ul class="icons list-inline">
                <?php foreach ($icon as $name => $unicode): ?>
                    <li class="<?= $name ?>">
                        <div class="icon"><?= Icon::i($name) ?></div>
                        <div class="name"><?= $name ?></div>
                        <div class="unicode hide">Unicode: <?= $unicode ?></div>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>