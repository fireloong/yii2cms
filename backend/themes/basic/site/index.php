<?php

use common\components\Helper;

/* @var $this yii\web\View */

$this->title = Yii::t('site', 'CONTROL_PANEL');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="body-content">
        <div class="row"><?= Helper::hook('cpanel') ?></div>
    </div>
</div>
