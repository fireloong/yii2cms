<?php

use yii\bootstrap\Html;

/* @var $this \yii\web\View */

$this->title = Yii::t('site', 'SYSTEM_INFORMATION');
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS
.site-sysinfo .nav.nav-tabs{margin-bottom: 18px;}
.site-sysinfo .page-header{padding-bottom: 0;height: 36px;line-height: 36px;}
.site-sysinfo .adminlist .e{width:20%;}
CSS;
$this->registerCss($css);
?>

<div class="site-sysinfo">

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <?php
        foreach (array_keys($data) as $key) {
            $options = $key == 'info' ? ['class' => 'active'] : [];
            echo Html::tag('li', Html::a($msg[$key], '#' . $key, [
                'aria-controls' => $key,
                'role' => 'tab',
                'data-toggle' => 'tab'
            ]), $options);
        }
        ?>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <?php
        foreach ($data as $key => $item) {
            $options = [
                'id' => $key,
                'class' => 'tab-pane' . ($key == 'info' ? ' active' : ''),
                'role' => 'tabpanel'
            ];

            $content = Html::tag('h3', $msg[$key], ['class' => 'page-header']);
            if ($key === 'phpInfo') {
                $content .= $item;
            } else {
                $settingMsg = Yii::t('site', 'SETTING');
                $valueMsg = Yii::t('site', 'VALUE');
                $thContent = Html::tag('th', $settingMsg, ['width' => '20%']) . Html::tag('th', $valueMsg);
                $theadContent = Html::tag('thead', Html::tag('tr', $thContent));
                $itemContent = '';
                foreach ($item as $k => $v) {
                    $setting = Html::tag('td', $msg[$k]);
                    $value = Html::tag('td', $v);
                    $itemContent .= Html::tag('tr', $setting . $value);
                }
                $tbodyContent = Html::tag('tbody', $itemContent);
                $content .= Html::tag('table', $theadContent . $tbodyContent, ['class' => 'table table-striped']);
            }
            echo Html::tag('div', $content, $options);
        }
        ?>
    </div>

</div>
