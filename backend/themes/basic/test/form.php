<?php

/* @var $this \yii\web\View */

/* @var $model \backend\models\TestForm */

use yii\helpers\Html;
use common\components\form\Form;

$this->title = 'Test Form';
?>

<?php $form = Form::begin(['id' => 'test_form']); ?>
<?= $form->field($model, 'title', 'RadioList', ['setupParams' => [['a'=>'b','b'=>'c','c'=>'d']]]) ?>

<?= $form->field($model,'content','Editor') ?>
<?= $form->field($model,'author','text') ?>
<?= $form->field($model,'date','datetime') ?>

<div class="form-group">
    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
</div>
<?php Form::end(); ?>

