<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Extensions */

$this->title = Yii::t('extensions', 'Update Extensions: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('extensions', 'Extensions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('extensions', 'Update');
$this->params['active'] = 'update';
?>
<div class="extensions-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
