<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::t('extensions', 'EXTENSIONS_INSTALL');
$this->params['breadcrumbs'][] = ['label' => Yii::t('extensions', 'EXTENSIONS'), 'url' => ['installer']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'installer';

$this->registerJs($this->render('_script.js'));
$maxSize = Yii::$app->formatter->asShortSize($model->maxSize);
?>
<div class="extensions-installer">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation">
            <a href="#web" aria-controls="web" role="tab" data-toggle="tab">
                <?= Yii::t('extensions', 'INSTALL_FROM_WEB') ?>
            </a>
        </li>
        <li role="presentation">
            <a href="#package" aria-controls="package" role="tab" data-toggle="tab">
                <?= Yii::t('extensions', 'UPLOAD_PACKAGE_FILE') ?>
            </a>
        </li>
        <li role="presentation">
            <a href="#folder" aria-controls="folder" role="tab" data-toggle="tab">
                <?= Yii::t('extensions', 'INSTALL_FROM_FOLDER') ?>
            </a>
        </li>
        <li role="presentation">
            <a href="#url" aria-controls="url" role="tab" data-toggle="tab">
                <?= Yii::t('extensions', 'INSTALL_FROM_URL') ?>
            </a>
        </li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="web">
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                不能连接到服务器。 请稍候再试！
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="package">
            <?php $form = ActiveForm::begin(['id' => 'installer-form']) ?>
            <fieldset class="uploadform">
                <legend><?= Yii::t('extensions', 'UPLOAD_INSTALL_EXTENSION') ?></legend>
                <div id="uploader-wrapper">
                    <div id="dragarea">
                        <div id="dragarea-content" class="text-center">
                            <p>
                                <span id="upload-icon" class="glyphicon glyphicon-open" aria-hidden="true"></span>
                            </p>
                            <div class="upload-actions">
                                <p class="lead"><?= Yii::t('extensions', 'DRAG_FILE_HERE') ?></p>
                                <p>
                                    <button id="select-file-button" type="button" class="btn btn-success">
                                        <span class="glyphicon glyphicon-duplicate" aria-hidden="true"></span>
                                        <?= Yii::t('extensions', 'SELECT_FILE') ?>
                                    </button>
                                </p>
                                <p><?= Yii::t('extensions', 'MAXIMUM_UPLOAD_SIZE_LIMIT', $maxSize) ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="legacy-uploader" class="hidden">
                    <?= $form->field($model, 'file')->fileInput() ?>
                    <?= Html::submitButton(Yii::t('extensions', 'UPLOAD_INSTALL'), ['id' => 'installbutton_package']) ?>
                </div>
            </fieldset>
            <?php ActiveForm::end() ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="folder">
            <fieldset class="uploadform">
                <legend><?= Yii::t('extensions', 'FOLDER_INSTALLER') ?></legend>
                <div class="form-group">
                    <?= Html::label(Yii::t('extensions', 'FOLDER_INSTALLER'), 'install_directory', [
                        'class' => 'control-label'
                    ]) ?>
                    <div class="controls">
                        <input id="install_directory" type="text" name="install_directory"
                               value="<?= Yii::getAlias('@common/tmp/') ?>">
                    </div>
                </div>
                <div class="form-actions">
                    <?= Html::submitButton(Yii::t('extensions', 'FOLDER_INSTALLER_BUTTON'), [
                        'id' => 'installbutton_directory',
                        'class' => 'btn btn-primary'
                    ]) ?>
                </div>
            </fieldset>
        </div>
        <div role="tabpanel" class="tab-pane" id="url">
            <fieldset class="uploadform">
                <legend><?= Yii::t('extensions', 'INSTALL_FROM_URL') ?></legend>
                <div class="form-group">
                    <?= Html::label(Yii::t('extensions', 'INSTALL_FROM_URL'), 'install_url', [
                        'class' => 'control-label'
                    ]) ?>
                    <div class="controls">
                        <input id="install_url" type="text" name="install_url" placeholder="https://">
                    </div>
                </div>
                <div class="form-actions">
                    <?= Html::submitButton(Yii::t('extensions', 'URL_INSTALLER_BUTTON'), [
                        'id' => 'installbutton_url',
                        'class' => 'btn btn-primary'
                    ]) ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
