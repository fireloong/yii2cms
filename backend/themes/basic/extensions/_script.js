$('#select-file-button').on('click', function (e) {
    $('#uploadform-file').click();
});
$('#uploadform-file').on('change', function (e) {
    setTimeout(function () {
        var txt = $('#uploadform-file + .help-block').text();
        if (txt !== '') {
            $('#ps').remove();
            $('.upload-actions').append('<p id="ps" class="text-danger">' + txt + '</p>');
        }
    }, 600);
    $('#installbutton_package').click();
});
/**
 * 拖放上传功能，待完善
 */
var dragZone = $('#dragarea');
dragZone.on('dragenter', function (e) {
    e.preventDefault();
    e.stopPropagation();
    dragZone.addClass('hover');
    return false;
});
// Notify user when file is over the drop area
dragZone.on('dragover', function (e) {
    e.preventDefault();
    e.stopPropagation();

    dragZone.addClass('hover');

    return false;
});
dragZone.on('dragleave', function (e) {
    e.preventDefault();
    e.stopPropagation();
    dragZone.removeClass('hover');

    return false;
});
var uploading = false;
dragZone.on('drop', function (e) {
    e.preventDefault();
    e.stopPropagation();

    dragZone.removeClass('hover');

    if (uploading) {
        return;
    };

    var files = e.originalEvent.target.files || e.originalEvent.dataTransfer.files;

    if (!files.length) {
        return;
    };

    var file = files[0];

    var data = new FormData();
    data.append('file', file);
    //console.log(file.name);
    alert('<?= Yii::t('extensions','Function to be improved.') ?>');
});

/**
 * 标签点击停留状态
 */
$('.extensions-installer .nav-tabs a').on('click', function () {
    sessionStorage.setItem(window.location.pathname, $(this).attr('href'));
});

var active = sessionStorage.getItem(window.location.pathname);
if (active !== null) {
    $('.extensions-installer .nav-tabs li a[href="' + active + '"]').tab('show');
} else {
    $('.extensions-installer .nav-tabs li a[href="#web"]').tab('show');
};

$('.select-on-check-all').attr({'data-toggle':'tooltip', 'title':'<?= Yii::t('common', 'CHECK_ALL') ?>'});

$('[data-toggle="tooltip"]').tooltip({trigger: 'hover', html: true});

$('#extensions_enable').click(function(e){
    var keys = $('#extensions_list').yiiGridView('getSelectedRows');
    if (keys.length){
        $('#extensions_form').append('<input type="hidden" name="action" value="enable">').submit();
    } else{
        alert('<?= Yii::t('extensions','SELECTION_FROM_LIST') ?>');
    }
});

$('#extensions_disable').click(function(e){
    var keys = $('#extensions_list').yiiGridView('getSelectedRows');
    if (keys.length){
        $('#extensions_form').append('<input type="hidden" name="action" value="disable">').submit();
    } else{
        alert('<?= Yii::t('extensions','SELECTION_FROM_LIST') ?>');
    }
});

$('#extensions_uninstall').click(function(e){
    var keys = $('#extensions_list').yiiGridView('getSelectedRows');
    if (keys.length){
        yii.confirm('<?= Yii::t('extensions','CONFIRM_UNINSTALL') ?>', function(){
            $('#extensions_form').append('<input type="hidden" name="action" value="uninstall">').submit();
        });
    } else{
        alert('<?= Yii::t('extensions','SELECTION_FROM_LIST') ?>');
    }
});
