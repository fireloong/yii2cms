<?php

use yii\bootstrap\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('extensions', 'EXTENSIONS');
$this->params['breadcrumbs'][] = $this->title;
$this->params['active'] = 'manage';

$css = <<<CSS
a.btn-xs.disabled{pointer-events:auto;cursor:default;opacity:0.65;}
label span[data-toggle="tooltip"]{font-weight:normal;cursor:pointer;}
label .tooltip .tooltip-inner,td .tooltip .tooltip-inner{text-align:left;max-width:none;}
label .tooltip .tooltip-inner strong{white-space:nowrap;}
label .tooltip{max-width: 400px;}
CSS;

$this->registerCss($css);

$this->registerJs($this->render('_script.js'));

$iconHtml = [
    'ok' => Html::icon('ok', ['class' => 'text-success']),
    'circle' => Html::icon('remove-circle', ['class' => 'text-danger']),
    'lock' => Html::icon('lock'),
    'repeat' => Html::icon('repeat'),
    'remove' => Html::icon('remove')
];
?>
<div class="extensions-index">

    <p class="btn-toolbar">
        <?= Html::button($iconHtml['ok'] . ' ' . Yii::t('common', 'ENABLE'), [
            'id' => 'extensions_enable',
            'class' => 'btn btn-default btn-sm'
        ]) ?>
        <?= Html::button($iconHtml['circle'] . ' ' . Yii::t('common', 'DISABLE'), [
            'id' => 'extensions_disable',
            'class' => 'btn btn-default btn-sm'
        ]) ?>
        <?= Html::button($iconHtml['repeat'] . ' ' . Yii::t('common', 'REFRESH_CACHE'), [
            'id' => 'extensions_refresh',
            'class' => 'btn btn-default btn-sm'
        ]) ?>
        <?= Html::button($iconHtml['remove'] . ' ' . Yii::t('common', 'UNINSTALL'), [
            'id' => 'extensions_uninstall',
            'class' => 'btn btn-default btn-sm'
        ]) ?>
    </p>

    <?=
    Html::beginForm('', 'post', ['id' => 'extensions_form']) .
    GridView::widget([
        'id' => 'extensions_list',
        'layout' => '{items}{pager}{summary}',
        'tableOptions' => ['class' => 'table table-striped'],
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'class' => \yii\grid\CheckboxColumn::class,
                'headerOptions' => ['width' => '1%'],
                'checkboxOptions' => function ($model) {
                    return ['id' => 'cb' . $model->id];
                },
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'headerOptions' => ['width' => '2%', 'class' => 'text-nowrap'],
                'value' => function ($model) use ($iconHtml) {
                    $status = [
                        'enabled' => Html::a($iconHtml['ok'], '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('extensions', 'DISABLE_EXTENSION'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ]),
                        'disabled' => Html::a($iconHtml['circle'], '', [
                            'class' => 'btn btn-default btn-xs',
                            'title' => Yii::t('extensions', 'ENABLE_EXTENSION'),
                            'data' => [
                                'toggle' => 'tooltip',
                                'method' => 'post',
                                'params' => [
                                    'id' => $model->id,
                                ],
                            ],
                        ]),
                        'protected' => Html::a($iconHtml['lock'], null, [
                            'class' => 'btn btn-default btn-xs disabled',
                            'role' => 'button',
                            'data-toggle' => 'tooltip',
                            'title' => Yii::t('extensions', 'PROTECTED_EXTENSION'),
                        ]),
                    ];

                    return $model->protected === 0 ? ($model->status === 0 ? $status['disabled'] : $status['enabled']) : $status['protected'];
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function ($model) {
                    $desc = $model->manifest_cache['description'];
                    $langCat = $model->manifest_cache['langCat'] ?? null;
                    if ($model->type === 'theme') {
                        $client = \backend\models\Clients::findOne($model->client_id);
                        Yii::$app->i18n->translations[$langCat ?? '*'] = [
                            'class' => 'yii\i18n\PhpMessageSource',
                            'basePath' => '@' . $client->name . '/themes/' . $model->element . '/messages'
                        ];
                    }
                    $name = $langCat ? Yii::t($langCat, $model->name) : $model->name;
                    $description = $langCat ? Yii::t($langCat, $desc) : $desc;

                    return Html::label(Html::tag('span', $name, [
                        'data-toggle' => 'tooltip',
                        'title' => '<strong>' . $name . '</strong><br>' . $description,
                    ]), 'cb' . $model->id);
                },
            ],
            [
                'attribute' => 'type',
                'value' => function ($model) {
                    return Yii::t('extensions', strtoupper($model->type));
                },
            ],
            [
                'label' => Yii::t('extensions', 'VERSION'),
                'value' => function ($model) {
                    return $model->manifest_cache['version'];
                },
            ],
            [
                'label' => Yii::t('extensions', 'DATE'),
                'headerOptions' => ['width' => '10%'],
                'value' => function ($model) {
                    return $model->manifest_cache['creationDate'];
                },
            ],
            [
                'label' => Yii::t('extensions', 'AUTHOR'),
                'format' => 'raw',
                'headerOptions' => ['width' => '15%'],
                'value' => function ($model) {
                    return Html::tag('span', $model->manifest_cache['author'], [
                        'data-toggle' => 'tooltip',
                        'title' => '<strong>' . Yii::t('extensions', 'AUTHOR_INFORMATION') . '</strong><br>' .
                            $model->manifest_cache['authorEmail'] . '<br>' . $model->manifest_cache['authorUrl'],
                    ]);
                },
            ],
            [
                'attribute' => 'client_id',
                'value' => function ($model) {
                    return $model->clients === null ? null : $model->clients->name;
                },
            ],
            [
                'attribute' => 'package_id',
                'value' => function ($model) {
                    return $model->package_id === 0 ? '' : $model->package_id;
                },
            ],
            'id',
        ],
    ]) . Html::endForm()
    ?>


</div>
