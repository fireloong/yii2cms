<?php

namespace backend\themes\basic\assets;

use Yii;
use yii\web\AssetBundle;
use yii\helpers\Url;
use yii\helpers\FileHelper;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{

    public $sourcePath = '@backend/themes/basic/media';
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\bootstrap\BootstrapThemeAsset'
    ];

    /**
     * 视图文件中添加CSS文件
     * @param string|array $cssFile 要添加的CSS文件名
     * @param boolean $isAfter 是否放在 $css 后面
     * @param string $dir
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     */
    public function addCss($cssFile, $isAfter = true, $dir = 'css')
    {
        if (is_string($cssFile)) {
            $cssFile = array($cssFile);
        }
        if (!is_array($cssFile)) {
            return FALSE;
        }

        if (!is_null($this->sourcePath)) {
            $url = Yii::$app->assetManager->getPublishedUrl($this->sourcePath);
            if (!is_dir($this->basePath . str_replace($this->baseUrl, '', $url))) {
                Yii::$app->assetManager->publish($this->sourcePath);
            }
        }

        $cssFiles = [];
        foreach ($cssFile as $item) {
            $dir = '/' . trim($dir, '/ ') . '/';
            $cssFiles[] = Url::isRelative($item) ? FileHelper::normalizePath($url . $dir . $item) : $item;
        }

        $bundle = Yii::$app->getAssetManager()->bundles[self::class];

        $bundle->css = $isAfter ? array_merge($bundle->css, $cssFiles) : array_merge($cssFiles, $bundle->css);
    }

    /**
     * 视图文件中添加JS文件
     * @param string|array $jsFile 要添加的JS文件名
     * @param boolean $isAfter 是否放在 $js 后面
     * @param string $dir
     * @return boolean
     * @throws \yii\base\InvalidConfigException
     */
    public function addJs($jsFile, $isAfter = true, $dir = 'js')
    {
        if (is_string($jsFile)) {
            $jsFile = array($jsFile);
        }
        if (!is_array($jsFile)) {
            return FALSE;
        }
        if (!is_null($this->sourcePath)) {
            $url = Yii::$app->assetManager->getPublishedUrl($this->sourcePath);
            if (!is_dir($this->basePath . str_replace($this->baseUrl, '', $url))) {
                Yii::$app->assetManager->publish($this->sourcePath);
            }
        }

        $jsFiles = [];
        foreach ($jsFile as $item) {
            $dir = '/' . trim($dir, '/ ') . '/';
            $jsFiles[] = Url::isRelative($item) ? FileHelper::normalizePath($url . $dir . $item) : $item;
        }

        $bundle = Yii::$app->getAssetManager()->bundles[self::class];

        $bundle->js = $isAfter ? array_merge($bundle->js, $jsFiles) : array_merge($jsFiles, $bundle->js);
    }

}
