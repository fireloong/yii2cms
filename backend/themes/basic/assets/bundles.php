<?php

return [
    'yii\web\JqueryAsset' => [
        'sourcePath' => null,
        'js' => ['https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js']
    ]
];
