<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $clients \backend\controllers\CacheController */

$this->title = Yii::t('cache', 'CLEAR_CACHE');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clients-index">
    <p class="btn-toolbar">
        <?php
        foreach ($clients as $clent) {
            echo Html::a(Yii::t('cache','CLEAR_CLIENT_CACHE', ucfirst($clent)), '', [
                'class' => 'btn btn-success',
                'data' => [
                    'method' => 'post',
                    'params' => [
                        'path' => Yii::getAlias('@' . $clent . DIRECTORY_SEPARATOR . 'runtime')
                    ]
                ]
            ]);
        }
        ?>
    </p>
</div>
