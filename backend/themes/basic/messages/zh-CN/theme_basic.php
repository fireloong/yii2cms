<?php

return [
    'THEME_BASIC' => '基本(basic)后台管理主题',
    'THEME_XML_DESCRIPTION' => '基本(basic)是一个管理后台的基本主题模板，基于 Bootstrap 3.4.1 编辑的主题',
    'POSITION_CONTENT-HEADER' => '内容头部位置',
    'POSITION_CPANEL' => '控制面板',
    'POSITION_FOOTER' => '页脚位置'
];
