<?php


namespace backend\themes\basic\widgets\alert;

use Yii;
use yii\helpers\Html;

/**
 * description of Widget
 *
 * @author FireLoong
 */
class Widget extends \yii\bootstrap\Widget
{
    public $widget = 'alert';
    /**
     * @var array the alert types configuration for the flash messages.
     * This array is setup as $key => $value, where:
     * - key: the name of the session flash variable
     * - value: the bootstrap alert type (i.e. danger, success, info, warning)
     */
    public $alertTypes = [
        'error' => 'alert-danger',
        'danger' => 'alert-danger',
        'success' => 'alert-success',
        'info' => 'alert-info',
        'warning' => 'alert-warning',
        'panel' => 'panel panel-default',
        'confirm' => 'alert-warning'
    ];

    /**
     * @var array the options for rendering the close button tag.
     * Array will be passed to [[\yii\bootstrap\Alert::closeButton]].
     */
    public $closeButton = [];

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();
        $appendClass = isset($this->options['class']) ? ' ' . $this->options['class'] : '';

        foreach ($flashes as $type => $flash) {
            if (!isset($this->alertTypes[$type])) {
                continue;
            }

            foreach ((array)$flash as $i => $message) {
                if ($type == 'panel') {
                    $content = Html::tag('div', $message, ['class' => 'panel-body']);
                    echo Html::tag('div', $content, ['class' => $this->alertTypes[$type] . $appendClass]);
                } elseif ($type == 'confirm') {
                    echo \yii\bootstrap\Alert::widget([
                        'body' => Html::tag('p', $message, ['style' => 'margin-bottom: 10px;']) .
                            Html::tag('div', Html::a(Yii::t('common', 'No'), $flashes['linkNo'], [
                                    'class' => 'btn btn-default',
                                    'role' => 'button'
                                ]) . Html::a(Yii::t('common', 'Yes'), $flashes['linkYes'], [
                                    'class' => 'btn btn-primary',
                                    'style' => 'margin-left:15px;',
                                    'data' => [
                                        'method' => 'post',
                                        'params' => [
                                            'action' => 'continue'
                                        ]
                                    ]
                                ])
                            ),
                        'closeButton' => $this->closeButton,
                        'options' => array_merge($this->options, [
                            'id' => $this->getId() . '-' . $type . '-' . $i,
                            'class' => $this->alertTypes[$type] . $appendClass
                        ])
                    ]);
                } else {
                    echo \yii\bootstrap\Alert::widget([
                        'body' => $message,
                        'closeButton' => $this->closeButton,
                        'options' => array_merge($this->options, [
                            'id' => $this->getId() . '-' . $type . '-' . $i,
                            'class' => $this->alertTypes[$type] . $appendClass,
                        ]),
                    ]);
                }
            }

            $session->removeFlash($type);
        }
    }
}
