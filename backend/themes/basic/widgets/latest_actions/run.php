<?php

/* @var $logs */
/* @var $bootstrap_size */

?>
<div class="col-lg-<?= $bootstrap_size ?>">
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading" style="color: #666;">
            <strong><?= Yii::t('mod_actionlogs', 'USER_ACTIONS_LOGS') ?></strong>
        </div>
        <!-- Table -->
        <table class="table table-striped table-hover">
            <tbody>
            <?php foreach ($logs as $log): $message = json_decode($log->message, true); ?>
                <tr>
                    <td><?= Yii::t('plg_actionlog', $log->info, $message) ?></td>
                    <td class="text-right"><?= Yii::$app->formatter->asDatetime($log->log_date) ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
