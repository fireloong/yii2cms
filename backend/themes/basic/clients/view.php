<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Clients */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('clients', 'CLIENTS'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="clients-view">
    <p>
        <?= Html::a(Yii::t('common', 'UPDATE'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'url:url',
            'path',
            'description',
            'ordering',
            'status',
        ],
    ]) ?>

</div>
