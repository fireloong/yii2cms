<?php
/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\components\Icon;
use common\components\Helper;
use backend\themes\basic\assets\AppAsset;

$asset = AppAsset::register($this);
$asset->css[] = '/css/icomoon.css';
$asset->css[] = 'css/common.css';

$items = Helper::getAssignedMenu('main', null, 2);
$submenusShow = $this->params['submenusShow'] ?? true;
$submenus = $submenusShow ? (Helper::addSubmenus($this->params['active'] ?? null) ?? []) : [];
$appName = Yii::t('common', 'APP_NAME');
$client = Yii::t('common', strtoupper(Helper::getClient()));
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= Html::encode($this->title) ?> - <?= $appName ?> - <?= $client ?></title>
<?php
$this->registerCsrfMetaTags();
$this->head();
?>

</head>
<body>
<?php $this->beginBody() ?>
<?php
if ($this->params['navShow'] ?? true) {
    NavBar::begin([
        'options' => ['class' => 'navbar-inverse navbar-fixed-top'],
        'brandLabel' => Icon::i('gl-fire', ['style' => 'color:#d9534f']),
        'innerContainerOptions' => ['class' => 'container-fluid'],
        'containerOptions' => ['id' => 'navbar']
    ]);

    echo Nav::widget([
        'items' => $items,
        'activateItems' => false,
        'options' => ['class' => 'nav navbar-nav navbar-left']
    ]);

    $identity = Yii::$app->user->identity;
    $userHear = Icon::i('gl-user', ['aria-hidden' => 'true'])
        . Html::tag('strong', $identity->admin->name ?: $identity->username, ['class' => 'zmh-ml-7']);

    echo Nav::widget([
        'items' => [
            ['label' => 'Icon', 'url' => ['/site/icon'], 'linkOptions' => ['target' => '_blank']],
            ['label' => 'Gii', 'url' => ['/gii'], 'linkOptions' => ['target' => '_blank']],
            [
                'label' => 'siteHome',
                'url' => Yii::$app->urlManagerFrontend->hostInfo,
                'linkOptions' => ['target' => '_blank']
            ],
            [
                'label' => Icon::i('gl-user', ['aria-hidden' => 'true']),
                'url' => null,
                'items' => [
                    Html::tag('li', $userHear, ['class' => 'dropdown-header']),
                    Html::tag('li', '', ['class' => 'divider']),
                    ['label' => Yii::t('site', 'EDIT_PROFILE'), 'url' => ['/admin/user/profile']],
                    Html::tag('li', '', ['class' => 'divider']),
                    [
                        'label' => Yii::t('site', 'LOGOUT'),
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ],
                'linkOptions' => [
                    'role' => 'button',
                    'aria-haspopup' => 'true'
                ]
            ]
        ],
        'options' => ['class' => 'nav navbar-nav navbar-right'],
        'encodeLabels' => false,
        'activateItems' => false
    ]);
    NavBar::end();
}
?>
<div class="container-fluid zmh-main">
    <div class="row">
        <?php if (!empty($submenus)) { ?>
            <div class="col-sm-3 col-md-2 sidebar">
                <?= Nav::widget(['items' => $submenus, 'encodeLabels' => false, 'options' => ['class' => 'nav nav-sidebar']]) ?>
            </div>
        <?php } ?>
        <div class="<?= empty($submenus) ? 'col-sm-12' : 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2' ?> main">
            <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
            <?= Helper::hook('content-header') ?>
            <?= $content ?>
        </div>
    </div>
</div>

<?php if ($this->params['footerShow'] ?? true): ?>
<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>
<?php endif; ?>
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
