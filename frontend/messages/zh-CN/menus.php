<?php

return [
    'About' => '关于',
    'Contact' => '联系',
    'Signup' => '注册',
    'Login' => '登录',
    'Logout({0})' => '注销（{0}）'
];
