<?php

return [
    'hello' => '哈哈',
    'My Application' => '我的应用',
    'My Yii Application' => '我的 Yii 应用',
    'Congratulations!' => '恭喜！',
    'You have successfully created your Yii-powered application.' => '您已经成功创建了 Yii 的应用程序。',
    'Get started with Yii' => '开始使用 Yii',
    'Heading' => '标题',
    'Yii Documentation' => 'Yii 文档',
    'Yii Forum' => 'Yii 论坛',
    'Yii Extensions' => 'Yii 扩展',
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore '
    . 'et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut '
    . 'aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum '
    . 'dolore eu fugiat nulla pariatur.' => '洛雷姆·伊普索姆·多尔坐在阿米特的座位上，他是一位非常优秀的运动员，他是一位非常'
    . '出色的速度运动员，他是一位非常出色的运动员。如果是最小的鹿肉，那么诺斯特鲁德的实验室就不需要支付任何费用。在巴黎的富家大教堂里，'
    . '有一只可爱的海豚。',
    'Thank you for registration. Please check your inbox for verification email.' => '感谢您的注册。 请在收件箱中查看验证邮件。'
];
