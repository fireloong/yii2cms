<?php

return [
    'MOD_CONTENT' => '内容管理',
    'CONTENTS' => '文章管理',
    'CATEGORIES_VIEW_DEFAULT_TITLE' => '文章分类列表',
    'CATEGORIES_VIEW_DEFAULT_DESC' => '以列表形式显示一个文章分类中包含的全部下级分类',
    'CATEGORIES_CHOOSE_CATEGORY_LABEL' => '选择文章类别',
    'CATEGORIES_CHOOSE_CATEGORY_DESC' => '选择此分类中要显示的下级分类的最高级别',
    'CATEGORIES_VIEW_ARTICLE_TITLE' => '单篇文章',
    'CATEGORIES_VIEW_ARTICLE_DESC' => '显示单篇文章内容',
    'SELECT_ARTICLE_LABEL' => '选择文章',
    'SELECT_ARTICLE_DESC' => '选择要显示的文章',
    'MENU_ITEM_LINK' => '菜单项链接',
    'CATEGORIES_SHOW_CHILDREN_LABEL' => '显示子菜单项',
    'CATEGORIES_SHOW_CHILDREN_DESC' => '是否显示子菜单项',
    'TEMPLATE_LABEL' => '指定模板',
    'TEMPLATE_DESC' => '选择要显示的样式模板'
];
