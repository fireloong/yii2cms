<?php

return [
    'APP-FRONTEND' => '系统前台',
    'SITE_INDEX_VIEW_DEFAULT_TITLE' => '其它',
    'SITE_INDEX_VIEW_DEFAULT_DESC' => '特定页面排版',
    'TEMPLATE_LABEL' => '指定模板',
    'TEMPLATE_DESC' => '选择要显示的样式模板'
];
