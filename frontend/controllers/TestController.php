<?php


namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\components\Markdown;

/**
 * description of TestController
 *
 * @author FireLoong
 */
class TestController extends Controller
{

    public function actionIndex()
    {
        //$regex = "/^>(?:\[(info|warning|success|danger)\])(?:\[(0|1)\])?/";
        //$str = '>[info]';
        //preg_match($regex, $str,$matches);
        //print_r($matches);
        //exit;
        $markdown = <<<MARKDOWN
### 主要特性

- 支持“标准”Markdown / CommonMark和Github风格的语法，也可变身为代码编辑器；
- 支持实时预览、图片（跨域）上传、预格式文本/代码/表格插入、代码折叠、搜索替换、只读模式、自
定义样式主题和多语言语法高亮等功能；
- \[x\] 支持ToC（Table of Contents）、Emoji表情、Task lists、@链接等Markdown扩展语法；
- 支持TeX科学公式（基于KaTeX）、流程图 Flowchart 和 时序图 Sequence Diagram;
- 支持识别和解析HTML标签，并且支持自定义过滤标签解析，具有可靠的安全性和几乎无限的扩展性；
- 支持 AMD / CMD 模块化加载（支持 Require.js & Sea.js），并且支持自定义扩展插件；
- 兼容主流的浏览器（IE8+）和Zepto.js，且支持iPad等平板设备；
- 支持自定义主题样式；
- [ ] 基于 `trie` 树结构实现高效词图扫描
-   \[ \] 生成所有切词可能的有向无环图 `DAG`
-   \[ \] 采用动态规划算法计算最佳切词组合
-   \[ \] 基于 `HMM` 模型，采用 `Viterbi` (维特比)算法实现未登录词识别

> ejkfh

>[success][right] fegeg
--------ghh

## 我好方姐姐
>[info] dfjkefhje

codepen[vclLe][480]

```shell
\$ composer require annotations
```

```xml
<!-- config/routes.xml -->
<?xml version="1.0" encoding="UTF-8" ?>
<routes xmlns="http://symfony.com/schema/routing"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://symfony.com/schema/routing
        https://symfony.com/schema/routing/routing-1.0.xsd">

    <!-- the controller value has the format 'controller_class::method_name' -->
    <route id="blog_list" path="/blog"
           controller="App\Controller\BlogController::list"/>

    <!-- if the action is implemented as the __invoke() method of the
         controller class, you can skip the '::method_name' part:
         controller="App\Controller\BlogController"/> -->
</routes>
```

```yaml
# config/routes.yaml
api_post_show:
    path:       /api/posts/{id}
    controller: App\Controller\BlogApiController::show
    methods:    GET|HEAD

api_post_edit:
    path:       /api/posts/{id}
    controller: App\Controller\BlogApiController::edit
    methods:    PUT
```

```php
// config/routes.php
use App\Controller\BlogApiController;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;

return function (RoutingConfigurator \$routes) {
    \$routes->add('api_post_show', '/api/posts/{id}')
        ->controller([BlogApiController::class, 'show'])
        ->methods(['GET', 'HEAD'])
    ;
    \$routes->add('api_post_edit', '/api/posts/{id}')
        ->controller([BlogApiController::class, 'edit'])
        ->methods(['PUT'])
    ;
};
```
MARKDOWN;

//        $hyperDown = new HyperDown();
//        $html = $hyperDown->makeHtml($markdown);
//        $parser = new FireLoongMarkdown();
//        $html = $parser->parse($markdown);
        $html = Markdown::process($markdown);

//        print_r(Yii::$classMap);exit;

        return $this->render('index', ['html' => $html]);
    }

    public function actionA()
    {
        $modulesConfigPath = Yii::getAlias('@backend/config/modules.php');
        $aa = include $modulesConfigPath;
        $bb = \yii\helpers\VarDumper::export($aa);
        print_r(str_replace('\\\\','\\',$bb));
    }
}
