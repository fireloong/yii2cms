<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%menu}}".
 *
 * @property int $id
 * @property string $name
 * @property string $menutype
 * @property string $alias
 * @property string $note
 * @property string $path
 * @property string $type
 * @property int $published
 * @property int|null $parent
 * @property int $level
 * @property string $route
 * @property string $request
 * @property string $params
 * @property int $browserNav
 * @property string $language
 * @property int $lft
 * @property int $rgt
 * @property int $ordering
 * @property int $home
 * @property resource|null $data
 *
 * @property Menu $parent0
 * @property Menu[] $menus
 */
class Menu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'menutype', 'alias', 'path', 'type', 'route', 'request', 'params'], 'required'],
            [['published', 'parent', 'level', 'browserNav', 'lft', 'rgt', 'ordering', 'home'], 'integer'],
            [['request', 'params', 'data'], 'string'],
            [['name'], 'string', 'max' => 128],
            [['menutype'], 'string', 'max' => 24],
            [['alias'], 'string', 'max' => 200],
            [['note', 'route'], 'string', 'max' => 255],
            [['path'], 'string', 'max' => 1024],
            [['type'], 'string', 'max' => 16],
            [['language'], 'string', 'max' => 7],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => Menu::class, 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'menutype' => 'Menutype',
            'alias' => 'Alias',
            'note' => 'Note',
            'path' => 'Path',
            'type' => 'Type',
            'published' => 'Published',
            'parent' => 'Parent',
            'level' => 'Level',
            'route' => 'Route',
            'request' => 'Request',
            'params' => 'Params',
            'browserNav' => 'Browser Nav',
            'language' => 'Language',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'ordering' => 'Ordering',
            'home' => 'Home',
            'data' => 'Data',
        ];
    }

    /**
     * Gets query for [[Parent0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(Menu::class, ['id' => 'parent']);
    }

    /**
     * Gets query for [[Menus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::class, ['parent' => 'id']);
    }
}
