<?php
return [
    'as access' => [
        'class' => 'backend\modules\admin\components\AccessControl',
        'allowActions' => [
//            '/site/*',
            '/debug/*',
        ],
    ]
];