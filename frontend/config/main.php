<?php

use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => require __DIR__ . '/modules.php',
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'basePath' => '@app/themes/' . $params['global']['theme'],
                'baseUrl' => '@web/themes/' . $params['global']['theme'],
                'pathMap' => [
                    '@app/views' => '@app/themes/' . $params['global']['theme']
                ]
            ]
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/themes/' . $params['global']['theme'] . '/messages',
                    'on missingTranslation' => [
                        'common\components\TranslationEventHandler',
                        'handleMissingTranslation'
                    ]
                ],
                'yii' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@yii/messages',
                    'on missingTranslation' => [
                        'common\components\YiiTranslationEventHandler',
                        'handleMissingTranslation'
                    ]
                ]
            ]
        ],
        'urlManager' => [
            'class' => 'common\components\yii\UrlManager',
            'enableStrictParsing' => true,// 是否开启严格解析
            'rules' => [
                'language' => '/site/language',
//                'index' => '/site/index',
//                'about' => '/site/about',
//                'contact' => '/site/contact',
//                'signup' => '/site/signup',
//                'login' => '/site/login',
//                'logout' => '/site/logout',
//                'debug' => '/site/debug',
//                'debugb' => '/site/debugb',
            ],
        ],
    ],
    'params' => $params,
];
