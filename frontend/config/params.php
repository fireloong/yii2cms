<?php
return [
    'adminEmail' => 'admin@example.com',
    'global' => [
        'theme' => 'basic',
    ],
];
