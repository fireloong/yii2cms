<?php

/* @var $this yii\web\View */

/* @var $content string */

use yii\bootstrap\Html;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\themes\basic\_assets\AppAsset;
use frontend\themes\basic\_widgets\MenuWidget;
use frontend\themes\basic\_widgets\LanguageSwitcherWidget;

$asset = AppAsset::register($this);

$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php $this->registerCsrfMetaTags() ?>
<title><?= Html::encode($this->title) ?></title>
<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Html::icon('piggy-bank', ['style' => 'color:#d9534f']),
        'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => ['title' => Yii::t('site', Yii::$app->name)],
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top'
        ],
    ]);

    echo MenuWidget::widget(['type' => 'mainmenu']);
    echo LanguageSwitcherWidget::widget();
    NavBar::end();
    ?>

    <div id="<?= $this->params['id'] ?? str_replace('/', '_', Yii::$app->controller->route) ?>" class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::t('site', Yii::$app->name)) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
