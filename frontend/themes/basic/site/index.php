<?php

/* @var $this yii\web\View */

use frontend\themes\basic\_assets\AppAsset;

$asset = AppAsset::register($this);

$asset->css[] = 'css/site.css';
$asset->js[] = 'https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.6/holder.min.js';

$this->title = '首页';
?>
<div class="row">
    <div class="col-sm-12">
        <div class="jumbotron">
            <h1>Theme example</h1>
            <p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting
                point to create something more unique by building on or modifying it.</p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <img class="img-circle" data-src="holder.js/140x140?auto=yes&bg=777&fg=777" alt="Generic placeholder image">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
            vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus
            magna.</p>
        <p>
            <a href="#" class="btn btn-default" role="button">View details »</a>
        </p>
    </div>
    <div class="col-lg-4">
        <img class="img-circle" data-src="holder.js/140x140?auto=yes&bg=777&fg=777" alt="Generic placeholder image">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
            vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus
            magna.</p>
        <p>
            <a href="#" class="btn btn-default" role="button">View details »</a>
        </p>
    </div>
    <div class="col-lg-4">
        <img class="img-circle" data-src="holder.js/140x140?auto=yes&bg=777&fg=777" alt="Generic placeholder image">
        <h2>Heading</h2>
        <p>Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies
            vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus
            magna.</p>
        <p>
            <a href="#" class="btn btn-default" role="button">View details »</a>
        </p>
    </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
    <div class="col-md-7">
        <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span>
        </h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod
            semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus
            commodo.</p>
    </div>
    <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="500x500">
    </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
    <div class="col-md-7 col-md-push-5">
        <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-5 col-md-pull-7">
        <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="500x500">
    </div>
</div>
<hr class="featurette-divider">
<div class="row featurette">
    <div class="col-md-7">
        <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-5">
        <img class="featurette-image img-responsive center-block" data-src="holder.js/500x500/auto" alt="500x500">
    </div>
</div>
