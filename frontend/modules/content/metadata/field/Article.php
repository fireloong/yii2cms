<?php


namespace frontend\modules\content\metadata\field;

use common\components\Helper;
use frontend\modules\content\models\Content;
use Yii;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use common\components\form\Field;
use backend\components\Icon;
use yii\helpers\Url;

/**
 * description of Article
 *
 * @author FireLoong
 */
class Article extends Field
{
    public function xmlSetup(\SimpleXMLElement $field, $options = [])
    {
        $data = ArrayHelper::remove($options, 'data');

        $nameAlias = $this->attribute . '_' . (string)$field->attributes()->name . '_alias';
        $modalId = $this->attribute . ucfirst((string)$field->attributes()->name) . 'AliasModal';
        $inputAlias = Html::textInput($nameAlias, '', ['id' => $nameAlias, 'class' => 'form-control', 'readonly' => true]);
        $inputAliasBtn = Html::tag('div', Html::button(Icon::i('file-empty') . Yii::t('common', 'SELECT'), [
            'class' => 'btn btn-default',
            'data' => [
                'toggle' => 'modal',
                'target' => '#' . $modalId
            ]
        ]), ['class' => 'input-group-btn']);
        $attribute = $this->attribute . '[' . (string)$field->attributes()->name . ']';
        //$inputId = Html::getInputId($this->model, $attribute);
        $hiddenInput = Html::activeHiddenInput($this->model, $attribute);

        $this->parts['{input}'] = Html::tag('div', $inputAlias . $inputAliasBtn, ['class' => 'input-group']) . $hiddenInput;

        $label = Yii::t($data['langCat'], (string)$field->attributes()->label);
        $description = (string)$field->attributes()->description;
        $labelOptions = [];
        if (!empty($description)) {
            $labelOptions['desc'] = Yii::t($data['langCat'], $description);
        }

        return $this->label($label, $labelOptions);
    }

    public function xSetup(\SimpleXMLElement $field, $options = [])
    {
        $data = ArrayHelper::remove($options, 'data');

        $inputOptions['value'] = (string)$field->attributes()->default;

        $aliasLabel = null;

        $run = Helper::runApp(strstr(__CLASS__, '\\', true));
        if ($this->model->route === '@' . $run->id . '/content/default/view') {
            $attributeValue = Html::getAttributeValue($this->model,$this->attribute);
            if (!is_null($attributeValue)) {
                $inputOptions['value'] = $attributeValue;
                $content = Content::findOne($inputOptions['value']);
                $aliasLabel = $content->title;
            }
        }

        $inputId = Html::getInputId($this->model, $this->attribute);
        $label = Yii::t($data['langCat'], (string)$field->attributes()->label);
        $inputAlias = Html::textInput($inputId . '-alias', $aliasLabel ?? $label, [
            'id' => $inputId . '-alias',
            'class' => 'form-control',
            'readonly' => true
        ]);
        $inputBtn = Html::tag('div', Html::button(Icon::i('file-empty') . Yii::t('common', 'SELECT'), [
            'class' => 'btn btn-default',
            'data' => [
                'toggle' => 'modal',
                'target' => '#' . Inflector::variablize($inputId) . 'Modal'
            ]
        ]), ['class' => 'input-group-btn']);

        $hiddenInput = Html::activeHiddenInput($this->model, $this->attribute, $inputOptions);

        $this->parts['{input}'] = Html::tag('div', $inputAlias . $inputBtn, ['class' => 'input-group']) . $hiddenInput;

        $description = (string)$field->attributes()->description;
        $labelOptions = [];
        if (!empty($description)) {
            $labelOptions['desc'] = Yii::t($data['langCat'], $description);
        }

        $this->form->attributes[] = $this->getClientOptions($field->validator, $label);

        $modalId = Inflector::variablize($inputId) . 'Modal';
        $this->form->view->on($this->form->view::EVENT_END_BODY, function () use ($modalId, $label) {
            $src = Url::to(['/content/articles', 'layout' => 'modal']);
            $html = <<<HTML
<div id="$modalId" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">$label</h4>
            </div>
            <div class="modal-body">
                <div class="embed-responsive embed-responsive-4by3">
                    <iframe class="embed-responsive-item" src="$src"></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
HTML;
            echo $html;
        });

        $js = <<<JS
window.articleModalClose = function(data){
    $('#$modalId').modal('hide');
    $('#$inputId-alias').val(data.title);
    $('#$inputId').val(data.id);
};
JS;
        $this->form->view->registerJs($js);

        return $this->label($label, $labelOptions);
    }
}
