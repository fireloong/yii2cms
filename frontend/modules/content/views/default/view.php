<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\content\controllers\Content */
/* @var $user common\models\User */

$this->title = $model->title;
$css = <<<CSS
.page-body img{max-width: 100%;}
CSS;
$this->registerCss($css);
?>
<div class="content-default-view">
    <div class="row">
        <div class="col-sm-9">
            <div class="page-header">
                <h1><?= $model->title ?></h1>
                <div class="info">
                    <span class="text-muted date"><?= Yii::$app->formatter->asDatetime($model->created_at) ?></span>
                    <span class="author"><?= $model->created_by_alias ?: $user->username ?></span>
                </div>
            </div>
            <div class="page-body"><?= $model->fulltext ?></div>
            <div class="page-footer">
                <nav aria-label="pager">
                    <ul class="pager">
                        <li class="previous"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>
                        <li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
