<?php

use yii\helpers\Url;
use yii\helpers\Html;
//use frontend\assets\AppAsset;
use frontend\themes\basic\_assets\AppAsset;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\content\models\searchs\Content */
/* @var $list yii\data\ActiveDataProvider */
/* @var $mid frontend\modules\content\controllers\DefaultController */

$this->title = Yii::t('mod_content', 'CONTENTS');

$asset = AppAsset::register($this);
$asset->js[] = 'https://cdn.jsdelivr.net/npm/holderjs@2.9.6/holder.min.js';
?>
<div class="content-default-index">
    <div class="row">
        <div class="col-sm-9">
            <?php if (empty($list)): ?>
                <h1 class="text-warning">暂 无 数 据 ！</h1>
            <?php else: ?>
            <ul class="media-list">
                <?php foreach ($list as $item): ?>
                    <li class="media">
                        <div class="media-left">
                            <a href="<?= Url::to(['/content/default/view', 'id' => $item->id, 'mid' => $mid]) ?>">
                                <img class="media-object" data-src="holder.js/120x80" alt="thumbnail">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">
                                <?= Html::a($item->title, ['/content/default/view', 'id' => $item->id, 'mid' => $mid]) ?>
                            </h4>
                            <div class="media-intro">
                                <?= trim(str_replace(PHP_EOL, '', mb_substr(strip_tags($item->fulltext), 0, 100))) ?>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</div>
