<?php

namespace frontend\modules\content\models;

use Yii;

/**
 * This is the model class for table "{{%content}}".
 *
 * @property int $id
 * @property string $title
 * @property string $alias
 * @property string $introtext
 * @property string $fulltext
 * @property int $status
 * @property int $catid
 * @property int $created_at
 * @property int $created_by
 * @property string $created_by_alias
 * @property int $modified_at
 * @property int $modified_by
 * @property int|null $publish_up
 * @property int|null $publish_down
 * @property string $images
 * @property string $urls
 * @property string $attribs
 * @property int $ordering
 * @property string $metakey
 * @property string $metadesc
 * @property int $hits
 * @property string $metadata
 * @property int $featured
 * @property string $language
 * @property string $xreference
 * @property string $note
 */
class Content extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%content}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['introtext', 'fulltext', 'created_at', 'modified_at', 'images', 'urls', 'attribs', 'metakey', 'metadesc', 'metadata', 'language'], 'required'],
            [['introtext', 'fulltext', 'images', 'urls', 'metakey', 'metadesc', 'metadata'], 'string'],
            [['status', 'catid', 'created_at', 'created_by', 'modified_at', 'modified_by', 'publish_up', 'publish_down', 'ordering', 'hits', 'featured'], 'integer'],
            [['title', 'created_by_alias', 'note'], 'string', 'max' => 255],
            [['alias'], 'string', 'max' => 400],
            [['attribs'], 'string', 'max' => 5120],
            [['language'], 'string', 'max' => 7],
            [['xreference'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('mod_content', 'ID'),
            'title' => Yii::t('mod_content', 'Title'),
            'alias' => Yii::t('mod_content', 'Alias'),
            'introtext' => Yii::t('mod_content', 'Introtext'),
            'fulltext' => Yii::t('mod_content', 'Fulltext'),
            'status' => Yii::t('mod_content', 'Status'),
            'catid' => Yii::t('mod_content', 'Catid'),
            'created_at' => Yii::t('mod_content', 'Created At'),
            'created_by' => Yii::t('mod_content', 'Created By'),
            'created_by_alias' => Yii::t('mod_content', 'Created By Alias'),
            'modified_at' => Yii::t('mod_content', 'Modified At'),
            'modified_by' => Yii::t('mod_content', 'Modified By'),
            'publish_up' => Yii::t('mod_content', 'Publish Up'),
            'publish_down' => Yii::t('mod_content', 'Publish Down'),
            'images' => Yii::t('mod_content', 'Images'),
            'urls' => Yii::t('mod_content', 'Urls'),
            'attribs' => Yii::t('mod_content', 'Attribs'),
            'ordering' => Yii::t('mod_content', 'Ordering'),
            'metakey' => Yii::t('mod_content', 'Metakey'),
            'metadesc' => Yii::t('mod_content', 'Metadesc'),
            'hits' => Yii::t('mod_content', 'Hits'),
            'metadata' => Yii::t('mod_content', 'Metadata'),
            'featured' => Yii::t('mod_content', 'Featured'),
            'language' => Yii::t('mod_content', 'Language'),
            'xreference' => Yii::t('mod_content', 'Xreference'),
            'note' => Yii::t('mod_content', 'Note'),
        ];
    }
}
