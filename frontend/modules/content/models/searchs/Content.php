<?php

namespace frontend\modules\content\models\searchs;

use backend\modules\categories\models\Categories;
use common\components\Tree;
use frontend\models\Menu;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\content\models\Content as ContentModel;
use yii\helpers\ArrayHelper;

/**
 * Content represents the model behind the search form of `frontend\modules\content\models\Content`.
 */
class Content extends ContentModel
{
    public $cid;
    public $mid;
    public $menu;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'catid', 'created_at', 'created_by', 'modified_at', 'modified_by',
                'publish_up', 'publish_down', 'ordering', 'hits', 'featured'], 'integer'],
            [['title', 'alias', 'introtext', 'fulltext', 'created_by_alias', 'images', 'urls',
                'attribs', 'metakey', 'metadesc', 'metadata', 'language', 'xreference', 'note'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $mid = ArrayHelper::remove($params, 'mid');
        $cid = ArrayHelper::remove($params, 'cid');
        $query = ContentModel::find();

        $categories = Categories::find()->where(['published' => 1])->orderBy('lft')->asArray()->all();

        $tree = new Tree($categories);

        if (is_null($mid)) {
            $childrenCatIds = $tree->getAllChildrenIds($cid);
            $query->where('catid = :catid', [':catid' => $cid])->orWhere(['catid' => $childrenCatIds]);
        } else {
            $this->menu = Menu::findOne($mid);
            $request = json_decode($this->menu->request, true);
            if (isset($request['cid']) && is_numeric($request['cid'])) {
                $childrenCatIds = $tree->getAllChildrenIds($request['cid']);
                $query->where(['catid' => $request['cid']])->orWhere(['catid' => $childrenCatIds]);
            }
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'catid' => $this->catid,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'modified_at' => $this->modified_at,
            'modified_by' => $this->modified_by,
            'publish_up' => $this->publish_up,
            'publish_down' => $this->publish_down,
            'ordering' => $this->ordering,
            'hits' => $this->hits,
            'featured' => $this->featured,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'alias', $this->alias])
            ->andFilterWhere(['like', 'introtext', $this->introtext])
            ->andFilterWhere(['like', 'fulltext', $this->fulltext])
            ->andFilterWhere(['like', 'created_by_alias', $this->created_by_alias])
            ->andFilterWhere(['like', 'images', $this->images])
            ->andFilterWhere(['like', 'urls', $this->urls])
            ->andFilterWhere(['like', 'attribs', $this->attribs])
            ->andFilterWhere(['like', 'metakey', $this->metakey])
            ->andFilterWhere(['like', 'metadesc', $this->metadesc])
            ->andFilterWhere(['like', 'metadata', $this->metadata])
            ->andFilterWhere(['like', 'language', $this->language])
            ->andFilterWhere(['like', 'xreference', $this->xreference])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
