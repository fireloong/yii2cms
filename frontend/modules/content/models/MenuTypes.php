<?php

namespace frontend\modules\content\models;

use Yii;

/**
 * This is the model class for table "{{%menu_types}}".
 *
 * @property int $id
 * @property string $menutype
 * @property string $title
 * @property string $description
 * @property int $client_id
 */
class MenuTypes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%menu_types}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['menutype', 'title'], 'required'],
            [['client_id'], 'integer'],
            [['menutype'], 'string', 'max' => 24],
            [['title'], 'string', 'max' => 48],
            [['description'], 'string', 'max' => 255],
            [['menutype'], 'unique'],
        ];
    }
}
