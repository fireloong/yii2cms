<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title>Debug</title>
        <?php $this->head() ?>
        <link href="https://cdn.bootcss.com/SyntaxHighlighter/3.0.83/styles/shCore.min.css" rel="stylesheet">
        <link href="https://cdn.bootcss.com/SyntaxHighlighter/3.0.83/styles/shThemeRDark.min.css" rel="stylesheet">
        <style>.syntaxhighlighter table td.gutter,.syntaxhighlighter table td.code{padding:7px 0!important}</style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <pre class="brush:php"><?= Html::encode($content) ?></pre>
        <?php $this->endBody() ?>
        <script src="https://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/shCore.min.js"></script>
        <script src="https://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/shBrushPhp.min.js"></script>
        <script src="https://cdn.bootcss.com/SyntaxHighlighter/3.0.83/scripts/shBrushXml.min.js"></script>
        <script>
            SyntaxHighlighter.defaults['highlight'] = <?= $line ?>;
            SyntaxHighlighter.defaults['quick-code'] = false;
            SyntaxHighlighter.defaults['toolbar'] = false;
            //SyntaxHighlighter.defaults['html-script'] = true;
            SyntaxHighlighter.all();
        </script>
    </body>
</html>
<?php $this->endPage() ?>