<?php

/* @var $this \yii\web\View */

use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);
$this->beginPage();
?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title>Debug</title>
        <?php $this->head() ?>
        <link href="https://cdn.bootcss.com/highlight.js/9.15.8/styles/monokai-sublime.min.css" rel="stylesheet">
        <script src="https://cdn.bootcss.com/highlight.js/9.15.8/highlight.min.js"></script>
        <script src="https://cdn.bootcss.com/highlightjs-line-numbers.js/2.7.0/highlightjs-line-numbers.min.js"></script>
        <script>hljs.initHighlightingOnLoad();hljs.initLineNumbersOnLoad();</script>
        <style>
            .hljs-ln{width: 100%;}
            .hljs-ln .hljs-ln-numbers {
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -khtml-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;

                text-align: center;
                color: #ccc;
                border-right: 1px solid #CCC;
                vertical-align: top;
                padding-right: 5px;
                white-space: nowrap;
            }
            .hljs-ln .hljs-ln-code {
                padding-left: 10px;
            }
            .hljs-ln-line[data-line-number="<?= $line ?>"]{background-color: #363830;}
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>
        <pre style="margin-bottom:50px;"><code class="html"><?= Html::encode($content) ?></code></pre>
        <?php $this->endBody() ?>
        <script></script>
    </body>
</html>
<?php $this->endPage() ?>