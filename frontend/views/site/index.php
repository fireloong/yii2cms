<?php

/* @var $this yii\web\View */

$this->title = Yii::t('site', 'My Yii Application');
?>
<div class="site-index">

    <div class="jumbotron">
        <h1><?= Yii::t('site', 'Congratulations!') ?></h1>

        <p class="lead"><?= Yii::t('site', 'You have successfully created your Yii-powered application.') ?></p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com"><?= Yii::t('site', 'Get started with Yii') ?></a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2><?= Yii::t('site', 'Heading') ?></h2>

                <p><?= Yii::t('site', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.') ?></p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/doc/"><?= Yii::t('site', 'Yii Documentation') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('site', 'Heading') ?></h2>

                <p><?= Yii::t('site', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.') ?></p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/forum/"><?= Yii::t('site', 'Yii Forum') ?> &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2><?= Yii::t('site', 'Heading') ?></h2>

                <p><?= Yii::t('site', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.') ?></p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/"><?= Yii::t('site', 'Yii Extensions') ?> &raquo;</a></p>
            </div>
        </div>
        <div class="col-lg-12">
            <pre></pre>
        </div>
    </div>
</div>
