<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Unicode 中文转化';
$this->params['breadcrumbs'][] = $this->title;

$js = <<<JS
$('#lrc').click(function(){
    $(this).closest('form').append('<input type="hidden" name="type" value="lrc"/>');
});
$('#mp3').click(function(){
    $(this).closest('form').append('<input type="hidden" name="type" value="mp3"/>');
});
JS;
$this->registerJs($js);
?>
<div class="site-test">
    <?= Html::beginForm() ?>
    <?= Html::textInput('kugouurl', $kugouurl, ['class' => 'form-control', 'placeholder' => '酷狗 URL']) ?>
    <br>
    <?= Html::submitButton('下载歌词', ['id' => 'lrc', 'class' => 'btn btn-primary']) ?>
    &emsp;&emsp;
    <?= Html::submitButton('下载MP3', ['id' => 'mp3', 'class' => 'btn btn-success']) ?>
    <?= Html::endForm() ?>
    <hr>
    <?= Html::beginForm() ?>
    <?= Html::textarea('a', $a, ['class' => 'form-control', 'rows' => '3']) ?>
    <br>
    <?= Html::textarea('b', $b, ['class' => 'form-control', 'rows' => '3']) ?>
    <br>
    <?= Html::submitButton('提交', ['class' => 'btn btn-primary btn-lg btn-block']) ?>
    <br>
    <?= Html::textarea('c', $c, ['class' => 'form-control', 'rows' => '3']) ?>
    <br>
    <?= Html::textarea('d', $d, ['class' => 'form-control', 'rows' => '3']) ?>
    <?= Html::endForm() ?>
</div>
