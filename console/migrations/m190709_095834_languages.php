<?php

use yii\db\Migration;

/**
 * Class m190709_095834_languages
 */
class m190709_095834_languages extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%languages}}', [
            'id' => $this->primaryKey()->unsigned(),
            'code' => $this->char(7)->notNull(),
            'title' => $this->string(50)->notNull(),
            'title_native' => $this->string(50)->notNull(),
            'sef' => $this->string(50)->notNull(),
            'image' => $this->string(50)->notNull(),
            'description' => $this->string(512)->notNull(),
            'metakey' => $this->text()->notNull(),
            'metadesc' => $this->text()->notNull(),
            'sitename' => $this->string(1024)->notNull()->defaultValue(''),
            'published' => $this->tinyInteger()->notNull()->defaultValue(0),
            'order' => $this->integer()->notNull()->defaultValue(0)
                ], $tableOptions);

        $this->createIndex('idx_sef', '{{%languages}}', 'sef', true);
        $this->createIndex('idx_code', '{{%languages}}', 'code', true);

//        $this->insert('{{%languages}}', [
//            'code' => 'zh-CN',
//            'title' => 'Chinese Simplified (zh-CN)',
//            'title_native' => '简体中文(中国)',
//            'sef' => 'zh',
//            'image' => '',
//            'description' => '',
//            'metakey' => '',
//            'metadesc' => '',
//            'sitename' => '',
//            'published' => 1,
//            'order' => 0
//        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->delete('{{%languages}}');
        $this->dropIndex('idx_sef', '{{%languages}}');
        $this->dropIndex('idx_code', '{{%languages}}');
        $this->dropTable('{{%languages}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190709_095834_languages cannot be reverted.\n";

      return false;
      }
     */
}
