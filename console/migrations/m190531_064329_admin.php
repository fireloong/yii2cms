<?php

use yii\db\Migration;

/**
 * Class m190531_064329_admin
 */
class m190531_064329_admin extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%admin}}', [
            'uid' => $this->integer()->notNull()->unique(),
            ], $tableOptions);

        // add foreign key for table `user`
        $this->addForeignKey('fk-admin-uid', '{{%admin}}', 'uid', '{{%user}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey('fk-admin-uid', '{{%admin}}');

        $this->dropTable('{{%admin}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190531_064329_admin cannot be reverted.\n";

      return false;
      }
     */
}
