<?php

use yii\db\Migration;

/**
 * Class m190601_090302_member
 */
class m190601_090302_member extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%member}}', [
            'uid' => $this->integer()->notNull()->unique(),
            ], $tableOptions);

        // add foreign key for table `user`
        $this->addForeignKey('fk-member-uid', '{{%member}}', 'uid', '{{%user}}', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey('fk-member-uid', '{{%member}}');

        $this->dropTable('{{%member}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190601_090302_member cannot be reverted.\n";

      return false;
      }
     */
}
