<?php

use yii\db\Migration;

/**
 * Class m190713_104212_clients
 */
class m190713_104212_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%clients}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(50)->notNull()->unique(),
            'url' => $this->string(1024)->notNull(),
            'path' => $this->string(200),
            'description' => $this->string(),
            'order' => $this->integer()->notNull()->defaultValue(0),
            'status' => $this->tinyInteger(3)->unsigned()->notNull()->defaultValue(0)
                ], $tableOptions);
        
        $this->insert('{{%clients}}', [
            'name' => 'backend',
            'url' => 'http://backend.dev/',
            'path' => '/backend',
            'description' => 'Background management application',
            'order' => 0,
            'status' => 1
        ]);
        
        $this->insert('{{%clients}}', [
            'name' => 'frontend',
            'url' => 'http://frontend.dev/',
            'path' => '/frontend',
            'description' => 'Front desk application',
            'order' => 0,
            'status' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%clients}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190713_104212_clients cannot be reverted.\n";

      return false;
      }
     */
}
