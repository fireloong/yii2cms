<?php

use yii\db\Migration;
//use yii\helpers\Json;

/**
 * Class m190620_012148_extensions
 */
class m190620_012148_extensions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%extensions}}', [
            'id' => $this->primaryKey(),
            'package_id' => $this->integer()->notNull()->defaultValue(0),
            'name' => $this->string(100)->notNull(),
            'type' => $this->string(20)->notNull(),
            'element' => $this->string(100)->notNull(),
            'folder' => $this->string(100)->notNull(),
            'client_id' => $this->tinyInteger()->notNull(),
            'enabled' => $this->tinyInteger()->notNull()->defaultValue(0),
            'protected' => $this->tinyInteger()->notNull()->defaultValue(0),
            //'manifest_cache' => $this->text()->notNull(),
            'manifest_cache' => $this->json()->notNull(),
            'params' => $this->text()->notNull(),
            'order' => $this->integer()->defaultValue(0),
            'status' => $this->smallInteger()->defaultValue(0),
                ], $tableOptions);

//        $manifest_cache = [
//            'name' => 'Chinese Simplified (zh-CN) Language Pack',
//            'packagename' => 'zh-CN',
//            'version' => '1.0.0',
//            'creationDate' => '2019年7月',
//            'author' => 'FireLoong 火龙网 www.fireloong.com',
//            'authorEmail' => 'fireloong@foxmail.com',
//            'authorUrl' => 'www.fireloong.com',
//            'description' => 'Yii2CMS 简体中文(zh-CN) 语言'
//        ];

//        $this->insert('{{%extensions}}', [
//            'package_id' => 0,
//            'name' => 'Chinese Simplified (zh-CN) Language Pack',
//            'type' => 'package',
//            'element' => 'zh-CN_pkg',
//            'folder' => '',
//            'client_id' => 0,
//            'enabled' => 1,
//            'protected' => 1,
//            'manifest_cache' => Json::encode($manifest_cache, JSON_ERROR_NONE),
//            'params' => '',
//            'order' => 0,
//            'status' => 1
//        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
//        $this->delete('{{%extensions}}');
        $this->dropTable('{{%extensions}}');
    }

    /*
      // Use up()/down() to run migration code without a transaction.
      public function up()
      {

      }

      public function down()
      {
      echo "m190620_012148_extensions cannot be reverted.\n";

      return false;
      }
     */
}
