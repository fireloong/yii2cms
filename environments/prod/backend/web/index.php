<?php

defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');
defined('ROOT_PATH') or define('ROOT_PATH', dirname(dirname(__DIR__)));
defined('APP_PATH') or define('APP_PATH', dirname(__DIR__));

require ROOT_PATH . '/vendor/autoload.php';
require ROOT_PATH . '/vendor/yiisoft/yii2/BaseYii.php';

class Yii extends \yii\BaseYii
{
}

spl_autoload_register(['Yii', 'autoload'], true, true);
require ROOT_PATH . '/common/config/bootstrap.php';
require APP_PATH . '/config/bootstrap.php';

$classMap = require YII2_PATH . '/classes.php';

$commonClassMapFile = ROOT_PATH . '/common/config/classMap.php';
if (is_file($commonClassMapFile)) {
    $classMap = array_merge($classMap, require $commonClassMapFile);
}

$appClassMapFile = dirname(__DIR__) . '/config/classMap.php';
if (is_file($appClassMapFile)) {
    $classMap = array_merge($classMap, require $appClassMapFile);
}

Yii::$classMap = $classMap;

Yii::$container = new yii\di\Container();

$config = yii\helpers\ArrayHelper::merge(
    require ROOT_PATH . '/common/config/main.php',
    require ROOT_PATH . '/common/config/main-local.php',
    require ROOT_PATH . '/common/config/extension.php',
    require APP_PATH . '/config/main.php',
    require APP_PATH . '/config/main-local.php',
    require APP_PATH . '/config/extension.php'
);

(new yii\web\Application($config))->run();
